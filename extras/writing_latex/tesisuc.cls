% Modificada por pitillo, julio 2004
% Pensada para tesis de magister en ingles, sigue todas las recomendaciones de Debbie Meza :)
% Uselo bajo su propia responsabilidad!!!!!

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tesisuc}
[2004/07/21 v3
Estilo Tesis Escuela de Ingenier'ia] 

\DeclareOption{a4paper}
    {\ClassError{proc}{Option `a5paper' not supported}{}}
\DeclareOption{a5paper}
    {\ClassError{proc}{Option `a5paper' not supported}{}}
\DeclareOption{b5paper}
    {\ClassError{proc}{Option `b5paper' not supported}{}}
\DeclareOption{twocolumn}%
    {\ClassError{proc}{Option `twocolumn' not supported}{}}
\DeclareOption{twoside}%
    {\ClassError{proc}{Option `twoside' not supported}{}}
\DeclareOption{10pt}%
    {\ClassError{proc}{Option `10pt' not supported}{}}
\DeclareOption{11pt}%
    {\ClassError{proc}{Option `11pt' not supported}{}}

\PassOptionsToClass{12pt,oneside,fleqn,english}{book}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions
\LoadClass{book}

\RequirePackage{subfigure} 
%\RequirePackage[english,spanish]{babel} 
\RequirePackage[spanish,english]{babel} 
\selectlanguage{spanish}

% incluye el paquete harvardpuc.sty para las citas
% el estilo de cita es:
% \cite{c} = (Fulano, a#o)
% \citeasnoun{c} = Fulano (a#o) 
\RequirePackage[puccite]{harvardpuc}

%
% Necesarios para las portadas
%
\RequirePackage{graphicx}
\def\@logouc{logouc.ps}


% 
%  Espacio doble y simple
%
\RequirePackage{setspace}


\def\baselinestretchfloat{1.25} % one and half
\def\baselinestretchfoot{1.25}  % one and half
\def\baselinestretch{1} 
\def\baselinestretchnormal{1}

\def\@xfloat#1[#2]{\ifhmode \@bsphack\@floatpenalty -\@Mii\else
   \@floatpenalty-\@Miii\fi\def\@captype{#1}\ifinner
      \@parmoderr\@floatpenalty\z@
    \else\@next\@currbox\@freelist{\@tempcnta\csname ftype@#1\endcsname
       \multiply\@tempcnta\@xxxii\advance\@tempcnta\sixt@@n
       \@tfor \@tempa :=#2\do
                        {\if\@tempa h\advance\@tempcnta \@ne\fi
                         \if\@tempa t\advance\@tempcnta \tw@\fi
                         \if\@tempa b\advance\@tempcnta 4\relax\fi
                         \if\@tempa p\advance\@tempcnta 8\relax\fi
         }\global\count\@currbox\@tempcnta}\@fltovf\fi
    \global\setbox\@currbox\vbox\bgroup
    \def\baselinestretch{\baselinestretchfloat}\@normalsize
    \boxmaxdepth\z@
    \hsize\columnwidth \@parboxrestore}

\long\def\@footnotetext#1{\insert\footins{
    \def\baselinestretch{\baselinestretchfoot}\footnotesize
    \interlinepenalty\interfootnotelinepenalty
    \splittopskip\footnotesep
    \splitmaxdepth \dp\strutbox \floatingpenalty \@MM
    \hsize\columnwidth \@parboxrestore
   \edef\@currentlabel{\csname p@footnote\endcsname\@thefnmark}\@makefntext
    {\rule{\z@}{\footnotesep}\ignorespaces
      #1\strut}}}
     

%
% Solo para debug
%
\newif\if@debug \@debugfalse
\def\@ShowDebug#1{}


% Margenes para papel Tama\~no carta (letter)
% 21.6cm X 27.9
% 1in = 2.54 cm; 1pt = 0.035146cm
\setlength\paperheight{11in}  %
\setlength\paperwidth{8.5in}  %
\special{papersize=8.5in,11in}

\newdimen\realleftmargin
\newdimen\realrightmargin
\newdimen\realtopmargin
\newdimen\realbottommargin
\newdimen\realheadpos
\newdimen\citetextwidth

\realtopmargin    = 4cm
\realleftmargin   = 4cm 
\realbottommargin = 2.5cm
\realrightmargin  = 2.5cm 
\realheadpos      = 1.5cm

\headheight       = 12pt
\topskip          = 12pt
\parskip          = 12pt
%\parindent        = 2cm
\parindent	  = 0cm
\labelsep         = 2em

\setlength\oddsidemargin   {\realleftmargin}
\addtolength\oddsidemargin {-1in}
\@ShowDebug\oddsidemargin

\setlength\textheight{\paperheight}
  \addtolength\textheight {-\realtopmargin}
  \addtolength\textheight {-\realbottommargin}
\@ShowDebug\textheight

\setlength\textwidth{\paperwidth} 
 \addtolength\textwidth {-\realleftmargin}
 \addtolength\textwidth {-\realrightmargin}
\@ShowDebug\textwidth

\setlength\topmargin{\realheadpos} 
 \addtolength\topmargin {-1in}
\@ShowDebug\topmargin

\setlength\headsep{\realtopmargin} 
 \addtolength\headsep{-\realheadpos}
 \addtolength\headsep{-\headheight}
\@ShowDebug\headsep

\setlength\citetextwidth{\textwidth}
 \addtolength\citetextwidth{-1cm}

\newdimen\topmarginnormal
\newdimen\textheightnormal
\newdimen\headsepnormal

\setlength\textheightnormal{\textheight}
\setlength\topmarginnormal{\topmargin}
\setlength\headsepnormal{\headsep}

\newdimen\topmargintitle
\newdimen\textheighttitle
\newdimen\headseptitle

\topmargintitle     = -1in
\textheighttitle    = 26cm
\headseptitle       = 0cm

\newdimen\topmargincontents
\newdimen\textheightcontents
\newdimen\headsepcontents

\topmargincontents     = 4cm
 \addtolength\topmargincontents{-1in}
\headsepcontents       = 1.5cm
 \addtolength\headsepcontents{-\headheight}
\textheightcontents    = \paperheight
  \addtolength\textheightcontents {-4cm}
  \addtolength\textheightcontents {-\realbottommargin}

\renewcommand\baselinestretch{\baselinestretchnormal}  

%\addto\captionsspanish{%
\addto\captionsenglish{%
    \def\prefacename{Prefacio}% 
  \def\refname{Referencias}%
  \def\abstractname{Abstract}%
  \def\bibname{Bibliograf�a}%
  \def\chaptername{Cap�tulo}%
  \def\appendixpartname{Ap�ndices}%
  \def\appendixname{Ap�ndice}%
  \def\contentsname{Tabla de Contenidos}%
  \def\listfigurename{Lista de Figuras}%
  \def\listtablename{Lista de Tablas}%
  \def\indexname{�ndice}%
  \def\figurename{Figura}%
  \def\tablename{Tabla}%
  \def\partname{Parte}%
  \def\enclname{Adjunto}%
  \def\ccname{Copia a}%
  \def\headtoname{A}% 
  \def\pagename{P�gina}%
  \def\seename{ver}%          
  \def\alsoname{ver tambi�n}%
  \def\dedicaname{Dedicatoria}%
  \def\agradename{Agradecimientos}%
  \def\resumenname{Resumen}%
  \def\introname{Introducci�n}%
  \def\pucch{Pontificia Universidad Catolica de Chile}
  \def\escdeing{Escuela de Ingenieria}
  \def\deptodeing{Departamento de}
  \def\tesislineonetitle{Tesis presentada a la Direcci�n de Investigaci�n y Postgrado de la Escuela de Ingenier�a de la Pontificia Universidad Cat�lica de Chile para optar al grado acad�mico de Magister en Ciencias de la Ingenier�a}
  \def\gradename{}
  \def\profsup{Supervisor:}
  \def\city{Santiago de Chile}
  \def\monthname{\ifcase\month\or Enero\or Febrero\or Marzo\or 
  Abril\or Mayo\or Junio\or Julio\or Agosto\or Septiembre\or Octubre\or Noviembre\or Diciembre\fi}
  \def\tesislineonecom{Integrantes del Comit�:}
  \def\tesislinetwocom{Tesis presentada a la Direcci�n de Investigaci�n y Postgrado de la Escuela de Ingenier�a de la Pontificia Universidad Cat�lica de Chile para optar al grado acad�mico de Magister en Ciencias de la Ingenier�a}
  }%

\def\department#1{\def\@department{#1}}
\def\profesorsupervisor#1{\def\@profsupervisor{#1}}
\def\primerprofesorintegrante#1{\def\@primerprofintegrante{#1}}
\def\segundoprofesorintegrante#1{\def\@segundoprofintegrante{#1}}
\def\profesorinvitado#1{\def\@profinvitado{#1}}
\def\jefeprograma#1{\def\@jefeprograma{#1}}

\def\ps@plain{\let\@mkboth\@gobbletwo
     \let\@oddhead\@empty
     \def\@oddfoot{\reset@font\hfil\thepage\hfil}
     \let\@evenhead\@empty
     \let\@evenfoot\@oddfoot}

\def\ps@headings{\let\@mkboth\@gobbletwo
 \let\@oddfoot\@empty\let\@evenfoot\@empty % No feet.
 \def\@evenhead{\reset@font\thepage\hfil}         % Left heading.
 \def\@oddhead{\hfil\reset@font\thepage}          % Right heading.
 \def\chaptermark##1{}%
 \def\sectionmark##1{}
}

\def\ps@title{
  \let\@oddhead\@empty
  \def\@evenhead{\@oddhead}
  \let\@oddfoot\@empty
  \def\@evenfoot{\@oddfoot}
}

\newif\if@pagecontents \@pagecontentsfalse
\newif\if@nofirstpagecontents \@nofirstpagecontentsfalse
\def\ps@contents{
  \def\@oddhead{\hbox{}\hfil\reset@font\pagename}
  \def\@evenhead{\@oddhead}
  \def\@oddfoot{\reset@font\hfil\thepage\hfil}
  \def\@evenfoot{\@oddfoot}
  \@pagecontentstrue
}

\pagestyle{headings}

\pagenumbering{arabic}

%
% Counters
%
\setcounter{secnumdepth}{3} % Nivel de profundidad de numeracion de secciones
\setcounter{tocdepth}{3} % Nivel de profundidad de secciones en la
                         % tabla de contenidos

\newcommand\Thechapter  {\Roman{chapter}}
%\renewcommand\thechapter  {\arabic{chapter}}
% by pitillo
\renewcommand\thechapter  {\Roman{chapter}}

\renewcommand\labelenumi  {\theenumi)}
\renewcommand\theenumi    {\alph{enumi}}

\renewcommand\labelenumii {\theenumii)}
\renewcommand\theenumii   {\roman{enumii}}
\renewcommand\p@enumii    {\theenumi}

\renewcommand\labelenumiii{\theenumiii.}
\renewcommand\theenumiii  {\arabic{enumiii}}
\renewcommand\p@enumiii   {\theenumi(\theenumii)}

\renewcommand\labelenumiv {\theenumiv.}
\renewcommand\theenumiv   {\Alph{enumiv}}
\renewcommand\p@enumiv    {\p@enumiii\theenumiii}

\renewcommand\labelitemi   {\bf --}
\renewcommand\labelitemii  {\bf -}
\renewcommand\labelitemiii {-}
\renewcommand\labelitemiv  {$\m@th\cdot$}

\def\@sect#1#2#3#4#5#6[#7]#8{\ifnum #2>\c@secnumdepth
     \def\@svsec{}\else
     \refstepcounter{#1}\edef\@svsec{\hbox to 1cm{\csname the#1\endcsname}\hskip 1em }\fi
     \@tempskipa #5\relax
      \ifdim \@tempskipa>\z@
        \begingroup #6\relax
          \@hangfrom{\hskip #3\relax\@svsec}{\interlinepenalty \@M #8\par}%
        \endgroup
       \csname #1mark\endcsname{#7}\addcontentsline
         {toc}{#1}{\ifnum #2>\c@secnumdepth \else
                      \protect\numberline{\csname the#1\endcsname}\fi
                    #7}\else
        \def\@svsechd{#6\hskip #3\relax  %% \relax added 2 May 90
                   \@svsec #8\csname #1mark\endcsname
                      {#7}\addcontentsline
                           {toc}{#1}{\ifnum #2>\c@secnumdepth \else
                             \protect\numberline{\csname the#1\endcsname}\fi
                       #7}}\fi
     \@xsect{#5}}

\def\@startsection#1#2#3#4#5#6{\if@noskipsec \leavevmode \fi
  \par \@tempskipa #4\relax
  \@afterindenttrue
  \ifdim \@tempskipa <\z@ 
    \@tempskipa -\@tempskipa \@afterindenttrue
  \fi
  \if@nobreak \everypar{}\else
     \addpenalty{\@secpenalty}\addvspace{\@tempskipa}
  \fi 
  \@ifstar
     {\@ssect{#3}{#4}{#5}{#6}}
     {\@dblarg{\@sect{#1}{#2}{#3}{#4}{#5}{#6}}}}

\def\@makechapterhead#1{%
  \edef\@svsec{\hbox to 1cm{\Thechapter.}\hskip 1em }
  \begingroup \normalsize\bf\relax
     \@hangfrom{\hskip \z@\relax\@svsec}
     {\interlinepenalty \@M \MakeUppercase{#1}\par}%
  \endgroup
  \bigskip\bigskip
  }


\def\@makeschapterhead#1{%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    {\bf \MakeUppercase{{#1}}}\par\nobreak
    \bigskip
    }}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\bibliochapter{\cleardoublepage
  \global\@topnum\z@
  \@afterindenttrue
  \secdef\@bibliochapter\@schapter
  }

\def\@bibliochapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
  \refstepcounter{chapter}%
  %\typeout{\@chapapp\space\thechapter.}%
  \addcontentsline{toc}{chapter}{#1}
  \addtocontents{lof}%
  {\protect\addvspace{10\p@}}% Adds between-chapter space
  \addtocontents{lot}%
  {\protect\addvspace{10\p@}}% to lists of figs & tables.
  \if@twocolumn
  \@topnewpage[\@makebibliochapterhead{#2}]%
  \else \@makebibliochapterhead{#2}%
  \@afterheading
  \fi
  }

\def\@makebibliochapterhead#1{%
  %\edef\@svsec{\hbox to 1cm{\Thechapter.}\hskip 1em }
  \begingroup \normalsize\bf\relax
     %\@hangfrom{\hskip \z@\relax\@svsec}
     {\begin{center}\interlinepenalty \@M \MakeUppercase{#1}\par\end{center}}%
  \endgroup
  \bigskip\bigskip
  }

\def\@makesbibliochapterhead#1{%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    {\bf \MakeUppercase{{#1}}}\par\nobreak
    \bigskip
    }}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
      
    
\def\chapter{\cleardoublepage
  \global\@topnum\z@
  \@afterindenttrue
  \secdef\@chapter\@schapter
  }

\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
  \refstepcounter{chapter}%
  \typeout{\@chapapp\space\thechapter.}%
  \addcontentsline{toc}{chapter}{\protect
    \numberline{\Thechapter}{#1}}\else
  \addcontentsline{toc}{chapter}{#1}\fi
  \addtocontents{lof}%
  {\addvspace{10\p@}}% Adds between-chapter space
  {\protect\addvspace{10\p@}}% Adds between-chapter space
  \addtocontents{lot}%
  {\protect\addvspace{10\p@}}% to lists of figs & tables.
  {\addvspace{10\p@}}% to lists of figs & tables.
  \if@twocolumn
  \@topnewpage[\@makechapterhead{#2}]%
  \else \@makechapterhead{#2}%
  \@afterheading
  \fi
  }

\def\@schapter#1{
  \if@twocolumn \@topnewpage[\@makeschapterhead{#1}]%
  \else \@makeschapterhead{#1}%
  \@afterheading\fi
  }

\def\section{\@startsection {section}{1}{\z@}
  {-3.5ex plus -1ex minus -.2ex}
  {2.3ex plus .2ex}
  {\normalfont\normalsize\bf}}
\def\subsection{\@startsection{subsection}{2}{\z@}
  {-3.25ex plus-1ex minus-.2ex}
  {1.5ex plus.2ex}
  {\normalfont\normalsize\bf}}
\def\subsubsection{\@startsection{subsubsection}{3}{\z@}
  {-3.25ex plus -1ex minus-.2ex}
  {1.5ex plus.2ex}
  {\normalfont\normalsize\bf}}
\def\paragraph{\@startsection
  {paragraph}{4}{\z@}
  {3.25ex plus1ex minus.2ex}
  {-1em}
  {\normalfont\normalsize\bf}}
\def\subparagraph{\@startsection
  {subparagraph}{4}
  {\parindent}
  {3.25ex plus1ex minus .2ex}
  {-1em}
  {\normalfont\normalsize\bf}}
  

\long\def\@makecaption#1#2{%
   \vskip 10\p@
   \setbox\@tempboxa\hbox{#1 #2}%
   \ifdim \wd\@tempboxa >\hsize
       #1 #2\par
     \else
       \hbox to\hsize{\hfil\box\@tempboxa\hfil}%
   \fi}

\long\def\@caption#1[#2]#3{\par\addcontentsline{\csname
    ext@#1\endcsname}{#1}{
    {\csname #1name\endcsname}
    \protect\numberline{\csname the#1\endcsname}{\ignorespaces #2}}
  \begingroup
  \@parboxrestore
  \normalsize
  \@makecaption{\csname fnum@#1\endcsname}{\ignorespaces #3}\par
  \endgroup}

\def\l@figure{\@dottedtocline{1}{1.5em}{2.3em}}
\let\l@table\l@figure
\def\l@chapter#1#2{\addpenalty{-\@highpenalty}%
   \vskip 1.0em plus\p@
   \@tempdima 2.5em
   \begingroup
     \parindent \z@ \rightskip \@pnumwidth
     \parfillskip -\@pnumwidth
     \bf
     \leavevmode
      \advance\leftskip\@tempdima
      \hskip -\leftskip
     \MakeUppercase{#1}\nobreak\leaders\hbox{$\m@th \mkern \@dotsep mu.\mkern \@dotsep
       mu$}\hfill \nobreak\hbox to\@pnumwidth{\hss \MakeUppercase{#2}}\par
     \penalty\@highpenalty
   \endgroup}

\def\l@section{\@dottedtocline{1}{2.5em}{2.5em}}
\def\l@subsection{\@dottedtocline{2}{5.0em}{3.5em}}
\def\l@subsubsection{\@dottedtocline{3}{9.0em}{3.8em}}
\def\l@paragraph{\@dottedtocline{4}{10.7em}{5em}}
\def\l@subparagraph{\@dottedtocline{5}{16.6em}{6em}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewenvironment{titlepage}{ \null }{ \null }

\def\maketitle
{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% TAPA
%\singlespacing
\onehalfspacing

\pagestyle{empty}
\addtolength{\oddsidemargin}{2.3cm}
\addtolength{\topmargin}{1cm}

\setcounter{page}{-1}
\vspace*{-1.7in}
\hspace*{-1.5in}
\begin{tabular*}{18.55cm}{c|c}

\begin{minipage}{3.6cm}
%\begin{center}\resizebox{2.5cm}{!}{\includegraphics{logouc.ps}}\end{center}\vspace*{-1pt}
\begin{center}\resizebox{2.2cm}{!}{\includegraphics{logouc.ps}}\end{center}\vspace*{10pt}
\end{minipage}
& 
\begin{minipage}{15cm}
\setlength{\parindent}{10pt}
\vspace*{-40pt}
{\fontsize{14}{14} \MakeUppercase{\pucch}} \\

\vspace*{-10pt}
{\fontsize{14}{14} \MakeUppercase{\escdeing}}
\end{minipage}
\vspace*{-15pt}
\\ \hline
\begin{minipage}{3.6cm}
\hfill 
\end{minipage} 
&
\hspace{-0.3cm}
\begin{minipage}[t]{13cm}
\setlength{\parindent}{10pt}

\vspace{3.2cm}
\begin{center}
  {\fontshape{bf} \fontsize{16}{26} \bf \@title \\}
  \vskip 3.8cm
{\large POR}
\bigskip 

  {\large \fontshape{bf} \fontsize{13}{14} \MakeUppercase{\bf \@author}}
    \end{center}
    
    \vspace*{2.2cm}
    \noindent 
    \begin{flushleft}
    {\fontsize{10}{40}Tesis presentada a la Direcci�n de Investigaci�n y Postgrado de la Escuela de Ingenier�a de la Pontificia Universidad Cat�lica de Chile para optar al grado acad�mico de Magister en Ciencias de la Ingenier�a\par}
    \end{flushleft}
    
    \vskip 1cm
	\noindent \profsup \\ \MakeUppercase{\bf \@profsupervisor}\par
    
    \vskip 1.0cm
    \noindent \city, \monthname, \number\year \par
    \vfill
\end{minipage}
\end{tabular*}
\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% COMISION

\pagestyle{empty}
\vspace*{-1.7in}
\hspace*{-1.5in}
\begin{tabular*}{18.55cm}{c|c}

\begin{minipage}{3.6cm}
\begin{center}\resizebox{2.2cm}{!}{\includegraphics{logouc.ps}}\end{center}\vspace*{10pt}
\end{minipage}
& 
\begin{minipage}{15cm}
\setlength{\parindent}{10pt}
%\vspace*{-5pt}
\vspace*{-15pt}
{\fontsize{14}{14} \MakeUppercase{\pucch}} \\

\vspace*{-15pt}
{\fontsize{14}{14} \MakeUppercase{\escdeing}} \\

\vspace*{-15pt}
{\fontsize{14}{14} \deptodeing \ \@department}
\end{minipage}
\vspace*{-15pt}
\\ \hline
\begin{minipage}{3.6cm}
\hfill
\end{minipage} 
&
\hspace{-0.3cm}
\begin{minipage}[t]{13cm}
\setlength{\parindent}{5pt}

\vspace{2.2cm}
\begin{center}
  {\fontshape{bf} \fontsize{16}{26} \bf \@title \\}
  \vskip 2.8cm
{\large POR}\\
\bigskip 
  {\large \fontshape{bf} \fontsize{13}{14} \MakeUppercase{\bf \@author}}
    \end{center}
    
    \vskip 2.7cm
    \noindent Integrantes del Comit�: \par \vskip 0.5cm
    \noindent \MakeUppercase{\bf \@profsupervisor } \par \vskip 0.3cm
    \noindent \MakeUppercase{\bf \@primerprofintegrante} \par \vskip 0.3cm
    \noindent \MakeUppercase{\bf \@segundoprofintegrante} \par \vskip 0.3cm
    \noindent \MakeUppercase{\bf \@profinvitado} \par \vskip 0.3cm
    \noindent Jefe del Programa de Postgrado: \par \vskip 0.5cm
    \noindent \MakeUppercase{\bf \@jefeprograma} \par \vskip 0.3cm
    \vspace*{1.2cm}
    \noindent 
%    \begin{flushleft}
%    Tesis presentada a la Direcci�n de Investigaci�n y Postgrado de la Escuela de Ingenier�a de la Pontificia Universidad Cat�lica de %Chile para optar al grado acad�mico de Magister en Ciencias de la Ingenier�a\par
%    \end{flushleft}
    
    \vskip 0.5cm
    \noindent \city, \monthname, \number\year \par

\end{minipage}
\end{tabular*}
\newpage

\addtolength{\topmargin}{-1cm}
\addtolength{\oddsidemargin}{-2.3cm}
\onehalfspacing
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\InputIndex{\addcontentsline{toc}{chapter}{\indexname}
  \InputIfFileExists{\jobname.ind}{}
  {\typeout{No file \jobname.ind.}}
  }

\renewcommand\tableofcontents{
  \addcontentsline{toc}{chapter}{Tabla de Contenidos}
  \onehalfspacing
  \parskip 1pt
  \cleardoublepage
  \def\thepage{\csname @roman\endcsname \c@page}
  \thispagestyle{plain}
  \pagestyle{contents}

  \@restonecolfalse
  \if@twocolumn\@restonecoltrue\onecolumn\fi
  \centerline{\bf\MakeUppercase{Tabla de Contenidos}}%\contentsname}}%
  \begin{flushright} \pagename \end{flushright}\par
  \@starttoc{toc}\if@restonecol\twocolumn\fi

  \newpage
  \def\thepage{\csname @arabic\endcsname \c@page}
  \@pagecontentsfalse
  \@nofirstpagecontentsfalse
  \pagestyle{headings}%
  \parskip 12pt
  \gdef\topmargin{\topmarginnormal}
  \gdef\headsep{\headsepnormal}
  \gdef\textheight{\textheightnormal}
  \@ShowDebug\headsep
  \@ShowDebug\topmargin
  \@ShowDebug\textheight

  \doublespacing
   }

\renewcommand\listoffigures{
  \onehalfspacing
  \parskip 1pt
  \cleardoublepage\newpage
  \def\thepage{\csname @roman\endcsname \c@page}
  \pagestyle{contents}
  \thispagestyle{plain}

  \@restonecolfalse
  \if@twocolumn\@restonecoltrue\onecolumn\fi
  \centerline{\bf\MakeUppercase{\listfigurename}}%
  \begin{flushright} \pagename \end{flushright}\par 
  \addcontentsline{toc}{chapter}{\listfigurename}
  \@starttoc{lof}
  \if@restonecol\twocolumn\fi

  \newpage
  \def\thepage{\csname @arabic\endcsname \c@page}
  \@pagecontentsfalse
  \@nofirstpagecontentsfalse
  \pagestyle{headings}
  \parskip 12pt
  \gdef\topmargin{\topmarginnormal}
  \gdef\headsep{\headsepnormal}
  \gdef\textheight{\textheightnormal}
  \@ShowDebug\headsep
  \@ShowDebug\topmargin
  \@ShowDebug\textheight
  \doublespacing
  }

\renewcommand\listoftables{
  \onehalfspacing
  \clearpage
  \parskip 1pt
  \cleardoublepage\newpage
  \def\thepage{\csname @roman\endcsname \c@page}
  \pagestyle{contents}
  \thispagestyle{plain}

  \@restonecolfalse
  \if@twocolumn\@restonecoltrue\onecolumn\fi

  \centerline{\bf\MakeUppercase{\listtablename}}%
  
  \begin{flushright} \pagename \end{flushright}\par
  \addcontentsline{toc}{chapter}{\listtablename}
  \@starttoc{lot}
  \if@restonecol\twocolumn\fi

  \newpage
  \def\thepage{\csname @arabic\endcsname \c@page}
  \@pagecontentsfalse
  \@nofirstpagecontentsfalse
  \pagestyle{headings}%
  \parskip 12pt
  \gdef\topmargin{\topmarginnormal}
  \gdef\headsep{\headsepnormal}
  \gdef\textheight{\textheightnormal}
  \@ShowDebug\headsep
  \@ShowDebug\topmargin
  \@ShowDebug\textheight
  \doublespacing
  }

\newenvironment{textualcite}{
  \begin{center}
  \vskip -1em
  \singlespacing
  \begin{minipage}{\citetextwidth}
  ``\nolinebreak
}
{ 
  \nolinebreak''
  \end{minipage}
  \end{center}
  \doublespacing
}
  
\newenvironment{dedicatory}{
  \cleardoublepage
  \def\thepage{\csname @roman\endcsname \c@page}
  \pagestyle{plain}

  \addcontentsline{toc}{chapter}{\dedicaname}
%  \hbox{}\vskip 400\p@
  \hbox{}\vskip 500\p@
  \flushright
  }{
  \endflushright
  \pagebreak
  \def\thepage{\csname @arabic\endcsname \c@page}
  \pagestyle{headings}%
  }

\newenvironment{acknowledgment}{
  \cleardoublepage
  \def\thepage{\csname @roman\endcsname \c@page}
  \pagestyle{plain}%

  \@restonecolfalse
  \if@twocolumn\@restonecoltrue\onecolumn\fi
  \centerline{\bf\MakeUppercase{\agradename}}%
  \addcontentsline{toc}{chapter}{\agradename}
  \onehalfspacing
  \vskip 2.5em
  }
  {
  \newpage
  \def\thepage{\csname @arabic\endcsname \c@page}
  \pagestyle{headings}%
  \doublespacing
  }

\newenvironment{resumen}{
  \cleardoublepage
  \def\thepage{\csname @roman\endcsname \c@page}
  \thispagestyle{plain}%

  \@restonecolfalse
  \if@twocolumn\@restonecoltrue\onecolumn\fi
  \centerline{\bf\MakeUppercase{\resumenname}}%
  \addcontentsline{toc}{chapter}{\resumenname}
%  \selectlanguage{spanish} 
  \onehalfspacing
  \vskip 2.5em
  }{
%  \selectlanguage{english}
  \newpage
  \def\thepage{\csname @arabic\endcsname \c@page}
  \pagestyle{headings}
  \doublespacing
  }

\newenvironment{abstract}{
  \cleardoublepage
  \def\thepage{\csname @roman\endcsname \c@page}
  \pagestyle{plain}%

  \@restonecolfalse
  \if@twocolumn\@restonecoltrue\onecolumn\fi
  \centerline{\bf\MakeUppercase{\abstractname}}%
  \addcontentsline{toc}{chapter}{\abstractname}
  \onehalfspacing
  \vskip 2.5em
  }{
  \newpage
  \def\thepage{\csname @arabic\endcsname \c@page}
  \pagestyle{headings}%
  \doublespacing
  \setcounter{page}{1}
  \pagenumbering{arabic}
  }

\newenvironment{enumeratewithnumbers}
{
  \renewcommand{\labelenumi}{\theenumi.}
  \renewcommand{\theenumi}{\arabic{enumi}}
  \renewcommand{\labelenumii}{\theenumii)}
  \renewcommand{\theenumii}{\alph{enumii}}
  \renewcommand{\labelenumiii}{\theenumiii)}
  \renewcommand{\theenumiii}{\roman{enumiii}}
  \begin{enumerate}
}
{
  \end{enumerate}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewenvironment{itemize}
{
  \begin{list}{\bf --}{
  \setlength{\labelsep}{20pt}
  \setlength{\itemindent}{0pt}
  \setlength{\leftmargin}{1cm}
  \setlength{\labelwidth}{5pt}
  }
}
{
  \end{list}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newdimen\bibindent
\setlength\bibindent{1.5em}
\renewenvironment{thebibliography}[1]
     {\bibliochapter{\bibname}%
      %\addcontentsline{toc}{chapter}{\bibname}%
      \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\setlength{\leftmargin}{0pt}
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\appendixpart{
  \cleardoublepage
  \thispagestyle{empty}
  \setcounter{chapter}{0}%
  \def\Thechapter{\Alph{chapter}}
  \def\thechapter{\Alph{chapter}}

  \addcontentsline{toc}{chapter}{\appendixpartname}
  \addtocontents{lof}%
  {\protect\addvspace{10\p@}}% Adds between-chapter space
  \addtocontents{lot}%
  {\protect\addvspace{10\p@}}% to lists of figs & tables.

  \if@twocolumn
  \onecolumn
  \@tempswatrue
  \else \@tempswafalse
  \fi
  \hbox{}\vfil
  \centerline{\Large \bf \MakeUppercase\appendixpartname}\par
  \vfil\newpage
  \if@twoside
    \hbox{}%
    \thispagestyle{empty}%
    \newpage
  \fi
  \if@tempswa\twocolumn\fi
  }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\def\appendix{\cleardoublepage
  \setcounter{section}{0}
  \if@twocolumn
     \onecolumn
     \@tempswatrue
    \else \@tempswafalse
  \fi
  
  %\hbox{}\vfil
  \secdef\@appendix\@sappendix}

\def\@appendix[#1]#2{
  \ifnum \c@secnumdepth >-2\relax
  
  \refstepcounter{chapter}%
  \addcontentsline{toc}{chapter}{\appendixname\space\Thechapter:\hspace{1em}#1}
  \else
  \addcontentsline{toc}{chapter}{#1}
  \fi
  \addtocontents{lof}%
  {\protect\addvspace{10\p@}}% Adds between-chapter space
  \addtocontents{lot}%
  {\protect\addvspace{10\p@}}% to lists of figs & tables.

  %{\centering
  %  \interlinepenalty \@M
  %  \ifnum \c@secnumdepth >-2\relax
  %  {\large\bf \MakeUppercase{\appendixname}~\Thechapter}
  %  \par\vskip 10\p@ \fi}
  
  %\centerline{\large\bf{#2}}\par
  %\@endappendix
  \begin{center}
  {\large\bf \MakeUppercase{\appendixname}~\Thechapter:~#2}
  \end{center}
  \vskip 1em
  }

\def\@sappendix#1{{\centering
    \interlinepenalty \@M
    \Huge \bf
    #1\par}\@endappendix}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
\def \@shipoutsetup {%
     \@resetactivechars
     \let\-\@dischyph
     \let\'\@acci\let\`\@accii\let\=\@acciii
     \let\\\@normalcr
     \if@specialpage
       \global\@specialpagefalse\@nameuse{ps@\@specialstyle}%
     \fi
     \if@twoside
       \ifodd\count\z@ \let\@thehead\@oddhead \let\@thefoot\@oddfoot
            \let\@themargin\oddsidemargin
       \else \let\@thehead\@evenhead
          \let\@thefoot\@evenfoot \let\@themargin\evensidemargin
       \fi
     \fi
     \reset@font
     \normalsize
     \baselineskip\z@skip \lineskip\z@skip \lineskiplimit\z@
     \let\par\@@par          %% 15 Sep 87
}

\def\@outputpage{%
  \def\@ShowDebug##1{}

  \let \protect \@unexpandable@protect
  \shipout \vbox{%
    \let \protect \relax
    \aftergroup\let \aftergroup\protect \aftergroup\relax
    \@shipoutsetup
    \vskip \topmargin
    \moveright\@themargin \vbox {%
      \setbox\@tempboxa \vbox to\headheight{%
        \vfil
        \color@begingroup
        \normalcolor
        \hbox to\textwidth {%
          \let \label \@gobble
          \let \index \@gobble
          \let \glossary \@gobble %% 21 Jun 91
          \@thehead
          }%
        \color@endgroup
        }%                        %% 22 Feb 87
      \dp\@tempboxa \z@
      \box\@tempboxa
      \vskip \headsep
      \box\@outputbox
      \baselineskip \footskip
      \color@begingroup
      \normalcolor
      \hbox to\textwidth{%
        \let \label \@gobble
        \let \index \@gobble      %% 22 Feb 87
        \let \glossary \@gobble   %% 21 Jun 91
        \@thefoot
        }%
      \color@endgroup
      }%
    }%
  \global \@colht \textheight
                                %      \endgroup
  \stepcounter{page}%
  \let\firstmark\botmark
  \if@pagecontents
    \if@nofirstpagecontents
      \gdef\topmargin{\topmargincontents}
      \gdef\textheight{\textheightcontents}
      \gdef\headsep{\headsepcontents}
      \@ShowDebug\headsep
      \@ShowDebug\topmargin
      \@ShowDebug\textheight
    \else
    \fi
    \global \@nofirstpagecontentstrue
  \fi
  }


\endinput





