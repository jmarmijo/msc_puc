\chapter{Modelos 3D}
\label{chap.Modelos3D}

La generaci�n de modelos 3D con textura requiere la transformaci�n de los datos entregados por el l�ser, que se encuentran en distintos sistemas de coordenadas, a s�lo uno, para luego, proceder a la reconstrucci�n de la superficie del modelo usando pol�gonos. Despu�s de esto se obtienen los par�metros de calibraci�n de la c�mara que permite encontrar la relaci�n entre las im�genes y el espacio 3D. Finalmente, usando estos par�metros se a�ade la textura a la superficie reconstruida.

A lo largo de este cap�tulo se expone la forma en que se resuelven los puntos mencionados anteriormente. M�s en detalle, en la Secci�n~\ref{sec.Modelos3D.Coords} se muestra c�mo se realizan los cambios de coordenadas y en la Secci�n~\ref{sec.Modelos3D.Reconst} se detalla c�mo se generan los modelos tridimensionales, a�n sin textura. En la Secci�n~\ref{sec.Modelos3D.Calib} se explica el m�todo empleado para calibrar la c�mara digital y en la Secci�n~\ref{sec.Modelos3D.Textura} se explica c�mo se agrega textura a los modelos reconstruidos.

\section{Cambio de coordenadas}
\label{sec.Modelos3D.Coords}

Los datos pertenecientes a distintos barridos del l�ser se encuentran en distintos sistemas de coordenadas. Antes de poder crear superficies en base a estos datos es necesario que todos ellos se encuentren sobre un mismo sistema de coordenadas.

Como se indic� en el cap�tulo anterior, los rayos emitidos por el l�ser se encuentran sobre un mismo plano en el espacio. Para obtener datos tridimensionales, el l�ser es rotado con un movimiento \begin{it}tilt\end{it} en sentido horario en un intervalo de 45� entre 22,5� y -22,5� con respecto a la horizontal dividido en 128 partes iguales, obteniendo 129 planos de mediciones no paralelos entre s�, como se observa en la Figura~\ref{f.barridos.a}.

Cuando el l�ser es rotado, el punto de emisi�n de rayos se desplaza de acuerdo a la trayectoria que se observa en la Figura~\ref{f.barridos.b}, que corresponde a un arco de circunferencia. El centro de este arco se obtiene intersecando el eje de rotaci�n del l�ser con el plano cuya normal coincide con este eje y que contiene a los distintos puntos de emisi�n de rayos. Si el punto de origen de los rayos del l�ser no es el mismo en todos los barridos, como se acaba de describir, la reconstrucci�n de modelos se vuelve extremadamente compleja. Sin embargo, dada la relaci�n expuesta entre estos puntos, es posible establecer uno que pueda ser utilizado como origen com�n a los rayos de todos los barridos y aplicar las transformaciones correspondientes sobre los valores entregados por el l�ser para realizar las reconstrucciones.

\begin{figure}[htbp]
  \begin{center}
    \subfigure[Movimiento tilt]{\label{f.barridos.a}\includegraphics[height=40mm]{imagenes/barridoslaser.eps}}
    \hspace{2.0cm}
    \subfigure[Rotaci�n del origen]{\label{f.barridos.b}\includegraphics[height=40mm]{imagenes/lasercentros.eps}}
  \end{center}
  \caption[Movimiento tilt del l�ser en sentido horario.]{Movimiento tilt del l�ser en sentido horario. Las l�neas que parten del l�ser corresponden a s�lo algunos barridos de �ste, en diferentes �ngulos. (a) Los barridos que se muestran aparecen numerados en el orden en que se realizan. (b) La flecha, que se halla sobre una circunferencia, indica el recorrido del origen de los rayos.}
  \label{f.barridos}
\end{figure}

Para ello, por cada barrido $b$ del l�ser se define un sistema de coordenadas esf�ricas $(\rho_b, \varphi_b, \theta_b)$ con centro en $O_b$, como se muestra en la Figura~\ref{f.coords.a}, donde $\theta_b$ corresponde al �ngulo de inclinaci�n del plano que contiene a $b$, $\varphi_b$ al �ngulo en que se realiza cada medici�n de $b$, $\rho_b$ al valor obtenido y $O_b$ al punto de origen de los rayos de las mediciones.

\begin{figure}[htbp]
  \begin{center}
    \subfigure[Sistema de coordenadas de cada barrido]{\label{f.coords.a}\includegraphics[height=40mm]{imagenes/coordesfericas.eps}}
    \hspace{1.5cm}
    \subfigure[Sistema de coordenadas general]{\label{f.coords.b}\includegraphics[height=60mm]{imagenes/esfericasgeneral.eps}}
  \end{center}
  \caption[Sistemas de coordenadas.]{Sistemas de coordenadas. El l�ser se haya inclinado $\theta_{b,q}$ grados sobre la horizontal y mide un punto $q$ en $\varphi_{b,q}$ grados a una distancia $\rho_{b,q}$. Se desea encontrar los valores para $\rho_q$, $\varphi_q$ y $\theta_q$ en base a los valores de $\rho_{b,q}$, $\varphi_{b,q}$ y $\theta_{b,q}$ y a la distancia entre $O_b$ y $O$, el centro del nuevo sistema.}
  \label{f.coords}
\end{figure}

Por la forma en que son definidos, todos los sistemas de coordenadas esf�ricas descritos anteriormente tienen sus ejes directores correspondientes orientados en forma paralela entre s�, siendo su �nica diferencia la posici�n en el espacio de sus or�genes. En consecuencia, para tener de una relaci�n directa entre todos los datos del l�ser se define un nuevo sistema de coordenadas esf�ricas $(\rho, \varphi, \theta)$, cuyos ejes directores son tambi�n paralelos a los otros sistemas, y cuyo origen $O$ corresponde al centro de la circunferencia descrita anteriormente, de modo que el cambio de coordenadas para todos los puntos obtenidos por el l�ser se obtiene a trav�s de las siguientes ecuaciones:

$$\rho = \sqrt{\left(\rho_b \sin{\theta_b} - \delta_2\right)^2 + r^2}$$
$$\varphi = \arcsin{\left(\frac{\rho_b \cos{\theta_b} \sin{\varphi_b} + \delta_1}{r}\right)}$$
$$\theta = \arctan{\left( \frac{\rho_b \arcsin{\theta_b} - \delta_2}{r}\right)}$$

Con $r = \sqrt{   \delta_1^2 + {\rho_b \cos{\theta_b}^2 - 2 \delta_1 \rho_b \cos{\theta_b}} \cos{90�+\varphi_b}  }$

$\rho$, $\varphi$ y $\theta$ se encuentran mediante ecuaciones trigonom�tricas simples, mientras que $\delta_1$ y $\delta_2$ son constantes cuyo valor corresponde a la separaci�n entre $O_b$ y $O$. Una representaci�n gr�fica de este nuevo sistema se puede observar en la Figura~\ref{f.coords.b}.

Finalmente, luego de aplicar esta transformaci�n a todas las mediciones del l�ser y lograr que todos los puntos se encuentren en este sistema de coordenadas esf�ricas com�n, se realiza un nuevo cambio de coordenadas, en esta ocasi�n a coordenadas rectangulares, con el fin de facilitar los c�lculos posteriores.


\section{Reconstrucci�n de superficies}
\label{sec.Modelos3D.Reconst}

Una vez que se tienen todas las mediciones del l�ser en un mismo sistema de coordenadas, se procede a generar las superficies de los modelos. Para ello, en base a estas mediciones se generan pol�gonos que, unidos entre s�, conforman las superficies.

La generaci�n de pol�gonos requiere que todos los puntos obtenidos por el l�ser formen grupos de cuatro puntos, donde cada grupo, compuesto por las mediciones obtenidas en $\varphi_i$, $\theta_j$; $\varphi_{i+1}$, $\theta_j$; $\varphi_{i+1}$, $\theta_{j+1}$ y $\varphi_i$, $\theta_{j+1}$; para $i=1, \cdots, 361$ y $j=1, \cdots, 129$, generan un cuadril�tero tridimensional cuyos v�rtices no necesariamente se encuentran sobre un mismo plano en el espacio. La manera en que se seleccionan los puntos se muestra en la Figura~\ref{f.poligonos}.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=35mm]{imagenes/poligonos.eps}
  \end{center}
  \caption[Selecci�n de pol�gonos.]{Selecci�n de pol�gonos. Cada fila de puntos corresponde a un barrido del l�ser, y los puntos de la fila son las mediciones del barrido correspondiente. Se selecciona un cuadril�tero con v�rtices en $\varphi_i$, $\theta_j$; $\varphi_{i+1}$, $\theta_j$; $\varphi_{i+1}$, $\theta_{j+1}$ y $\varphi_i$, $\theta_{j+1}$.}
  \label{f.poligonos}
\end{figure}

Sin embargo, el texturizado de los modelos requiere que los pol�gonos que forman las superficies se encuentren �ntegramente sobre un plano, por lo que los cuadril�teros obtenidos no pueden utilizarse directamente, ya que no es posible asegurar que sus cuatro v�rtices se encuentren sobre un mismo plano. Para resolver este problema, se dividen los cuadril�teros a trav�s de una de sus diagonales, con el fin de obtener dos tri�ngulos no superpuestos entre s�, cada uno de los cuales se encuentra contenido dentro de un plano.

Para dividir el cuadril�tero podr�a utilizarse cualquiera de sus dos diagonales, pero en este caso se decide utilizar la que produce una menor diferencia entre las �reas de los tri�ngulos generados, ya que, dado el per�metro de un tri�ngulo, su �rea ser� m�xima si corresponde a un tri�ngulo equil�tero, lo que, a su vez, indica que mientras mayor sea su simetr�a mayor ser� su �rea. As�, la diagonal seleccionada busca que los tri�ngulos obtenidos maximicen su simetr�a con respecto a s� mismos y al segundo tri�ngulo, dadas las restricciones impuestas por el cuadril�tero que los define.

Sin embargo, al generar las superficies de esta manera, no se hace distintinci�n entre distintos objetos, y todos ellos se reconstruyen unidos entre s�. Esta situaci�n se refleja en la Figura~\ref{f.estanteparedjuntos}. En la pr�ctica, con los sensores utilizados no es posible deducir inequ�vocamente si un tri�ngulo dado de la superficie corresponde a uno u otro objeto en la escena, o al espacio vac�o entre ellos, pero s� es posible tener una idea aproximada para la mayor�a de los casos pr�cticos si se utiliza el largo de las aristas de los tri�ngulos como elemento de decisi�n, considerando que aristas demasiado largas en general unir�an dos puntos de objetos distintos.

\begin{figure}[htbp]
  \begin{tabular}[b]{cc}
        \subfigure[Secci�n superior]{\label{f.estanteparedjuntos.a}\includegraphics[height=30mm]{imagenes/pared2.eps}}
        \hspace{0.5cm}
        \subfigure[Secci�n inferior]{\label{f.estanteparedjuntos.b}\includegraphics[height=30mm]{imagenes/pared3.eps}}
        \hspace{0.5cm}
        \subfigure[Modelo reconstruido]{\label{f.estanteparedjuntos.c}\includegraphics[height=60mm]{imagenes/pared.eps}}
  \end{tabular}
  \caption[Reconstrucci�n de superficies cercanas.]{Reconstrucci�n de superficies cercanas. Las figuras \ref{f.estanteparedjuntos.a} y \ref{f.estanteparedjuntos.b} corresponden a fotograf�as de una escena donde aparece una repisa frente a una pared, separados entre s�. En la figura \ref{f.estanteparedjuntos.c} se presenta una reconstrucci�n texturizada de estos objetos donde la separaci�n entre ellos es reconstruida como si fuera una superficie que une el mueble al muro y, adem�s, texturizada como parte del muro.}
  \label{f.estanteparedjuntos}
\end{figure}

M�s en detalle, si se establece que dos puntos cuyo punto medio se encuentra a distancia $R$ del origen y que estos se hallan a una distancia $d$ entre s� se consideran pertenecientes a la misma superficie, entonces puede aplicarse este mismo criterio sobre cada par de v�rtices de un tri�ngulo y, aplicando el Teorema de Thales, extenderlo de modo que un tri�ngulo se dir� parte de una superficie si, para todas sus aristas, cumple la restricci�n:

$$a_{i,T} <= \frac{L(i,T) \; d}{R}$$

Siendo $a_{i,T}$ el largo de la arista $i$ del tri�ngulo $T$, $L(i,T)$ la distancia desde el origen del sistema de coordenadas $O$ al punto medio de la arista $i$ de $T$, y $d$ y $R$ constantes predefinidas, donde $d$ corresponde al largo m�ximo aceptado para una arista cuyo punto medio se encuentra a distancia $R$ de $O$.

Cuando finalmente se dispone de todos los tri�ngulos que forman parte de las superficies, se procede a clasificarlos de acuerdo a su ubicaci�n en el espacio con el fin de seleccionar en forma eficiente los tri�ngulos de una zona particular, sin necesidad de realizar una b�squeda exhaustiva. Para ello, el espacio tridimensional se divide en una grilla 3D de celdas c�bicas con arista de largo $l$ en las que se insertan los tri�ngulos cuyos centros se encuentran en esa zona del espacio. Por esta raz�n, el valor de $l$ debe ser tan peque�o como sea posible de manera que, en el caso ideal, s�lo exista un tri�ngulo por cada celda de la grilla.

\section{Calibraci�n de la c�mara}
\label{sec.Modelos3D.Calib}

La obtenci�n de los par�metros de la c�mara es necesaria para encontrar la relaci�n entre la informaci�n de distancia entregada por el l�ser y la de textura que se obtiene de las im�genes. Para encontrar estos valores se ejecuta un m�todo de calibraci�n de la c�mara, que utiliza como datos de entrada la posici�n en el espacio de ciertos puntos caracter�sticos (como los v�rtices de un cubo) y los p�xeles correspondientes a dichos puntos en las im�genes obtenidas.

Para ello, es necesario introducir el concepto de \begin{it}C�mara Pinhole\end{it}, que corresponde a una c�mara fotogr�fica que carece de lentes. El modelo b�sico consiste en una caja sellada completamente, a excepci�n de un agujero peque�o por el que entra la luz de modo que en la cara opuesta de la caja, por el interior, se forme una imagen. Si se considera que la c�mara digital es modelada como una \begin{it}C�mara Pinhole\end{it}, es posible aplicar una transformaci�n Euclidiana 3D sobre los puntos del modelo. Esta transformaci�n produce una rotaci�n y traslaci�n de los ejes coordenados de un sistema manteniendo invariantes todas las dem�s propiedades del sistema --distancia entre puntos, �ngulos entre rectas, etc.--, de modo que al aplicarla sobre los puntos del modelo, estos se cambian de coordenadas desde el sistema general al sistema de coordenadas particular de la c�mara.

Como se indic� previamente, el l�ser es rotado en un movimiento \begin{it}tilt\end{it} para realizar barridos en distintas posiciones y la c�mara, al estar montada sobre �l, sufre el mismo movimiento de rotaci�n. En la captura de informaci�n visual se usan tres im�genes en �ngulos distintos predeterminados ($\sim5,6�$, $\sim8,4�$ y $-22,5�$). Esto permite que los barridos del l�ser se encuentren en el campo visual de al menos una de las im�genes para objetos no demasiado cerca del l�ser, tal como se observa en la Figura~\ref{f.visualrange.a}. Sin embargo, debe considerarse que en cada barrido s�lo los �ngulos centrales son visibles en las im�genes, como se muestra en la Figura~\ref{f.visualrange.b}.

\begin{figure}[ht]
  \begin{center}
    \subfigure[Sentido vertical]{\label{f.visualrange.a}\includegraphics[height=40mm]{imagenes/rangoverticalcamara.eps}}
    \hspace{1.5cm}
    \subfigure[Sentido horizontal]{\label{f.visualrange.b}\includegraphics[height=40mm]{imagenes/rangohorizontalcamara.eps}}
  \end{center}
  \caption[Espacio explorado por el l�ser.]{Espacio explorado por el l�ser. Zonas del espacio explorado por el l�ser que se encuentran en el campo visual de la c�mara.}
  \label{f.visualrange}
\end{figure}

Debido a la rotaci�n del l�ser, se tiene que la transformaci�n Euclidiana 3D depende de la posici�n en que la c�mara captura la imagen sobre la que se realiza la proyecci�n. Esta posici�n es conocida, pues depende directamente del �ngulo de rotaci�n del motor. Adem�s, este movimiento se produce precisamente sobre uno de los ejes del sistema de coordenadas general. Por estos motivos, la transformaci�n Euclidiana 3D es modificada con el fin de agregar este �ngulo de rotaci�n, obteniendo as� una transformaci�n Euclidiana por cada imagen capturada por la c�mara. De esta forma, utilizando vectores en coordenadas homog�neas, la funci�n de rotaci�n queda definida de la siguiente manera:

\begin{eqnarray}
\vec M_i' =
\left[
\begin{array}{cc}
\left[R_i\right]_{3 \times 3}   & \vec t_i\\
\vec 0^T & 1
\end{array}
\right]
\vec M
\label{fun.Euclidea3D}
\end{eqnarray}

Donde $\vec M$ corresponde al punto 3D original, $\vec M_i'$ es el punto $\vec M$ en el sistema de coordenadas de la c�mara para la imagen $i$, $\vec t_i = \left[t_{i,x}, t_{i,y}, t_{i,z}\right]^T$ es el vector de traslaci�n del origen del sistema de coordenadas del espacio 3D al de la c�mara para la imagen $i$ y $\left[R_i\right]_{3 \times 3}$ es la matriz de rotaciones entre estos mismos dos sistemas. M�s en detalle, la matriz $R_i$ queda definida por los valores:

\vspace{1cm}
\leftskip=1cm $R_{i,1,1} = cos(\omega_Y) cos(\omega_Z)$ \\
\leftskip=1cm $R_{i,1,2} = cos(\omega_Y) sin(\omega_Z)$ \\
\leftskip=1cm $R_{i,1,3} =-sin(\omega_Y)$ \\
\leftskip=1cm $R_{i,2,1} = sin(\omega_{X_i} + \theta) sin(\omega_Y) cos(\omega_Z) - cos(\omega_X + \theta_i) sin(\omega_Z)$ \\
\leftskip=1cm $R_{i,2,2} = sin(\omega_{X_i} + \theta) sin(\omega_Y) sin(\omega_Z) + cos(\omega_X + \theta_i) cos(\omega_Z)$ \\
\leftskip=1cm $R_{i,2,3} = sin(\omega_{X_i} + \theta) cos(\omega_Y)$ \\
\leftskip=1cm $R_{i,3,1} = cos(\omega_{X_i} + \theta) sin(\omega_Y) cos(\omega_Z) + sin(\omega_X + \theta_i) sin(\omega_Z)$ \\
\leftskip=1cm $R_{i,3,2} = cos(\omega_{X_i} + \theta) sin(\omega_Y) sin(\omega_Z) - sin(\omega_X + \theta_i) cos(\omega_Z)$ \\
\leftskip=1cm $R_{i,3,3} = cos(\omega_{X_i} + \theta) cos(\omega_Y)$
\vspace{1cm}

\leftskip=0cm Siendo $\omega_{X_i}$ el �ngulo que se ha rotado el sistema de coordenadas general sobre el ejes $X$ para la imagen $i$, $\omega_Y$ y $\omega_Z$ los �ngulos que se ha rotado el sistema de coordenadas general sobre los ejes $Y$ y $Z$, respectivamente, y $\theta$ un �ngulo de ajuste de la c�mara sobre el eje $X$. Las variables $t_{i,x}$, $t_{i,y}$, $t_{i,z}$, $\omega_X$, $\omega_Y$ y $\omega_Z$ corresponden a par�metros extr�nsecos de la c�mara, pues no depende de sus caracter�sticas, sino de su posici�n en el espacio.

Luego, en base al Teorema de Thales, es posible definir la funci�n de proyecci�n de $\vec M_i'$ sobre el plano de la imagen $i$ como:

\begin{eqnarray}
\vec m_i =
\left[
\begin{array}{cccc}
f & 0 & 0 & 0 \\
0 & f & 0 & 0 \\
0 & 0 & 1 & 0
\end{array}
\right]
\vec M_i'
\label{fun.Proy}
\end{eqnarray}

Con $f$ la distancia focal de la c�mara y $\vec m_i$ el punto $\vec M_i'$ proyectado sobre el plano de la imagen $i$.

Las ecuaciones encontradas hasta el momento permiten hallar las proyecciones de los puntos 3D sobre las im�genes. Sin embargo, el origen del sistema de coordenadas en la imagen es desconocido, y los puntos obtenidos con esta rotaci�n a�n est�n en la escala original del sistema --mil�metros-- y no en p�xeles, unidad en que se miden las im�genes. Gracias a que la c�mara usada dispone de un sensor CCD mediante el cual se capturan las im�genes, tal como fue indicado Secci�n~\ref{sec.Componentes.Camara}, es posible definir una transformaci�n que corrija estos problemas, que corresponde a:

%Adem�s, existen otros factores a tomar en cuenta, como la distorsi�n provocada por el lente empleado y el origen del sistema de coordenadas en la imagen, entre otros.

\begin{eqnarray}
\vec w_i =
\left[
\begin{array}{cccc}
\alpha_x  & s        & u_0 & 0\\
    0     & \alpha_y & v_0 & 0\\
    0     & 0        & 1   & 0
\end{array}
\right]
m_i
\label{fun.Trans2D2D}
\end{eqnarray}

Donde $\vec w_i$ es el p�xel correspondiente al punto $\vec m_i$ para la imagen $i$ y $\alpha_x$, $\alpha_y$, $s$, $u_0$ y $v_0$ son par�metros intr�nsecos de la c�mara que tienen relaci�n con el cambio de unidades y de relaci�n de aspecto de las im�genes, y que son independientes de la posici�n y/u orientaci�n de la c�mara.

De esta forma, en base a las ecuaciones \ref{fun.Euclidea3D}, \ref{fun.Proy} y \ref{fun.Trans2D2D} puede definirse la funci�n que relaciona puntos 3D con p�xeles en las im�genes como:

\begin{eqnarray}
\vec w_i =
\left[
\begin{array}{cccc}
f \alpha_x  & f s        & u_0 & 0\\
    0       & f \alpha_y & v_0 & 0\\
    0       & 0          & 1   & 0
\end{array}
\right]
\left[
\begin{array}{cc}
\left[R_i\right]_{3 \times 3} & \vec t\\
\vec 0^T & 1
\end{array}
\right]
\vec M
\label{fun.Trans3DImage}
\end{eqnarray}

En esta funci�n, los valores de los par�metros $\Phi$ de la c�mara corresponden a los 6 par�metros extr�nsecos --$t_x$, $t_y$, $t_z$, $\omega_Y$, $\omega_Z$ y $\theta$-- y los 5 intr�nsecos --$\alpha_x$, $\alpha_y$, $s$, $u_0$ y $v_0$--. Estos par�metros son desconocidos, pero es posible ejecutar alg�n m�todo de calibraci�n para encontrar su valor.

Los m�todos de calibraci�n t�picamente se clasifican como fotogram�tricos o autom�ticos. El primer grupo \cite{zhang00a} requiere conocer con gran exactitud la posici�n de un cierto n�mero de puntos en el espacio 3D y en las im�genes obtenida. Por esta raz�n, se requiere la utilizaci�n de un objeto de calibraci�n cuya geometr�a y posici�n respecto a la c�mara son conocidas con gran precisi�n. En cambio, los m�todos autom�ticos \cite{triggs-autocalibration} se basan en la correspondencia de puntos que se repiten en distintas im�genes, teniendo problemas al momento de encontrar la escala correcta de los objetos presentes en las im�genes. Debido a esto, y dado que en este trabajo se busca que la textura de los modelos reconstruidos sea lo m�s precisa posible, la calibraci�n de la c�mara se realiza mediante un m�todo fotogram�trico, donde la principal dificultad consiste en encontrar la geometr�a y posici�n del objeto de calibraci�n en base a la informaci�n proporcionada por los sensores usados.

El m�todo de calibraci�n empleado se basa en la definici�n de una funci�n de error de los par�metros de la c�mara y, a continuaci�n, su minimizaci�n mediante alg�n m�todo num�rico. En particular, la funci�n del error $g$ definida corresponde a:

\begin{eqnarray}
g(\Phi) = \frac{\sum_{j=1}^n \|\vec w_{i,j}(\vec M_j) - \hat {\vec w_{i,j}}\|}{n}
\label{fun.ErrorCalib}
\end{eqnarray}

En esta ecuaci�n, $\vec w_{i,j}(\vec M_j)$ corresponde al p�xel te�rico entregado por la ecuaci�n~\ref{fun.Trans3DImage} para el punto $\vec M_j$, $\hat {\vec w_{i,j}}$ al p�xel real para el mismo punto y $n$ a la cantidad total de puntos $\hat {\vec w_{i,j}}$.

Para llevar a cabo la minimizaci�n de $g$ es necesario evaluar la funci�n en al menos $3*i + 8$ puntos 3D distintos, uno por cada inc�gnita de la funci�n, con su p�xel correspondiente en alguna imagen. Para ello, se dispone de un cuadrado de arista $a$ ubicado en forma de diamante, y que en su parte frontal tiene un dibujo similar al de un tablero de ajedrez, como puede observarse en la Figura~\ref{f.objetoCal}. Su posici�n y orientaci�n exactas en el espacio son desconocidas, siendo necesario estimarlas con exactitud para realizar el proceso de calibraci�n. En la pr�ctica, esto equivale a encontrar la posici�n en el espacio de sus cuatro v�rtices $\vec v_1, \cdots, \vec v_4$, ya que cualquier punto del objeto puede obtenerse en base a ellos.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[height=45mm]{imagenes/objetoCal.eps}
  \end{center}
  \caption[Objeto de calibraci�n.]{Objeto de calibraci�n. Cuadrado dispuesto en forma de diamante para utilizarse como objeto de calibraci�n.}
  \label{f.objetoCal}
\end{figure}

Con el fin de encontrar la posici�n de estos v�rtices se genera un modelos tridimensional del objeto de calibraci�n de la misma forma descrita en la Secci�n~\ref{sec.Modelos3D.Coords}, lo cual permite obtener los puntos del espacio correspondientes a su superficie. Durante la generaci�n del modelo los puntos obtenidos se filtran para seleccionar �nicamente aquellos que corresponden al objeto de calibraci�n y no a otros elementos del ambiente que puedan haber sido detectados, para lo que se requiere que durante la captura de datos no se encuentren otros elementos cerca del objeto.

Sin embargo, como puede verse en la Figura~\ref{f.cal.obj.0}, los puntos obtenidos presentan un alto nivel de ruido, por lo que no pueden ser utilizados directamente para realizar la calibraci�n. Como en cada medici�n los �ngulos $\varphi$ como $\theta$ son fijos, se concluye que el error de los datos se presenta en las mediciones realizadas por el l�ser. Por este motivo, se efect�a una regresi�n lineal en tres dimensiones sobre todos los puntos del objeto de calibraci�n, obteniendo el plano que mejor los representa. Luego, los puntos son proyectados sobre este plano siguiendo la trayectoria del rayo del l�ser que gener� dicho punto. El resultado de esta operaci�n se muestra en la Figura~\ref{f.cal.obj.1}.

\begin{figure}[htbp]
  \begin{center}
    \subfigure[Vista frontal]{\label{f.cal.obj.0.a}\includegraphics[height=40mm]{imagenes/calobj0front.eps}}
    \hspace{0.5cm}
    \subfigure[Vista lateral]{\label{f.cal.obj.0.b}\includegraphics[height=40mm]{imagenes/calobj0lat.eps}}
    \hspace{0.5cm}
    \subfigure[Vista superior]{\label{f.cal.obj.0.c}\includegraphics[width=40mm]{imagenes/calobj0sup.eps}}
  \end{center}
  \caption[Medici�n del objeto de calibraci�n.]{Medici�n del objeto de calibraci�n. Puntos medidos por el l�ser correspondientes al objeto de calibraci�n.}
  \label{f.cal.obj.0}
\end{figure}

\begin{figure}[htbp]
  \begin{center}
    \subfigure[Vista frontal]{\label{f.cal.obj.1.a}\includegraphics[height=40mm]{imagenes/calobj1front.eps}}
    \hspace{0.5cm}
    \subfigure[Vista lateral]{\label{f.cal.obj.1.b}\includegraphics[height=40mm]{imagenes/calobj1lat.eps}}
    \hspace{0.5cm}
    \subfigure[Vista superior]{\label{f.cal.obj.1.c}\includegraphics[width=40mm]{imagenes/calobj1sup.eps}}
  \end{center}
  \caption[Correcci�n del objeto de calibraci�n.]{Correcci�n del objeto de calibraci�n. Puntos correspondientes al objeto de calibraci�n proyectados sobre el plano que mejor los representa.}
  \label{f.cal.obj.1}
\end{figure}

Para encontrar las coordenadas de los v�rtices se utiliza como punto de referencia el centro $c$ de la figura, que se calcula como el centro geom�trico de los puntos del objeto de calibraci�n. Como los cuatro v�rtices se encuentran a la misma distancia $R$ del centro de la figura, es inmediato que todos ellos est�n en el lugar geom�trico definido por la circunferencia con centro en $c$ que se haya sobre el plano obtenido anteriormente. La funci�n que define un punto $q$ en esta circunferencia corresponde a:

\begin{eqnarray}
\vec q = R cos(\alpha) \hat u + R sin(\alpha) \hat n \times \hat u + \vec c
\label{fun.Cincunferencia}
\end{eqnarray}

En ella, $\hat n$ es la normal del plano, $\hat u$ es un vector unitario y $\alpha$ es el �ngulo entre los vectores $\hat u$ y $(\vec q - \vec c)$.

El vector unitario $\hat u$ se utiliza como referencia para medir los �ngulos dentro de la circunferencia. En este caso, su valor se establece como $\|\vec q_1 - \vec c\|$, donde $q_1$ corresponde al punto m�s elevado del objeto de calibraci�n. De esta forma, indicando los �ngulos correspondientes en la ecuaci�n \ref{fun.Cincunferencia}, se obtienen las coordenadas de los v�tices $\vec v_1, \cdots, \vec v_4$. Esto permite conseguir un cuadrado de dimensiones y forma correctas, centrado en $c$. No obstante, debido a que $\hat u$ fue seleccionado en forma arbitraria, es posible que el rect�ngulo obtenido est� rotado con respecto a la posici�n real del objeto. Para corregir esto, la figura es rotada con respecto a $c$ hasta encontrar la posici�n en que se maximiza el n�mero de puntos dentro de �l.

Como se indic� con anterioridad, el m�todo de calibraci�n utilizado requiere como par�metros de entrada un conjunto de puntos tridimensionales y sus p�xeles correspondientes en las im�genes. En este caso, los puntos 3D corresponden a las intersecciones de los casilleros del dibujo con forma de tablero de ajedrez que se encuentra en la parte frontal del objeto de calibraci�n, que pueden encontrarse en forma inmediata mediante sumas vectoriales a partir de los v�rtices $\vec v_1, \cdots, \vec v_4$. Por su parte, los p�xeles correspondientes en las im�genes son seleccionados uno a uno en forma manual.

Una vez que se tienen todos estos puntos con sus p�xeles correspondientes en las im�genes, se procede a minimizar la funci�n de error $g$ mediante el m�todo del gradiente.

\section{Texturizado de los modelos}
\label{sec.Modelos3D.Textura}

Los modelos tridimensionales que se han construido a�n no tienen incorporada la informaci�n de textura. En esta secci�n se detalla la forma en que a partir de las im�genes obtenidas con la c�mara se agrega la textura a los modelos reconstruidos.

En la secci�n anterior se indic� la forma en que la c�mara es rotada y, en ciertos �ngulos fijos, captura im�genes del ambiente, gracias a las cuales es posible obtener la textura de las superficies reconstruidas. Para ello, los v�rtices de todos los tri�ngulos de las superficies son proyectados sobre cada una de las tres im�genes mediante la funci�n de proyecci�n encontrada tras el proceso de calibraci�n de la c�mara, obteniendo as� los p�xeles correspondientes para cada una de ellos.

Con esto se busca encontrar los tres v�rtices de cada tri�ngulo en al menos una de las im�genes y asignar la textura de acuerdo a los p�xeles correspondientes a los v�rtices. Sin embargo, si al menos uno de los v�rtices de un tri�ngulo no se proyecta sobre ninguna imagen se entender� que no se dispone de informaci�n de textura para dicho tri�ngulo y no se considera dentro del modelo. Si, en cambio, el tri�ngulo se proyecta en una o m�s im�genes se utiliza aquella donde el centro de la proyecci�n del tri�ngulo se encuentre m�s cerca del centro de la imagen y, junto a la informaci�n del tri�ngulo, se guarda una referencia a esta imagen y los p�xeles correspondientes a los v�rtices del tri�ngulo.

Aunque en la mayor�a de estos casos el texturizado se realiza sin inconvenientes, existen algunas excepciones. En particular, en la situaci�n representada en la Figura~\ref{f.oclusion.1.a}, se observa la superficie de una mesa, que es sostenida por una �nica pata central. Dadas ciertas condiciones de inclinaci�n del l�ser y la c�mara y la posici�n de esta mesa, puede ocurrir que a la pata de la mesa se le asigne textura correspondiente a la superficie de la mesa. Una situaci�n muy similar es la que se aprecia en la Figura~\ref{f.oclusion.1.b}, donde tanto la pared como la mesa presentes en la escena son texturizados con las mismas zonas de las im�genes. La escena con el texturizado corregido se muestra en la Figura~\ref{f.oclusion.1.c}.

\begin{figure}[htbp]
  \begin{center}
    \subfigure[Esquema]{\label{f.oclusion.1.a}\includegraphics[width=80mm]{imagenes/oclusion.eps}}
  \end{center}
  \begin{center}
    \subfigure[Escena texturizada incorrectamente]{\label{f.oclusion.1.b}\includegraphics[width=80mm]{imagenes/ocluido.eps}}
  \end{center}
  \begin{center}
    \subfigure[Escena texturizada correctamente]{\label{f.oclusion.1.c}\includegraphics[width=80mm]{imagenes/noocluido.eps}}
  \end{center}
  \caption[Correcci�n del texturizado.]{Correcci�n del texturizado. La oclusi�n entre el l�ser y la c�mara produce un texturiado incorrecto.}
  \label{f.oclusion.1}
\end{figure}

Este problema se debe a que la funci�n de proyecci�n encuentra la relaci�n entre cada tri�ngulo y las im�genes sin importar la posici�n de otros tri�ngulos en el espacio y, dado que el l�ser obtienen datos desde puntos distintos, es posible reconstruir algunas superficies que no son capturadas con la c�mara. Para solucionar este problema es necesario revisar para cada uno de los tri�ngulos del modelo si alguno de los otros tri�ngulo se encuentra entre �l y el centro �ptico de la c�mara.

Para realizar esto se utiliza la divisi�n realizada sobre el espacio, ya que permite encontrar todos los tri�ngulos que se encuentran sobre una misma regi�n del espacio. De esta manera, dado un tri�ngulo  $T_0$, se seleccionan todas las celdas $c_i$ de esta grilla que se encuentran entre $T_0$ y el centro de la c�mara y luego se revisa si $T_0$ es bloqueda o no por alguno de los tri�ngulos $T_j$ presentes en $c_i$ 

Para hacer esto �ltimo, el tri�ngulo $T_0$ se proyecta sobre el plano que contiene al tri�ngulo $T_j$, generando un nuevo tri�ngulo $T_{0_p}$, y si los tri�ngulos $T_{0_p}$ y $T_j$ se intersecan en alg�n punto se dice que $T_j$ ocluye a $T_0$. La verificaci�n de la intersecci�n de los tri�ngulos se realiza mediante el m�todo propuesto por \cite{Moller97}.

De esta manera, al finalizar este proceso, se dispone de superficies tridimensionales compuestas por tri�ngulos que tienen su informaci�n de textura correspondiente, en base a la cual puden representarse visualmente utilizando la librer�a gr�fica OpenGL.
