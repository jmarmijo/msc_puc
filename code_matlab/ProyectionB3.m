function valorJ = ProyectionB3(Parametros)
% function valorJ = Proyection3(Pars)

global m11;
global m12;
global m13;
global m21;
global m22;
global m23;
global m31;
global m32;
global m33;
global m41;
global m42;
global m43;
global m51;
global m52;
global m53;
global M1;
global M2;
global M3;
global M4;
global M5;
global evaluate;
global dibujar;
global contador;

global errorMedio;
global centro0;
global Pars;
% global Parametros;
% global errorMax;

% global alfap_x;
% global alfap_y;
% global u0;
% global v0;
% global s;

format long;

alfap_x = Parametros(1);
alfap_y = Parametros(2);
u0      = Parametros(3);
v0      = Parametros(4);
s       = 0.00000;% Parametros(5);

% wx1     = 0.098174770424681035;%+Parametros(5);
% wx2     = -0.14726215563702155;%+Parametros(5);
% wx3     = -0.39269908169872414;%+Parametros(5);

wx2     = -0.14726215563702155+Parametros(5);
wx1     = wx2+Parametros(6);
wx3     = wx2-Parametros(7);

wy1     = Parametros(8);
% wy1     = Params(6);
wy2 = wy1;%     = Parametros(10);
wy3 = wy1;%     = Parametros(11);

wz1     = Parametros(9);
% wz1     = Params(7);
wz2 = wz1;%     = Parametros(13);
wz3 = wz1;%     = Parametros(14);

% tx      = Params(8);
% ty      = Params(9);
% tz      = Params(10);
tx      = Parametros(10);
ty      = Parametros(11);
tz      = Parametros(12);
%%%%%%%%%%%%%%%%%%%%%

KP = [alfap_x s u0 0; 0 alfap_y v0 0; 0 0 1 0];

[imagen1, map1] = imread('C01\\camara0.bmp');
[imagen2, map2] = imread('C01\\camara1.bmp');
[imagen3, map3] = imread('C01\\camara2.bmp');

[imagen4, map4] = imread('C02\\camara0.bmp');
[imagen5, map5] = imread('C02\\camara1.bmp');
[imagen6, map6] = imread('C02\\camara2.bmp');

[imagen7, map7] = imread('C03\\camara0.bmp');
[imagen8, map8] = imread('C03\\camara1.bmp');
[imagen9, map9] = imread('C03\\camara2.bmp');

[imagen10, map10] = imread('C04\\camara0.bmp');
[imagen11, map11] = imread('C04\\camara1.bmp');
[imagen12, map12] = imread('C04\\camara2.bmp');

[imagen13, map13] = imread('C05\\camara0.bmp');
[imagen14, map14] = imread('C05\\camara1.bmp');
[imagen15, map15] = imread('C05\\camara2.bmp');

%se inicializan variables para la iteracion
error = 0;
max = 0;
numPts = 0;

for i=1:3
    
    if i==1
        S1 = [cos(wy1)*cos(wz1)                             cos(wy1)*sin(wz1)                               -sin(wy1)           tx;
              sin(wx1)*sin(wy1)*cos(wz1)-cos(wx1)*sin(wz1)  sin(wx1)*sin(wy1)*sin(wz1)+cos(wx1)*cos(wz1)    sin(wx1)*cos(wy1)   ty;
              cos(wx1)*sin(wy1)*cos(wz1)+sin(wx1)*sin(wz1)  cos(wx1)*sin(wy1)*sin(wz1)-sin(wx1)*cos(wz1)    cos(wx1)*cos(wy1)   tz;
              0                                             0                                               0                   1];
        Proy1 = KP*S1;
    end

    if i==2
        S2 = [cos(wy2)*cos(wz2)                             cos(wy2)*sin(wz2)                               -sin(wy2)          tx;
              sin(wx2)*sin(wy2)*cos(wz2)-cos(wx2)*sin(wz2)  sin(wx2)*sin(wy2)*sin(wz2)+cos(wx2)*cos(wz2)    sin(wx2)*cos(wy2)  ty;
              cos(wx2)*sin(wy2)*cos(wz2)+sin(wx2)*sin(wz2)  cos(wx2)*sin(wy2)*sin(wz2)-sin(wx2)*cos(wz2)    cos(wx2)*cos(wy2)  tz;
              0                                             0                                               0                  1];
        Proy2 = KP*S2;
    end

    if i==3
        S3 = [cos(wy3)*cos(wz3)                             cos(wy3)*sin(wz3)                               -sin(wy3)          tx;
              sin(wx3)*sin(wy3)*cos(wz3)-cos(wx3)*sin(wz3)  sin(wx3)*sin(wy3)*sin(wz3)+cos(wx3)*cos(wz3)    sin(wx3)*cos(wy3)  ty;
              cos(wx3)*sin(wy3)*cos(wz3)+sin(wx3)*sin(wz3)  cos(wx3)*sin(wy3)*sin(wz3)-sin(wx3)*cos(wz3)    cos(wx3)*cos(wy3)  tz;
              0                                             0                                               0                  1];
        Proy3 = KP*S3;
    end

    for j=1:5
        
        if evaluate
%             if (j==2) continue; end %#ok<SEPEX>
%             if (j==3) continue; end %#ok<SEPEX>
%             if (j==4) continue; end %#ok<SEPEX>
%             if (j==5) continue; end %#ok<SEPEX>
        else
               %             if (j==2) continue; end %#ok<SEPEX>
%             if (j==3) continue; end %#ok<SEPEX>
%             if (j==4) continue; end %#ok<SEPEX>
%             if (j==5) continue; end %#ok<SEPEX>
        end
        
%         delta(1) = Params(11 + (j-1)*3);%-centro0(1,j);
%         delta(2) = Params(12 + (j-1)*3);%-centro0(2,j);
%         delta(3) = Params(13 + (j-1)*3);%-centro0(3,j);
%         
%         angulo(1)= Params(26 + (j-1)*3);
%         angulo(2)= Params(27 + (j-1)*3);
%         angulo(3)= Params(28 + (j-1)*3);

        delta(1) = Pars(1 + (j-1)*3);%-centro0(1,j);
        delta(2) = Pars(2 + (j-1)*3);%-centro0(2,j);
        delta(3) = Pars(3 + (j-1)*3);%-centro0(3,j);
        
        angulo(1)= Pars(16 + (j-1)*3);
        angulo(2)= Pars(17 + (j-1)*3);
        angulo(3)= Pars(18 + (j-1)*3);
        
        R1 = [1, 0, 0; 0, cos(angulo(1)), sin(angulo(1)); 0, -sin(angulo(1)), cos(angulo(1))];
        R2 = [cos(angulo(2)), 0, -sin(angulo(2)); 0, 1, 0; sin(angulo(2)), 0, cos(angulo(2))];
        R3 = [cos(angulo(3)), sin(angulo(3)), 0; -sin(angulo(3)), cos(angulo(3)), 0; 0, 0, 1];
        
        R = R1*R2*R3;

        if (j==1)
            M=M1;
            if (i==1) P=Proy1; m=m11; imagen=imagen1; map=map1; end %#ok<SEPEX>
            if (i==2) P=Proy2; m=m12; imagen=imagen2; map=map2; end %#ok<SEPEX>
            if (i==3) P=Proy3; m=m13; imagen=imagen3; map=map3; end %#ok<SEPEX>
        end
        if (j==2)% && ((evaluate == 1 && testModel==j) || (evaluate == 0 && testModel~=j)))
            M=M2;
            if (i==1) P=Proy1; m=m21; imagen=imagen4; map=map4; end %#ok<SEPEX>
            if (i==2) P=Proy2; m=m22; imagen=imagen5; map=map5; end %#ok<SEPEX>
            if (i==3) P=Proy3; m=m23; imagen=imagen6; map=map6; end %#ok<SEPEX>
        end
        if (j==3)% && ((evaluate == 1 && testModel==j) || (evaluate == 0 && testModel~=j)))
            M=M3;
            if (i==1) P=Proy1; m=m31; imagen=imagen7; map=map7; end %#ok<SEPEX>
            if (i==2) P=Proy2; m=m32; imagen=imagen8; map=map8; end %#ok<SEPEX>
            if (i==3) P=Proy3; m=m33; imagen=imagen9; map=map9; end %#ok<SEPEX>
        end
        if (j==4)% && ((evaluate == 1 && testModel==j) || (evaluate == 0 && testModel~=j)))
            M=M4;
            if (i==1) P=Proy1; m=m41; imagen=imagen10; map=map10; end %#ok<SEPEX>
            if (i==2) P=Proy2; m=m42; imagen=imagen11; map=map11; end %#ok<SEPEX>
            if (i==3) P=Proy3; m=m43; imagen=imagen12; map=map12; end %#ok<SEPEX>
        end
        if (j==5)% && ((evaluate == 1 && testModel==j) || (evaluate == 0 && testModel~=j)))
            M=M5;
            if (i==1) P=Proy1; m=m51; imagen=imagen13; map=map13; end %#ok<SEPEX>
            if (i==2) P=Proy2; m=m52; imagen=imagen14; map=map14; end %#ok<SEPEX>
            if (i==3) P=Proy3; m=m53; imagen=imagen15; map=map15; end %#ok<SEPEX>
        end

        if (dibujar == 1 && evaluate)
            axes;
            subplot(1, 3, i);
            colormap(map);
            imshow(imagen);
            grid on;
            hold on;
        end
        
        %iteracion en los puntos
%        if ((testModel ~= j && evaluate == 0) || (testModel==j && evaluate == 1))
            for k=1:81
%                 if (k>= 1 && k<= 9) continue; end %#ok<SEPEX>
%                 if (k>=10 && k< 18) continue; end %#ok<SEPEX>
%                 if (k>=19 && k< 27) continue; end %#ok<SEPEX>
%                 if (k>=28 && k<=36) continue; end %#ok<SEPEX>
%                 if (k>=37 && k<=45) continue; end %#ok<SEPEX>
%                 if (k>=46 && k<=54) continue; end %#ok<SEPEX>
%                 if (k>=55 && k< 63) continue; end %#ok<SEPEX>
%                 if((k>=64 && k< 67) || (k> 67 && k<=72)) continue; end %#ok<SEPEX>
%                 if((k> 73 && k< 79) || (k> 79 && k< 81)) continue; end %#ok<SEPEX>

%                 if (k>= 1 && k<= 9) continue; end %#ok<SEPEX>
%                 if (k>=11 && k<=18) continue; end %#ok<SEPEX>
%                 if (k>=21 && k<=27) continue; end %#ok<SEPEX>
%                 if (k>=31 && k<=36) continue; end %#ok<SEPEX>
%                 if (k>=41 && k<=45) continue; end %#ok<SEPEX>
%                 if (k>=51 && k<=54) continue; end %#ok<SEPEX>
%                 if (k>=61 && k<=63) continue; end %#ok<SEPEX>
%                 if (k>=71 && k<=72) continue; end %#ok<SEPEX>
%                 if (k>=81 && k<=81) continue; end %#ok<SEPEX>

                if (m(3,k)==1)
                    w = P*M(:,k);

                    X = M(3,k)-centro0(3);
                    Y = M(1,k)-centro0(1);
                    Z = M(2,k)-centro0(2);
                    
                    NewM = R*[X;Y;Z];
                    
                    X = NewM(1)+centro0(3) + delta(3);
                    Y = NewM(2)+centro0(1) + delta(1);
                    Z = NewM(3)+centro0(2) + delta(2);

                    U = P(1,1)*X + P(1,2)*Y + P(1,3)*Z + P(1,4)*1;
                    V = P(2,1)*X + P(2,2)*Y + P(2,3)*Z + P(2,4)*1;
                    divisor = P(3,1)*X + P(3,2)*Y + P(3,3)*Z + P(3,4)*1;

                    w(1) = U/divisor;
                    w(2) = V/divisor;
                    w(3) = 1;

                    if (dibujar == 1)
%                         if (j==4 && k==9)
%                             m1k = m(1,k)
%                             m2k = m(2,k)
%                             w1 = w(1)
%                             w2 = w(2)
%                             raiz = sqrt((m(1,k)-w(1))*(m(1,k)-w(1)) + (m(2,k)-w(2))*(m(2,k)-w(2)))
%                         end
                        plot(m(1,k),m(2,k),'g.')
                        plot(w(1),w(2),'r.')
                    end

                    %hay que calcular el error sin considerar la tercera coordenada
                    aux = sqrt((m(1,k)-w(1))*(m(1,k)-w(1)) + (m(2,k)-w(2))*(m(2,k)-w(2)));
                    if (max < aux)
                        max = aux;
                    end

                    error = error + aux;
                    numPts = numPts + 1;
                end
            end
%         end
        
    end
end

contador = contador + 1;
if (evaluate == 0 && (mod(contador,50) == 0 || contador==1))
    contador                     %#ok<NOPRT>
    errorMedio = error/numPts    %#ok<NOPRT,NASGU>
else
    errorMedio = error/numPts;    %#ok<NOPRT,NASGU>
end


    fid = fopen('ultimos_parametros.txt','wt');     % 'rt' means "read text"
    if (fid < 0) disp('could not open file "ultimos_parametros.txt"'); return; end %#ok<SEPEX>
        fprintf(fid,'%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n',Parametros, Pars);
% fprintf(fid,'%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n%4.12f\n',Params);
    fclose(fid);

    valorJ = errorMedio;

%     dif = errorOld - errorMedio;
%     if (dif < 0.00001 && dif > 0 && evaluate==0)
%         evaluate = 1;
%         Proyection3(Parametros);
%         valorJ = errorMedio
%         pause;
%     end

%     errorOld = errorMedio;

% valorJ = max;