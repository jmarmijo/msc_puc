function EvaluarCalibracion

format long;

m21= [295 313 331 349 367 385 404 422   0         277 295 313 331 349 368 386   0   0         259 277 295 313 332   0   0   0   0         240 258 276 295   0   0   0   0   0         222 240 258   0   0   0   0   0   0         203 222   0   0   0   0   0   0   0         185   0   0   0   0   0   0   0   0           0   0   0   0   0   0   0   0   0           0   0   0   0   0   0   0   0   0;
      349 368 386 404 421 439 458 475   0         368 386 404 422 440 458 479   0   0         386 404 422 440 458   0   0   0   0         404 422 441 459   0   0   0   0   0         422 440 459   0   0   0   0   0   0         440 459   0   0   0   0   0   0   0         459   0   0   0   0   0   0   0   0           0   0   0   0   0   0   0   0   0           0   0   0   0   0   0   0   0   0;
        1   1   1   1   1   1   1   1   0           1   1   1   1   1   1   1   0   0           1   1   1   1   1   0   0   0   0           1   1   1   1   0   0   0   0   0           1   1   1   0   0   0   0   0   0           1   1   0   0   0   0   0   0   0           1   0   0   0   0   0   0   0   0           0   0   0   0   0   0   0   0   0           0   0   0   0   0   0   0   0   0];
    
m22= [283 303 323 343 362 382 402 421 440         264 284 304 324 343 363 382 402 421         245 265 285 304 324 344 364 383 402         226 245 265 285 305 325 344 364 383         207 226 246 266 286 306 326 345 365         188 207 227 247 267 287 307 326 346         169 188 208 228 248 268 288 307 327         150 170 189 209 229 249 269 289 308         131 151 171 191 210 230 250 270 290;
      106 124 144 163 182 201 220 239 258         125 144 163 183 201 221 240 258 277         145 164 183 202 221 240 259 278 279         165 185 203 222 241 260 279 298 317         185 204 223 242 261 280 299 317 336         205 224 243 262 281 300 318 337 355         224 244 263 281 300 320 338 356 375         245 264 282 301 320 339 357 376 394         264 284 303 321 340 358 377 395 413;
        1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1];

m23= [  0   0   0   0   0   0   0   0 441           0   0   0   0   0   0   0 399 420           0   0   0   0   0   0 356 378 400           0   0   0   0   0 314 336 358 379           0   0   0   0 271 293 315 337 258           0   0   0 228 251 273 295 317 338           0   0 186 208 230 253 275 296 318           0 143 166 188 210 233 255 276 298         101 123 146 168 191 213 235 257 278;
        0   0   0   0   0   0   0   0   2           0   0   0   0   0   0   0   3  23           0   0   0   0   0   0   3  24  45           0   0   0   0   0   5  26  46  67           0   0   0   0   6  27  47  68  88           0   0   0   8  28  49  69  90 110           0   0  10  30  51  71  91 111 130           0  13  33  53  73  93 113 132 152          15  35  55  75  95 115 134 154 173;
        0   0   0   0   0   0   0   0   1           0   0   0   0   0   0   0   1   1           0   0   0   0   0   0   1   1   1           0   0   0   0   0   1   1   1   1           0   0   0   0   1   1   1   1   1           0   0   0   1   1   1   1   1   1           0   0   1   1   1   1   1   1   1           0   1   1   1   1   1   1   1   1           1   1   1   1   1   1   1   1   1];

fid = fopen('C02\\calibration-object.txt','rt');     % 'rt' means "read text"
if (fid < 0) disp('could not open file "C02\\calibration-object.txt"'); return; end
M2 = fscanf(fid,'%lf %lf %lf',[3,81]);
fclose(fid);
M2(4,:)=1;

alfap_x =  814.87735;
alfap_y =  814.07356;
u0      =  331.23625;
v0      =  259.77755;
s       =    0.00000;
    
wx = 1.2201825 + 1.4722082882741638;
wy = 2.8645665;
wz = 0.116127 - 2.3545415991356222;

KP = [alfap_x s u0 0; 0 alfap_y v0 0; 0 0 1 0];

w_x = wx - 0.082968;  
w_y = wy - 0.118443;%   + alfa(1);
w_z = wz - 0.121242;
t_x = -  20.870662 - (-58.060061);
t_y = +  30.920106 - (568.592304);
t_z = + 476.544381 - (158.497876);
S1 = [cos(w_y)*cos(w_z)                             cos(w_y)*sin(w_z)                              -sin(w_y)           t_x;
      sin(w_x)*sin(w_y)*cos(w_z)-cos(w_x)*sin(w_z)  sin(w_x)*sin(w_y)*sin(w_z)+cos(w_x)*cos(w_z)    sin(w_x)*cos(w_y)  t_y;
      cos(w_x)*sin(w_y)*cos(w_z)+sin(w_x)*sin(w_z)  cos(w_x)*sin(w_y)*sin(w_z)-sin(w_x)*cos(w_z)    cos(w_x)*cos(w_y)  t_z;
      0                                             0                                               0                  1];

w_x = wx;
w_y = wy;%   + alfa(2);
w_z = wz;
t_x = -  25.236308 - (-58.060061);
t_y = +  30.870815 - (568.592304);
t_z = + 439.388147 - (158.497876);
S2 = [cos(w_y)*cos(w_z)                             cos(w_y)*sin(w_z)                              -sin(w_y)           t_x;
      sin(w_x)*sin(w_y)*cos(w_z)-cos(w_x)*sin(w_z)  sin(w_x)*sin(w_y)*sin(w_z)+cos(w_x)*cos(w_z)    sin(w_x)*cos(w_y)  t_y;
      cos(w_x)*sin(w_y)*cos(w_z)+sin(w_x)*sin(w_z)  cos(w_x)*sin(w_y)*sin(w_z)-sin(w_x)*cos(w_z)    cos(w_x)*cos(w_y)  t_z;
      0                                             0                                               0                  1];

w_x = wx + 0.7135166535897932384626433832795;
w_y = wy + 0.6444013071795864769252867665595;% + alfa(3);
w_z = wz - 0.398026;
t_x = -  29.571875 - (-49.676056);
t_y = - 125.540350 - (559.706458);
t_z = + 393.154689 - (53.849655);
S3 = [cos(w_y)*cos(w_z)                             cos(w_y)*sin(w_z)                              -sin(w_y)           t_x;
      sin(w_x)*sin(w_y)*cos(w_z)-cos(w_x)*sin(w_z)  sin(w_x)*sin(w_y)*sin(w_z)+cos(w_x)*cos(w_z)    sin(w_x)*cos(w_y)  t_y;
      cos(w_x)*sin(w_y)*cos(w_z)+sin(w_x)*sin(w_z)  cos(w_x)*sin(w_y)*sin(w_z)-sin(w_x)*cos(w_z)    cos(w_x)*cos(w_y)  t_z;
      0                                             0                                               0                  1];

Proy1 = KP*S1;
Proy2 = KP*S2;
Proy3 = KP*S3;

[imagen4, map4] = imread('C02\\camara0.bmp');
[imagen5, map5] = imread('C02\\camara1.bmp');
[imagen6, map6] = imread('C02\\camara2.bmp');

%iteracion en las imagenes
error = 0;
max = 0;
numPts = 0;

for i=4:6
    if i==4 P=Proy1; m=m21; M=M2; imagen=imagen4; map=map4; end %#ok<SEPEX>
    if i==5 P=Proy2; m=m22; M=M2; imagen=imagen5; map=map5; end %#ok<SEPEX>
    if i==6 P=Proy3; m=m23; M=M2; imagen=imagen6; map=map6; end %#ok<SEPEX>

    axes;
    subplot(2,2,i-3);
    colormap(map);
    imshow(imagen);
    grid on;
    hold on;
    
    %iteracion en los puntos
    for k=1:81
        if (m(3,k)==1)

            w = P*M(:,k);

            u = w(1)/w(3);
            v = w(2)/w(3);
            w(1) = v;
            w(2) = -u+480;
            w(3) = 1;

            plot(m(1,k),m(2,k),'g.');
            plot(w(1),w(2),'r.');
   
            %hay que quitar el la tercera coordenada para calcular el error
            aux = sqrt((m(1,k)-w(1))*(m(1,k)-w(1)) + (m(2,k)-w(2))*(m(2,k)-w(2)));
            if (max < aux)
                max = aux;
            end

            error = error + aux;
            numPts = numPts + 1;
        end
    end
    
end

errorMedio = error/numPts   %#ok<NOPRT,NASGU>
errorMax = max              %#ok<NOPRT,NASGU>

end