// SurfaceSimplification.h: archivo de encabezado principal para la aplicación PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error incluye 'stdafx.h' antes de incluir este archivo para PCH
#endif

#include "resource.h"		// Símbolos principales


// CSurfaceSimplificationApp:
// Consulte la sección SurfaceSimplification.cpp para obtener información sobre la implementación de esta clase
//

class CSurfaceSimplificationApp : public CWinApp
{
public:
	CSurfaceSimplificationApp();

// Reemplazos
	public:
	virtual BOOL InitInstance();

// Implementación

	DECLARE_MESSAGE_MAP()
};

extern CSurfaceSimplificationApp theApp;
