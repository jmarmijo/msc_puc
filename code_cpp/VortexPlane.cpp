#include "StdAfx.h"
#include ".\vortexplane.h"

VortexPlane::VortexPlane(void)
{
	largo = 0;
}

VortexPlane::~VortexPlane(void)
{
}

void VortexPlane::AddVertex(Vertex *v)
{
	int i;
	Vertex **aux;

	for(i=0; i<largo; ++i)
	{
		if (lista[i]==v)
			return;
	}

	if (largo > 0)
	{
		aux = (Vertex**)malloc(largo*sizeof(Vertex*));
		for(i=0; i<largo; ++i)
			aux[i] = lista[i];

		free(lista);
	}

	lista = (Vertex**)malloc((largo+1)*sizeof(Vertex*));
	for(i=0; i<largo; ++i)
		lista[i] = aux[i];
	lista[i] = v;

	if (largo > 0)
		free(aux);

	++largo;
}
