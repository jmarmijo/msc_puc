#include "StdAfx.h"
#include ".\vertex.h"

Vertex::Vertex()
{
this->x = 0.0;
	this->y = 0.0;
	this->z = 0.0;

	u0 = NULL;
	v0 = NULL;
}

Vertex::Vertex(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;

	u0 = NULL;
	v0 = NULL;
}

void Vertex::Set(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void Vertex::AllocatePixels()
{
	int i;

	u0 = (int**)malloc(NUM_IMAGENES*sizeof(int));
	v0 = (int**)malloc(NUM_IMAGENES*sizeof(int));

	for(i=0; i<NUM_IMAGENES; ++i)
	{
		u0[i] = NULL;
		v0[i] = NULL;
	}
}

bool Vertex::HasPixels()
{
	if (u0==NULL || v0==NULL)
		return false;
	else
		return true;
}

void Vertex::SetImagePixel(int numImage, int u, int v)
{
	if (u0 == NULL || v0 == NULL)
		AllocatePixels();

	if (u0[numImage]==NULL || v0[numImage]==NULL)
	{
		u0[numImage] = (int*)malloc(sizeof(int));
		v0[numImage] = (int*)malloc(sizeof(int));
	}

	*u0[numImage] = u;
	*v0[numImage] = v;
}

void Vertex::GetImagePixel(int numImage, int* u, int* v)
{
	*u = *u0[numImage];
	*v = *v0[numImage];
}

double Vertex::GetX()
{
	return x;
}

double Vertex::GetY()
{
	return y;
}

double Vertex::GetZ()
{
	return z;
}

Vertex::~Vertex(void)
{
}