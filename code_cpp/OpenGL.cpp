#include "StdAfx.h"
#include ".\opengl.h"

OpenGL *ogl;
BITMAPINFOHEADER info;
//unsigned char *bitmap = NULL;
bool xPressed = false;
bool yPressed = false;
bool zPressed = false;

OpenGL::OpenGL()
{
	s = NULL;

	rho = 1.0;
	phiX = phiY = phiZ = 0.0;

	eyeX = eyeY = eyeZ = 0.0;
	desplazamientoX = desplazamientoY = desplazamientoZ = 0.0;
	upX = upY = upZ = 0.0;

	rot = new Lista();
	ogl = this;

	//Perspectiva();
	CrearVentana();
	glutDisplayFunc(Dibujar);
}

void OpenGL::SetSurface(Surface *surf)
{
	s = surf;
	//(ogl->s) = s;
	(ogl->s)->NumTriangles(&maxPoligonosX, &maxPoligonosY, &maxPoligonosZ);
}

void OpenGL::start()
{
	glutMainLoop();
}

OpenGL::~OpenGL(void)
{
}

void OpenGL::CrearVentana()
{
	int argc = 1;
	char **argv;

	//main
	argv = new char * [1];
	argv[0] = new char[50];
	sprintf_s(argv[0], 50, "%s", "SurfaceReconstruction.exe");
	
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);   
	glutInitWindowSize(AnchoVentana, AltoVentana);
    glutInitWindowPosition(POS_X, POS_Y);   
	ventana = glutCreateWindow("Surface Reconstruction");

	//init
	glClearColor(0.3, 0.3, 0.3, 1.0);
	glShadeModel(GL_FLAT);

	//Light();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texName);
	glBindTexture(GL_TEXTURE_2D, texName);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glMatrixMode(GL_PROJECTION);
  	glViewport(0, 0, AnchoVentana, AltoVentana);
	gluPerspective(45.f,4.0/3.0,0.1f,500000.f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	
	glPolygonMode(GL_FRONT , GL_FILL);
	glPolygonMode(GL_BACK , GL_LINE); //GL_POINT


	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glutKeyboardFunc(&WindowKeyDown);
	glutKeyboardUpFunc(&WindowKeyUp);
}

void Light()
{
	/*GLfloat msp[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat msh[] = {128.0};
	GLfloat lightposition[] = {0.0, -500.0, 0.0, 0.0};
	GLfloat wl[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat ma[] = {0.1, 0.1, 0.1, 1.0};
	GLfloat luzAmbiente[] = {1.0, 1.0, 1.0, 1.0};
	
	glShadeModel(GL_SMOOTH);

	glMaterialfv(GL_FRONT, GL_SPECULAR, msp);
	glMaterialfv(GL_FRONT, GL_SHININESS, msh);

	glLightfv(GL_LIGHT0, GL_POSITION, lightposition);
	//glLightfv(GL_LIGHT0, GL_DIFFUSE, wl);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, wl);
	glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ma);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	glShadeModel(GL_SMOOTH);*/

	// Queremos que se dibujen solo los poligonos que estan "de frente" y que estos sean "rellenos"
	
	
	//glCullFace(GL_BACK);
}

void snapshot(int windowWidth, int windowHeight, char* filename)
{
	byte*  bmpBuffer = (byte*)malloc(windowWidth*windowHeight*3);
	if  (!bmpBuffer)
		return;
 
	glReadPixels((GLint)0,  (GLint)0,
	(GLint)windowWidth-1, (GLint)windowHeight-1,
	GL_BGR_EXT,  GL_UNSIGNED_BYTE, bmpBuffer);
	 
	FILE *filePtr = fopen(filename,  "wb");
	if (!filePtr)
		return;
	 
	BITMAPFILEHEADER  bitmapFileHeader;
	BITMAPINFOHEADER  bitmapInfoHeader;
	 
	bitmapFileHeader.bfType = 0x4D42;  //"BM"
	bitmapFileHeader.bfSize =  windowWidth*windowHeight*3;
	bitmapFileHeader.bfReserved1 =  0;
	bitmapFileHeader.bfReserved2 = 0;
	bitmapFileHeader.bfOffBits  =
	sizeof(BITMAPFILEHEADER) +  sizeof(BITMAPINFOHEADER);
	 
	bitmapInfoHeader.biSize =  sizeof(BITMAPINFOHEADER);
	bitmapInfoHeader.biWidth =  windowWidth-1;
	bitmapInfoHeader.biHeight =  windowHeight-1;
	bitmapInfoHeader.biPlanes = 1;
	bitmapInfoHeader.biBitCount  = 24;
	bitmapInfoHeader.biCompression =  BI_RGB;
	bitmapInfoHeader.biSizeImage = 0;
	bitmapInfoHeader.biXPelsPerMeter  = 0; // ?
	bitmapInfoHeader.biYPelsPerMeter = 0; //  ?
	bitmapInfoHeader.biClrUsed = 0;
	bitmapInfoHeader.biClrImportant =  0;
	 
	fwrite(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1,  filePtr);
	fwrite(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1,  filePtr);
	fwrite(bmpBuffer, windowWidth*windowHeight*3, 1,  filePtr);
	fclose(filePtr);
	free(bmpBuffer);
}

void Dibujar()
{
	Celda* c;

	float yMedio;
	bool esnull;
	char *s;
	
	static int theta = 50;
	static int phi = 130;
	static int r;

	double x, y, z;
	double x2, y2, z2;
	float upx, upy, upz, d;
	
	esnull = true;
	
	glLoadIdentity();

	if (ogl->s == NULL)
		yMedio = 0.0;
	else if (esnull)
	{
		yMedio = (float)(ogl->s->yMedio);
		esnull = false;
		r = (int)yMedio;
	}

	if (!esnull)
	{
		/**/
		x = r*sin(theta*_PI/180.0)*cos(phi*_PI/180.0);
		y = r*sin(theta*_PI/180.0)*sin(phi*_PI/180.0)-yMedio+800;
		z = r*cos(theta*_PI/180.0);

		x2 = r*sin((theta+0.00001)*_PI/180.0)*cos(phi*_PI/180.0);
		y2 = r*sin((theta+0.00001)*_PI/180.0)*sin(phi*_PI/180.0)-yMedio+800;
		z2 = r*cos((theta+0.00001)*_PI/180.0);

		d = abs(x-x2) + abs(y-y2) + abs(z-z2);
		upx = (x-x2)/d;
		upy = (y-y2)/d;
		upz = (z-z2)/d;

		//gluLookAt(	(ogl->rho)*0.0, (ogl->rho)*-200.0, (ogl->rho)*0.0,
		gluLookAt(	   x,	  -y,   z,
					 0.0, yMedio, 0.0,
					 //-cos(phi*_PI/180.0)*factor, sin(phi*_PI/180.0), 1.0);
					 upx, -upy, upz);

		//glTranslatef(0, yMedio, 0);
		//glRotatef(-2, 1.0, 0.0, 0.0);
		//glRotatef(-6, 0.0, 0.0, 1.0);
		//glTranslatef(0, -yMedio, 0);
		/**/

		/*
		gluLookAt(	(ogl->rho)*0.0, (ogl->rho)*-200.0, (ogl->rho)*0.0,
				0.0, 500.0, 0.0,
				0.0, 0.0, 1.0);

		if (!(ogl->rot)->Empty())
		{
			c = (ogl->rot)->ObtenerElemento();
			ogl->phiX += c->AnguloX();
			ogl->phiY += c->AnguloY();
			ogl->phiZ += c->AnguloZ();
			(ogl->rot)->RemoverElemento();
		}

		glTranslatef(0, 1000, 0);
		glRotatef(ogl->phiX, 1.0, 0.0, 0.0);
		glRotatef(ogl->phiY, 0.0, 1.0, 0.0);
		glRotatef(ogl->phiZ, 0.0, 0.0, 1.0);
		glTranslatef(0, -1000, 0);
		*/
	}

	DibujarPuntos();
	glFlush();
	glutPostRedisplay();

	if (!esnull)
	{
		/**/
		s = (char*)malloc(50*sizeof(char));
		sprintf(s, "escena/r%dph%dth%d.bmp",r,phi,theta);
		snapshot(AnchoVentana, AltoVentana, s);
		free(s);

		if (theta < 130)
			theta += 10;
		else if (phi > 50)
		{
			theta = 50;
			phi -= 10;
		}
		/**/
	}
}

void DibujarPuntos()
{
	//register int i, limite;
	int indiceImagen;
	int u[3], v[3];
	
	register int i, j, k;
	
	Vertex *p;
	Vertex *q;
	Vertex *r;
	Triangle *t;
	TriangleCell *tc;
	Surface *s;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	s = ogl->s;

	if (s != NULL)
	{
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glBindTexture(GL_TEXTURE_2D, ogl->texName);

		//limite = ogl->maxPoligonosX*ogl->maxPoligonosY*ogl->maxPoligonosZ;
		//for(i=0; i<limite; ++i)

		/*glDisable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLES);
		glColor3d(1.0, 0.0, 0.0);
		glVertex3f(0.0,400,65);
		glVertex3f(0.0,401,65);
		glVertex3f(0.0,401,64);

		glVertex3f(0.0,401,65);
		glVertex3f(0.0,400,65);
		glVertex3f(0.0,401,64);
		glEnd();
		glEnable(GL_TEXTURE_2D);*/

		for(i=0; i<s->largoX; ++i)
		{
			for(j=s->minCeldaY; j<s->maxCeldaY; ++j)
			{
				for(k=0; k<s->largoZ; ++k)
				{
					if (!s->IsEmptyTriangleList(i, j, k))
					{
						//if (!s->IsEmptyTriangleList(i))
						{
							t = s->GetTriangle(i,j,k);
							while(t!=NULL)
							{
								t->GetImagePixels(&indiceImagen, u, v);

								p = t->GetVertex(0);
								q = t->GetVertex(1);
								r = t->GetVertex(2);

								//glDisable(GL_TEXTURE_2D);
								
								if (u!=NULL && v!= NULL && indiceImagen!=-1)
									DibujarTriangulo(p, q, r, u, v, indiceImagen);
								
								//glEnable(GL_TEXTURE_2D);
								/*else
								{
									glDisable(GL_TEXTURE_2D);
									glBegin(GL_TRIANGLES);
									glColor3d(0.3, 0.1, 0.1);
									DibujarTriangulo(p, q, r);
									glEnd();

									glEnable(GL_TEXTURE_2D);
									glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
									glBindTexture(GL_TEXTURE_2D, ogl->texName);
								}*/

								t = s->GetTriangle(i,j,k);
							}
						}
					}
				}
			}
		}

		glDisable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLES);

		tc = s->GetNTTriangleCell();
		while(tc!=NULL)
		{
			t = tc->GetTriangle();

			p = t->GetVertex(0);
			q = t->GetVertex(1);
			r = t->GetVertex(2);
			DibujarTriangulo(p, q, r);

			tc = tc->GetNext();
		}

		tc = s->GetOccludedTriangleCell();
		glColor3d(0.3, 0.3, 0.3);
		//glColor3d(0.2, 0.2, 0.2);
		while(tc!=NULL)
		{
			t = tc->GetTriangle();

			p = t->GetVertex(0);
			q = t->GetVertex(1);
			r = t->GetVertex(2);
			DibujarTriangulo(p, q, r);

			tc = tc->GetNext();
		}

		glEnd();
	}

	glutSwapBuffers();
}

void DibujarTriangulo(Vertex *p, Vertex *q, Vertex *r)
{
	glVertex3f(p->GetX(),p->GetY(),p->GetZ());
	glVertex3f(q->GetX(),q->GetY(),q->GetZ());
	glVertex3f(r->GetX(),r->GetY(),r->GetZ());
}

void DibujarTriangulo(Vertex *p, Vertex *q, Vertex *r, int u[3], int v[3], int indiceImagen)
{
	int u0, v0;
	int i,j;

	//Normal block
	if ((int)(u[0]/BLOCK_SIZE) == (int)(u[1]/BLOCK_SIZE) &&
		(int)(u[0]/BLOCK_SIZE) == (int)(u[2]/BLOCK_SIZE) &&
		(int)(v[0]/BLOCK_SIZE) == (int)(v[1]/BLOCK_SIZE) &&
		(int)(v[0]/BLOCK_SIZE) == (int)(v[2]/BLOCK_SIZE))
	{
		u0 = (int)((u[0] + u[1] + u[2])/3);
		v0 = (int)((v[0] + v[1] + v[2])/3);

		u0 /= BLOCK_SIZE;
		v0 /= BLOCK_SIZE;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BLOCK_SIZE, BLOCK_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)]);
		glBegin(GL_TRIANGLES);

		glTexCoord2f((u[0]%BLOCK_SIZE)/(double)BLOCK_SIZE, (v[0]%BLOCK_SIZE)/(double)BLOCK_SIZE);
		//i = (int)((u[0]%BLOCK_SIZE));
		//j = (int)((v[0]%BLOCK_SIZE));
		//u0 = u[0]/BLOCK_SIZE;
		//v0 = v[0]/BLOCK_SIZE;
		//glColor3d(	Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][0]/255.0,
		//			Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][1]/255.0,
		//			Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][2]/255.0);
		glVertex3f(p->GetX(),p->GetY(),p->GetZ());

		glTexCoord2f((u[1]%BLOCK_SIZE)/(double)BLOCK_SIZE, (v[1]%BLOCK_SIZE)/(double)BLOCK_SIZE);
		//i = (int)((u[1]%BLOCK_SIZE));
		//j = (int)((v[1]%BLOCK_SIZE));
		//u0 = u[1]/BLOCK_SIZE;
		//v0 = v[1]/BLOCK_SIZE;
		//glColor3d(	Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][0]/255.0,
		//			Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][1]/255.0,
		//			Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][2]/255.0);
		glVertex3f(q->GetX(),q->GetY(),q->GetZ());

		glTexCoord2f((u[2]%BLOCK_SIZE)/(double)BLOCK_SIZE, (v[2]%BLOCK_SIZE)/(double)BLOCK_SIZE);
		//i = (int)((u[2]%BLOCK_SIZE));
		//j = (int)((v[2]%BLOCK_SIZE));
		//u0 = u[2]/BLOCK_SIZE;
		//v0 = v[2]/BLOCK_SIZE;
		//glColor3d(	Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][0]/255.0,
		//			Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][1]/255.0,
		//			Input::imagenes[indiceImagen][BLOCK_COLS*(v0)+(u0)][j][i][2]/255.0);
		glVertex3f(r->GetX(),r->GetY(),r->GetZ());

		glEnd();
	}
	
	//right block
	else 
		if ((int)((u[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((u[1]-(BLOCK_SIZE/2))/BLOCK_SIZE) &&
			 (int)((u[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((u[2]-(BLOCK_SIZE/2))/BLOCK_SIZE) &&
			 (int)((v[0]/BLOCK_SIZE) == (int)(v[1]/BLOCK_SIZE)) &&
			 (int)((v[0]/BLOCK_SIZE) == (int)(v[2]/BLOCK_SIZE)))
	{
		u0 = (int)((u[0] + u[1] + u[2])/3) - (BLOCK_SIZE/2);
		v0 = (int)((v[0] + v[1] + v[2])/3);

		u0 /= BLOCK_SIZE;
		v0 /= BLOCK_SIZE;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BLOCK_SIZE, BLOCK_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, Input::imagenesRight[indiceImagen][BLOCK_COLS*(v0)+(u0)]);

		glBegin(GL_TRIANGLES);

		glTexCoord2f(((u[0]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE, (v[0]%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(p->GetX(),p->GetY(),p->GetZ());

		glTexCoord2f(((u[1]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE, (v[1]%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(q->GetX(),q->GetY(),q->GetZ());

		glTexCoord2f(((u[2]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE, (v[2]%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(r->GetX(),r->GetY(),r->GetZ());

		glEnd();
	}
	//down block
	else if ((int)((u[0]/BLOCK_SIZE) == (int)(u[1]/BLOCK_SIZE)) &&
			 (int)((u[0]/BLOCK_SIZE) == (int)(u[2]/BLOCK_SIZE)) &&
			 (int)((v[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((v[1]-(BLOCK_SIZE/2))/BLOCK_SIZE) &&
			 (int)((v[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((v[2]-(BLOCK_SIZE/2))/BLOCK_SIZE))
	{
		u0 = (int)((u[0] + u[1] + u[2])/3);
		v0 = (int)((v[0] + v[1] + v[2])/3) - (BLOCK_SIZE/2);

		u0 /= BLOCK_SIZE;
		v0 /= BLOCK_SIZE;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BLOCK_SIZE, BLOCK_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, Input::imagenesDown[indiceImagen][BLOCK_COLS*(v0)+(u0)]);

		glBegin(GL_TRIANGLES);

		glTexCoord2f((u[0]%BLOCK_SIZE)/(double)BLOCK_SIZE, ((v[0]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(p->GetX(),p->GetY(),p->GetZ());

		glTexCoord2f((u[1]%BLOCK_SIZE)/(double)BLOCK_SIZE, ((v[1]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(q->GetX(),q->GetY(),q->GetZ());

		glTexCoord2f((u[2]%BLOCK_SIZE)/(double)BLOCK_SIZE, ((v[2]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(r->GetX(),r->GetY(),r->GetZ());

		glEnd();
	}
	//cross block
	else if ((int)((u[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((u[1]-(BLOCK_SIZE/2))/BLOCK_SIZE) &&
			 (int)((u[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((u[2]-(BLOCK_SIZE/2))/BLOCK_SIZE) &&
			 (int)((v[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((v[1]-(BLOCK_SIZE/2))/BLOCK_SIZE) &&
			 (int)((v[0]-(BLOCK_SIZE/2))/BLOCK_SIZE) == (int)((v[2]-(BLOCK_SIZE/2))/BLOCK_SIZE))
	{
		u0 = (int)((u[0] + u[1] + u[2])/3) - (BLOCK_SIZE/2);
		v0 = (int)((v[0] + v[1] + v[2])/3) - (BLOCK_SIZE/2);

		u0 /= BLOCK_SIZE;
		v0 /= BLOCK_SIZE;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BLOCK_SIZE, BLOCK_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, Input::imagenesCross[indiceImagen][BLOCK_COLS*(v0)+(u0)]);

		glBegin(GL_TRIANGLES);

		glTexCoord2f(((u[0]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE, ((v[0]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(p->GetX(),p->GetY(),p->GetZ());

		glTexCoord2f(((u[1]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE, ((v[1]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(q->GetX(),q->GetY(),q->GetZ());

		glTexCoord2f(((u[2]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE, ((v[2]-(BLOCK_SIZE/2))%BLOCK_SIZE)/(double)BLOCK_SIZE);
		glVertex3f(r->GetX(),r->GetY(),r->GetZ());

		glEnd();
	}
}

void OpenGL::Finalizar()
{
	glutDestroyWindow(ogl->ventana);
	ExitProcess(0);
}

GLvoid WindowKeyDown(unsigned char c, int x, int y)
{
	switch(c)
	{
		case VK_ESCAPE:
			ogl->Finalizar();
			break;
		case 'x':
			xPressed = true;
			yPressed = false;
			zPressed = false;
			break;
		case 'y':
			xPressed = false;
			yPressed = true;
			zPressed = false;
			break;
		case 'z':
			xPressed = false;
			yPressed = false;
			zPressed = true;
			break;
		case 'X':
			xPressed = true;
			yPressed = false;
			zPressed = false;
			break;
		case 'Y':
			xPressed = false;
			yPressed = true;
			zPressed = false;
			break;
		case 'Z':
			xPressed = false;
			yPressed = false;
			zPressed = true;
			break;
		case '+':
			if (xPressed)
				RotarX(5);
			else if (yPressed)
				RotarY(5);
			else if (zPressed)
				RotarZ(5);
			else
				Zoom(1);
			break;
		case '-':
			if (xPressed)
				RotarX(-5);
			else if (yPressed)
				RotarY(-5);
			else if (zPressed)
				RotarZ(-5);
			else
				Zoom(-1);
			break;
	}
}

GLvoid WindowKeyUp(unsigned char c, int x, int y)
{
	switch(c)
	{
		case VK_ESCAPE:
			ogl->Finalizar();
			break;
		case 'x':
			xPressed = false;
			break;
		case 'y':
			yPressed = false;
			break;
		case 'z':
			zPressed = false;
			break;
		case 'X':
			xPressed = false;
			break;
		case 'Y':
			yPressed = false;
			break;
		case 'Z':
			zPressed = false;
			break;
	}
}

void RotarX(double angulo)
{
	(ogl->phiX) += angulo;
}

void RotarY(double angulo)
{
	(ogl->phiY) += angulo;
}

void RotarZ(double angulo)
{
	(ogl->phiZ) += angulo;
}

void Zoom(double zoom)
{
	(ogl->rho) -= zoom;
}