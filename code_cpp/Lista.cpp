#include "StdAfx.h"
#include ".\lista.h"

Lista::Lista(void)
{
	this->first = NULL;
	this->last = NULL;
}

void Lista::AgregarElemento(Celda *c)
{
	if (this->Empty())
	{
		first = c;
		last = c;
	}
	else
		last->SetNext(c);
}

Celda *Lista::ObtenerElemento()
{
	return first;
}

void Lista::RemoverElemento()
{
	Celda *aux;
	aux = first->GetNext();
	delete first;
	first = aux;
}

bool Lista::Empty()
{
	if (first == NULL)
		return true;
	else
		return false;
}

Lista::~Lista(void)
{
}
