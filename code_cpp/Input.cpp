#include "StdAfx.h"
#include ".\input.h"

bool Input::numTrianglesLeido;
bool Input::numVerticesLeido;

double **Input::medicionesLaser;
double **Input::angulosPhi;
double **Input::angulosTheta;
Vertex **Input::listadoVertices;
Triangle **Input::listadoTriangulos;
int		 Input::totalVertices;
int		 Input::totalTriangulos;

CapturaCamara Input::camara;
Stepper Input::motor;
LaserSick Input::laser;
int Input::tilt;
FlyCaptureImage* Input::imagen;
char* Input::modelName;
char* Input::modelDir;
FILE* Input::fileMedicionesLaser;
BOOL Input::texturizar;
unsigned char Input::imagenes[NUM_IMAGENES][BLOCK_ROWS*BLOCK_COLS][BLOCK_SIZE][BLOCK_SIZE][3];
unsigned char Input::imagenesDown[NUM_IMAGENES][(BLOCK_ROWS-1)*BLOCK_COLS][BLOCK_SIZE][BLOCK_SIZE][3];
unsigned char Input::imagenesRight[NUM_IMAGENES][BLOCK_ROWS*(BLOCK_COLS-1)][BLOCK_SIZE][BLOCK_SIZE][3];
unsigned char Input::imagenesCross[NUM_IMAGENES][(BLOCK_ROWS-1)*(BLOCK_COLS-1)][BLOCK_SIZE][BLOCK_SIZE][3];

bool Input::PosicionarStepper(int puertoStepper)
{
	if (!motor.IniciarComunicacion(puertoStepper))
		return false;

	motor.HalfBack();
	motor.WaitMovement();
	motor.TerminarComunicacion();

	return true;
}

bool Input::RewindStepper(int puertoStepper)
{
	if (!motor.IniciarComunicacion(puertoStepper))
		return false;

	motor.GoToStart();
	motor.WaitMovement();
	motor.TerminarComunicacion();

	return true;
}

void Input::Inicializar()
{
	int i;

	//inicializar arreglo de mediciones
	medicionesLaser = (double**)malloc(NUM_TILTS*sizeof(double));
	angulosPhi = (double**)malloc(NUM_TILTS*sizeof(double));
	angulosTheta = (double**)malloc(NUM_TILTS*sizeof(double));

	for(i=0; i<NUM_TILTS; ++i)
	{
		medicionesLaser[i] = (double*)malloc(NUM_PANS*sizeof(double));
		angulosPhi[i] = (double*)malloc(NUM_PANS*sizeof(double));
		angulosTheta[i] = (double*)malloc(NUM_PANS*sizeof(double));
	}
}

bool Input::Scan(int puertoLaser, int puertoStepper)
{
	char *s;
	int dummy = 0;
	clock_t t1, t2;

	Inicializar();

	s = new char[200];

	sprintf_s(s, 200, "%sdatosLaser.txt", Input::GetModelDir());
	fileMedicionesLaser = fopen(s, "w");

	t1 = clock();

	if (!motor.IniciarComunicacion(puertoStepper))
		return false;

	if (Input::GetTexturizar())
		camara.IniciarCamara();

	if (!laser.Inicializar(puertoLaser))
		return false;

	for(tilt=0; tilt<NUM_TILTS; ++tilt)
	{
		if (Input::GetTexturizar() && (tilt==TILT_IMG1 || tilt==TILT_IMG2 || tilt==TILT_IMG3))
		{
			motor.WaitMovement();
			dummy = dummy;
			Input::GetImage();
		}
		else
		{
			motor.WaitMovement();
		}

		laser.PedirDatos();

		if (tilt < NUM_TILTS-1)
			motor.Move();

		Input::GetLaserScan(tilt);
	}

	//FINALIZAR EQUIPOS
	laser.Finalizar();
	motor.WaitMovement();
	motor.TerminarComunicacion();
	t2 = clock();

	fclose(fileMedicionesLaser);

	FILE *time;
	time = fopen("time.txt", "w");
	fprintf(time, "%d", (int)(t2-t1));
	fclose(time);

	return true;
}

void Input::CargarDatos(char *f)
{
	Inicializar();

	LoadLaserData(f);
	if (Input::GetTexturizar())
		CargarImagenes();
}

void Input::GenerarMesh()
{
	MeshGenerator mg;

	//CREAR MESH
	mg.GenerarMesh(medicionesLaser, angulosPhi, angulosTheta);
}

void Input::CargarMesh()
{
	if (Input::GetTexturizar())
		CargarImagenes();
}

void Input::SetModelName(char *name)
{
	unsigned int debug, i;
	char *nameCopy;
	nameCopy = new char[200];

	for(i=0; i<=strlen(name); ++i)
		nameCopy[i] = name[i];
	
	if (strchr(nameCopy, '\\') != NULL)
	{
		char *aux;
		modelDir = new char[200];
		aux = new char[200];
		int largo, i, largoDir;

		aux = strrchr(nameCopy,'\\')+1;
		largo = (int)(strlen(aux)-4);
		aux[largo] = '\0';
		if (strlen(aux) > 200)
			debug = 0;

		largoDir = (int)(aux-nameCopy);
		for(i=0; i<largoDir; ++i)
			modelDir[i] = nameCopy[i];
		modelDir[i] = '\0';
		if (strlen(modelDir) > 200)
			debug = 0;

		modelName = aux;
	}
	else
	{
		modelDir = new char[200];
		sprintf_s(modelDir, 200, "mesh\\%s\\", nameCopy);
		if (strlen(modelDir) > 200)
			debug = 0;
		modelName = nameCopy;
	}

	CreateDirectory(modelDir, NULL);
}

char* Input::GetModelName()
{
	return modelName;
}

char* Input::GetModelDir()
{
	return modelDir;
}

void Input::GetLaserScan(int tilt)
{
	int pan, valor;

	for(pan=0; pan<NUM_PANS; ++pan)
	{
		valor = laser.ObtenerMedicionAngulo(pan);
		fprintf(fileMedicionesLaser, "%d ", valor);
		CorregirMedicion(valor, pan, tilt);
	}
	fprintf(fileMedicionesLaser, "\n");
}

void Input::LoadLaserData(char *f)
{
	FILE *archivo;
	int tilt, pan, valor;

	archivo = fopen(f, "r");
	for(tilt=0; tilt<NUM_TILTS; ++tilt)
	{
		for(pan=0; pan<NUM_PANS; ++pan)
		{
			fscanf_s(archivo, "%d", &valor);
			CorregirMedicion(valor, pan, tilt);
		}
	}
	fclose(archivo);
}

void Input::CorregirMedicion(int rho, int pan, int tilt)
{
	double theta, phi, r;
	double rho_p, theta_p, phi_p;

	theta = (ANGULO_INICIO_TILT - tilt*DEGREES_TILT)*PI/180;
	phi = pan*DEGREES_PAN*PI/180.0;

	if (phi <= PI/2.0)
		r = sqrt(DY_MIRROR*DY_MIRROR + pow(rho*cos(theta),2) - 2*DY_MIRROR*rho*cos(theta)*cos(PI/2.0 + phi));
	else
		r = sqrt(DY_MIRROR*DY_MIRROR + pow(rho*cos(theta),2) - 2*DY_MIRROR*rho*cos(theta)*cos(3.0*PI/2.0 - phi));

	rho_p = sqrt(pow(rho*sin(theta)-DZ_MIRROR,2)+r*r);
	if (pan < 180)
		phi_p = asin((rho*cos(theta)*sin(phi)+DY_MIRROR)/r);
	else if (pan == 180)
		phi_p = PI/2;
	else
		phi_p = PI - asin((rho*cos(theta)*sin(phi)+DY_MIRROR)/r);
	theta_p = atan((rho*sin(theta)-DZ_MIRROR)/r);

	medicionesLaser[tilt][pan] = rho_p;
	angulosPhi[tilt][pan] = phi_p;
	angulosTheta[tilt][pan] = theta_p;
}

void Input::GetImage()
{
	int i, j, a, b;
	int u0, v0;
	int indicePixel;

	static int num_imagen = 0;
	//Input::imagen = (FlyCaptureImage*)malloc(sizeof(FlyCaptureImage));
	imagen = camara.Capturar(num_imagen, GetModelDir());

	for(i=0; i<BLOCK_ROWS; ++i)
	{
		for(j=0; j<BLOCK_COLS; ++j)
		{
			u0 = j*BLOCK_SIZE;
			v0 = i*BLOCK_SIZE;
			for(a=0; a<BLOCK_SIZE; ++a)
			{
				for(b=0; b<BLOCK_SIZE; ++b)
				{
					indicePixel = 3*(IMG_COLS*(v0+a) + (u0+b));
					
					imagenes[num_imagen][(int)(BLOCK_COLS*i + j)][a][b][0] = imagen->pData[indicePixel];
					imagenes[num_imagen][(int)(BLOCK_COLS*i + j)][a][b][1] = imagen->pData[indicePixel + 1];
					imagenes[num_imagen][(int)(BLOCK_COLS*i + j)][a][b][2] = imagen->pData[indicePixel + 2];
				}
			}
		}
	}

	++num_imagen;
	//free(imagen);
}

void Input::CargarImagenes()
{
	int numImagen, i, j, a, b;
	int u0, v0;
	int indicePixel;

	char s[200];

	Input::imagen = (FlyCaptureImage*)malloc(sizeof(FlyCaptureImage));

	for(numImagen=0; numImagen<NUM_IMAGENES; ++numImagen)
	{
		sprintf_s(s, 200, "%scamara%d.bmp", Input::GetModelDir(), numImagen);
		imagen = LoadBMP(s);

		for(i=0; i<BLOCK_ROWS; ++i)
		{
			for(j=0; j<BLOCK_COLS; ++j)
			{
				u0 = j*BLOCK_SIZE;
				v0 = i*BLOCK_SIZE;
				for(a=0; a<BLOCK_SIZE; ++a)
				{
					for(b=0; b<BLOCK_SIZE; ++b)
					{
						indicePixel = 3*(IMG_COLS*(v0+a) + (u0+b));

						imagenes[numImagen][(int)(BLOCK_COLS*i + j)][a][b][0] = imagen->pData[indicePixel];
						imagenes[numImagen][(int)(BLOCK_COLS*i + j)][a][b][1] = imagen->pData[indicePixel+1];
						imagenes[numImagen][(int)(BLOCK_COLS*i + j)][a][b][2] = imagen->pData[indicePixel+2];

						if (i>0)
						{
							imagenesDown[numImagen][(int)(BLOCK_COLS*(i-1) + j)][a][b][0] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)*IMG_COLS];
							imagenesDown[numImagen][(int)(BLOCK_COLS*(i-1) + j)][a][b][1] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)*IMG_COLS + 1];
							imagenesDown[numImagen][(int)(BLOCK_COLS*(i-1) + j)][a][b][2] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)*IMG_COLS + 2];
						}

						if (j>0)
						{
							imagenesRight[numImagen][(int)(BLOCK_COLS*i + (j-1))][a][b][0] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)];
							imagenesRight[numImagen][(int)(BLOCK_COLS*i + (j-1))][a][b][1] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2) + 1];
							imagenesRight[numImagen][(int)(BLOCK_COLS*i + (j-1))][a][b][2] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2) + 2];
						}

						if (i>0 && j>0)
						{
							imagenesCross[numImagen][(int)(BLOCK_COLS*(i-1) + (j-1))][a][b][0] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)*IMG_COLS - 3*(BLOCK_SIZE/2)];
							imagenesCross[numImagen][(int)(BLOCK_COLS*(i-1) + (j-1))][a][b][1] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)*IMG_COLS - 3*(BLOCK_SIZE/2) + 1];
							imagenesCross[numImagen][(int)(BLOCK_COLS*(i-1) + (j-1))][a][b][2] = imagen->pData[indicePixel - 3*(BLOCK_SIZE/2)*IMG_COLS - 3*(BLOCK_SIZE/2) + 2];
						}
					}
				}
			}
		}

	}
	free(imagen);
}

int Input::GetTilt()
{
	return tilt;
}

void Input::Calibrate()
{
	int i;
	double vertices[4][3], vInternos[81][3];
	double **calObj;
	double alfa[4];
	FILE *out;
	char *s;

	s = new char[200];
	calObj = NULL;

	PointProjection(MAX_DIST_CALIBRATION, vertices, alfa);

	double c[3];

	c[0] = (vertices[0][0]+vertices[1][0]+vertices[2][0]+vertices[3][0])/4;
	c[1] = (vertices[0][1]+vertices[1][1]+vertices[2][1]+vertices[3][1])/4;
	c[2] = (vertices[0][2]+vertices[1][2]+vertices[2][2]+vertices[3][2])/4;

	PuntosInteriores(vertices, vInternos, alfa);

	sprintf_s(s, 200, "%scalibration-object.txt", Input::GetModelDir());
	out = fopen(s, "w");

	for(i=0; i<81; ++i)
	{
		//fprintf(out, "%lf\t%lf\t%lf\n", vInternos[i][2], vInternos[i][0], vInternos[i][1]);
		fprintf(out, "%lf\t%lf\t%lf\n", vInternos[i][0], vInternos[i][1], vInternos[i][2]);
		if ((i+1)%9 == 0 && i>0) fprintf(out, "\n");
	}

	fprintf(out, "\n\n");
	for(i=0; i<4; ++i)
	{
		//fprintf(out, "%lf\t%lf\t%lf\n", vertices[i][2], vertices[i][0], vertices[i][1]);
		fprintf(out, "%lf\t%lf\t%lf\n", vertices[i][0], vertices[i][1], vertices[i][2]);
	}

	fclose(out);
}

void Input::PuntosInteriores(double vertices[4][3], double vInternos[81][3], const double alfa[4])
{
	int i, j, k;
	double projFactor, error;

	for(i=0; i<9; ++i)
	{
		for(j=0; j<9; ++j)
		{
			for(k=0; k<3; ++k)
			{
				vInternos[j + 9*i][k] = vertices[0][k];

				vInternos[j + 9*i][k] += ((TAM_CELDA_CALIB-.5)/LADO_A)*(vertices[1][k] - vertices[0][k]);
				vInternos[j + 9*i][k] += ((TAM_CELDA_CALIB-.5)/LADO_B)*(vertices[3][k] - vertices[0][k]);
				vInternos[j + 9*i][k] += (TAM_CELDA_CALIB/LADO_A)*(j)*(vertices[1][k] - vertices[0][k]) + (TAM_CELDA_CALIB/LADO_B)*(i)*(vertices[3][k] - vertices[0][k]);
			}

			projFactor = (alfa[0]*vInternos[j + 9*i][0] + alfa[1]*vInternos[j + 9*i][1] + alfa[2]*vInternos[j + 9*i][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
			
			for(k=0; k<3; ++k)
				vInternos[j + 9*i][k] -= projFactor*alfa[k];
		}
	}
}

bool PuntoDentro(double v[4][3], const double alfa[4], double *p)
{
	int i;
	double nPto[3];

	for(i=0; i<4; ++i)
	{
		//se quiere ver si un punto p se encuentra dentro de los limites del cuadrado encontrado en la parte anterior
		//se calcula el producto cruz "nPto" entre la arista v(i+1)-vi del cuadrado y el vector p-vi.
		nPto[0] = (v[(i+1)%4][1]-v[i][1])*(p[2]-v[i][2]) - (v[(i+1)%4][2]-v[i][2])*(p[1]-v[i][1]);
		nPto[1] = (v[(i+1)%4][2]-v[i][2])*(p[0]-v[i][0]) - (v[(i+1)%4][0]-v[i][0])*(p[2]-v[i][2]);
		nPto[2] = (v[(i+1)%4][0]-v[i][0])*(p[1]-v[i][1]) - (v[(i+1)%4][1]-v[i][1])*(p[0]-v[i][0]);

		//si el prod. punto entre "nPto" y la normal del plano da negativa para alguna arista el pto no esta en el cuadrado
		if (alfa[0]*nPto[0] + alfa[1]*nPto[1] + alfa[2]*nPto[2] < 0)
			return false;
	}

	return true;
}

void Input::TraslateRectangle(double v[4][3], double centro[3], const double alfa[4], double **calObj, int totalPts)
{
	int i, j;
	double numPtosAux;
	double newCentro[3], desp[3];
	double projFactor;

	//se resetea el nuevo centro del rectangulo
	newCentro[0] = 0;
	newCentro[1] = 0;
	newCentro[2] = 0;

	//Se encuentra el centro de gravedad de los puntos al interior del rect�ngulo
	numPtosAux = 0;
	for(i=0; i<totalPts; ++i)
	{
		if (PuntoDentro(v, alfa, calObj[i]))
		{
			//es importante incrementar este contador antes de actualizar los valores.
			++numPtosAux;
			//si esta dentro del cuadrado se agrega su posicion al calculo del nuevo centro de gravedad
			newCentro[0] += (calObj[i][0] - newCentro[0])/numPtosAux;
			newCentro[1] += (calObj[i][1] - newCentro[1])/numPtosAux;
			newCentro[2] += (calObj[i][2] - newCentro[2])/numPtosAux;
		}
	}

	//el nuevo centro es proyectado sobre el plano del objeto de calibracion (por si se hubiera salido del plano)
	projFactor = (alfa[0]*newCentro[0] + alfa[1]*newCentro[1] + alfa[2]*newCentro[2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
	newCentro[0] -= projFactor*alfa[0];
	newCentro[1] -= projFactor*alfa[1];
	newCentro[2] -= projFactor*alfa[2];

	//se calcula el desplazamiento que sufrio el centro del cuadrado
	desp[0] = newCentro[0] - centro[0];
	desp[1] = newCentro[1] - centro[1];
	desp[2] = newCentro[2] - centro[2];

	//se aplica el mismo desplazamiento a los v�rtices del cuadrado
	for(i=0; i<4; ++i)
	{
		for(j=0; j<3; ++j)
			v[i][j] += desp[j];
		
		//se proyecta el vertice i sobre el plano (por si se hubiera salido)
		projFactor = (alfa[0]*v[i][0] + alfa[1]*v[i][1] + alfa[2]*v[i][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
		v[i][0] -= projFactor*alfa[0];
		v[i][1] -= projFactor*alfa[1];
		v[i][2] -= projFactor*alfa[2];
	}
}

void Input::RotateRectangle(double v[4][3], double theta, const double alfa[4])
{
	int i, j;
	double c[3], u[3], w[3];
	double den, R, projFactor;
	
	R = K/2.0;

	c[0] = c[1] = c[2] = 0.0;
	for(i=0; i<4; ++i)
		for(j=0; j<3; ++j)
			c[j] += v[i][j]/4.0;

	for(i=0; i<4; ++i)
	{
		u[0] = v[i][0] - c[0];
		u[1] = v[i][1] - c[1];
		u[2] = v[i][2] - c[2];
		den = sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
		u[0] /= den;
		u[1] /= den;
		u[2] /= den;
		
		w[0] = alfa[1]*u[2] - alfa[2]*u[1];
		w[1] = alfa[2]*u[0] - alfa[0]*u[2];
		w[2] = alfa[0]*u[1] - alfa[1]*u[0];
		den = sqrt(w[0]*w[0]+w[1]*w[1]+w[2]*w[2]);
		w[0] /= den;
		w[1] /= den;
		w[2] /= den;

		v[i][0] = R*cos(theta)*u[0] + R*sin(theta)*w[0] + c[0];
		v[i][1] = R*cos(theta)*u[1] + R*sin(theta)*w[1] + c[1];
		v[i][2] = R*cos(theta)*u[2] + R*sin(theta)*w[2] + c[2];
		projFactor = (alfa[0]*v[i][0] + alfa[1]*v[i][1] + alfa[2]*v[i][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
		v[i][0] -= projFactor*alfa[0];
		v[i][1] -= projFactor*alfa[1];
		v[i][2] -= projFactor*alfa[2];
	}
}

void Input::CalibrationModelCenterCorrection(double centro[3], double v[4][3], const double alfa[4], double **calObj, int totalPts)
{
	int i, j, k, cont, m;
	double newV[3][4][3], auxV[4][3];
	int ptsInteriores, numPtsInteriores[3], newPtsAux;

	while(true)
	{
		//Se cuentan los puntos interiores antes de hacer ningun cambio.
		ptsInteriores = 0;
		for(i=0; i<totalPts; ++i)
			if (PuntoDentro(v, alfa, calObj[i]))
				++ptsInteriores;

		/********************************/
		//Se trasladar� el centro del rectangulo.
		//Se copian los valores iniciales de los v�rtices
		for(i=0; i<4; ++i)
			for(j=0; j<3; ++j)
				newV[0][i][j] = v[i][j];

		TraslateRectangle(newV[0], centro, alfa, calObj, totalPts);

		//se cuenta el n�mero de puntos al interior del rect�ngulo
		numPtsInteriores[0] = 0;
		for(i=0; i<totalPts; ++i)
			if (PuntoDentro(newV[0], alfa, calObj[i]))
				++numPtsInteriores[0];


		/********************************/
		for(m=1; m<3; ++m)
		{
			numPtsInteriores[m] = ptsInteriores;
			//newV es el mejor valor de la rotacion. auxV guarda el valor de cada iteracion.
			for(i=0; i<4; ++i)
				for(j=0; j<3; ++j)
				{
					if (m==1) newV[m][i][j] = v[i][j];
					else newV[m][i][j] = newV[0][i][j];
				}
			
			for (k=-1; k<2; k+=2)
			{
				//auxV guarda el valor de cada iteracion. Por defecto es igual que no rotarlo.
				for(i=0; i<4; ++i)
					for(j=0; j<3; ++j)
					{
						if (m==1) auxV[i][j] = v[i][j];
						else auxV[i][j] = newV[0][i][j];
					}

				//se rota el rectangulo 100 veces en sentido horario buscando el angulo que maximice el N� de puntos dentro del rectangulo
				//con k=-1 se rota en sentido antihorario y k=1 en sentido horario.
				for(cont=0; cont<100; ++cont)
				{
					//se rota en 0.001 radianes
					RotateRectangle(auxV, 0.001*k, alfa);

					//se cuenta el n�mero de puntos al interior del rect�ngulo
					newPtsAux = 0;
					for(i=0; i<totalPts; ++i)
						if (PuntoDentro(auxV, alfa, calObj[i]))
							++newPtsAux;

					//si el N� de pts es mayor se setean estos valores como los mejores del giro.
					if (newPtsAux > numPtsInteriores[1])
					{
						for(i=0; i<4; ++i)
							for(j=0; j<3; ++j)
								newV[1][i][j] = auxV[i][j];

						numPtsInteriores[1] = newPtsAux;
					}
				}
			}
		}

		if (ptsInteriores >= numPtsInteriores[0] && ptsInteriores >= numPtsInteriores[1] && ptsInteriores >= numPtsInteriores[2])
			break;
		else
		{
			for(k=0; k<3; ++k)
			{
				if (numPtsInteriores[k] >= numPtsInteriores[(int)(k+1)%3] && numPtsInteriores[k] >= numPtsInteriores[(int)(k+2)%3] && numPtsInteriores[k] >= ptsInteriores)
				{
					ptsInteriores = numPtsInteriores[k];
					for(i=0; i<4; ++i)
						for(j=0; j<3; ++j)
							v[i][j] = newV[k][i][j];
					break;
				}
			}
		}
	}
}

void Input::PointProjection(int maxDistance, double v[4][3], double alfa[4])
{
	register int i, j, mult;
	double valor, theta, phi;
	double x, y, z;
	double R;
	
	double XPtXP[3][3], XPtYP[3];
	double BP[3];
	int indPto, numPts;
	double **inversa;
	double projFactor;
	double **calObj;
	FILE *out;
	double c[3];

	//se inicializan los vertices del objeto de calibracion
	v[0][2] = -DBL_MAX;
	v[1][0] = -DBL_MAX;
	v[2][2] = DBL_MAX;
	v[3][0] = DBL_MAX;

	//inicializacion de las variables para hacer la regresion lineal que sirve para encontrar el plano que mejor aproxima los puntos.
	for(i=0; i<3; ++i)
	{
		XPtYP[i] = 0;
		for(j=0; j<3; ++j)
			XPtXP[i][j] = 0;
	}

	numPts = 0;
	for(i=0; i<NUM_TILTS; ++i)
	{
		for(j=MIN_SEL_CAL; j<MAX_SEL_CAL; ++j)
		{
			//se pasa el punto i,j a coordenadas rectangulares
			valor = medicionesLaser[i][j];
			phi = angulosPhi[i][j];
			theta = angulosTheta[i][j];

			Input::PolarToRectangular(valor, phi, theta, &x, &y, &z);

			//se cuenta el n�mero de puntos que pertenecen al obejto de calibracion
			if (valor < maxDistance && y < maxDistance)
			{
				++numPts;

				//se setean las variables para la regresion lineal
				XPtXP[0][0] += 1;
				XPtXP[0][1] += x;
				XPtXP[0][2] += z;

				XPtXP[1][0] += x;
				XPtXP[1][1] += x*x;
				XPtXP[1][2] += x*z;

				XPtXP[2][0] += z;
				XPtXP[2][1] += z*x;
				XPtXP[2][2] += z*z;

				XPtYP[0] += y;
				XPtYP[1] += x*y;
				XPtYP[2] += z*y;
			}
		}
	}

	//calcula la inversa de XPtXP para resolver la ecuacion B = INV(trans(XP)*XP)*trans(XP)*Yi
	inversa = MatrizInversa(XPtXP);

	//calcula los aprametros para resolver la ecuacion a0*x + a1*y + a2*z = a3 (No recuerdo si BP tiene relacion con B de la ecuacion de arriba)
	BP[0] = inversa[0][0]*XPtYP[0] + inversa[0][1]*XPtYP[1] + inversa[0][2]*XPtYP[2];	//	=  alfa[3]/alfa[1]
	BP[1] = inversa[1][0]*XPtYP[0] + inversa[1][1]*XPtYP[1] + inversa[1][2]*XPtYP[2];	//	= -alfa[0]/alfa[1]
	BP[2] = inversa[2][0]*XPtYP[0] + inversa[2][1]*XPtYP[1] + inversa[2][2]*XPtYP[2];	//	= -alfa[2]/alfa[1]

	//calcula los parametros de la ecuacion del plano
	alfa[1] = 1/(1 + BP[0] - BP[1] - BP[2]);
	if (alfa[1]<0) mult = -1;
	else mult = 1;
	alfa[0] = -BP[1]*alfa[1]*mult;
	alfa[2] = -BP[2]*alfa[1]*mult;
	alfa[3] =  BP[0]*alfa[1]*mult;

	//Proyecta los puntos del objeto de calibracion sobre el plano encontrado
	out = fopen("puntos_del_objeto.txt", "w");
	calObj = (double**)malloc(numPts*sizeof(double));

	double v0, vd, t, theta0;

	R = sqrt(DY_MIRROR*DY_MIRROR + DZ_MIRROR*DZ_MIRROR);
	theta0 = atan(DZ_MIRROR/DY_MIRROR);

	indPto = 0;
	for(i=0; i<NUM_TILTS; ++i)
	{
		c[0] = 0;
		c[1] = R*cos(theta0 + DEGREES_TILT*(i - (NUM_TILTS-1)/2.0)*(PI/180.0));
		c[2] = R*sin(theta0 + DEGREES_TILT*(i - (NUM_TILTS-1)/2.0)*(PI/180.0));

		for(j=MIN_SEL_CAL; j<MAX_SEL_CAL; ++j)
		{
			//se pasa el punto i,j a coordenadas rectangulares
			valor = medicionesLaser[i][j];
			phi = angulosPhi[i][j];
			theta = angulosTheta[i][j];

			Input::PolarToRectangular(valor, phi, theta, &x, &y, &z);

			//Se seleccionan los puntos que se encuentran dentro de un cierto rango - o sea, q solo son del objeto de calibracion
			if (valor < maxDistance && y < maxDistance)
			{
				v0 = alfa[3] - (alfa[0]*c[0] + alfa[1]*c[1] + alfa[2]*c[2]);
				vd = alfa[0]*(x-c[0]) + alfa[1]*(y-c[1]) + alfa[2]*(z-c[2]);
				t = v0/vd;

				calObj[indPto] = (double*)malloc(3*sizeof(double));
				/*projFactor = (alfa[0]*x + alfa[1]*y + alfa[2]*z - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
				calObj[indPto][0] -= projFactor*alfa[0];
				calObj[indPto][1] -= projFactor*alfa[1];
				calObj[indPto][2] -= projFactor*alfa[2];*/

				calObj[indPto][0] = c[0] + t*(x-c[0]);
				calObj[indPto][1] = c[1] + t*(y-c[1]);
				calObj[indPto][2] = c[2] + t*(z-c[2]);

				fprintf(out, "%lf\t%lf\t%lf\n", calObj[indPto][0], calObj[indPto][1], calObj[indPto][2]);

				//se seleccionan los vertices del diamantes (nuestra figura de calibracion)
				if (calObj[indPto][2] > v[0][2])
				{
					v[0][0] = calObj[indPto][0];
					v[0][1] = calObj[indPto][1];
					v[0][2] = calObj[indPto][2];
				}
				if (calObj[indPto][0] > v[1][0])
				{
					v[1][0] = calObj[indPto][0];
					v[1][1] = calObj[indPto][1];
					v[1][2] = calObj[indPto][2];
				}
				if (calObj[indPto][2] < v[2][2])
				{
					v[2][0] = calObj[indPto][0];
					v[2][1] = calObj[indPto][1];
					v[2][2] = calObj[indPto][2];
				}
				if (calObj[indPto][0] < v[3][0])
				{
					v[3][0] = calObj[indPto][0];
					v[3][1] = calObj[indPto][1];
					v[3][2] = calObj[indPto][2];
				}

				++indPto;
			}
		}
	}

	fclose(out);

	//se encuentra el punto promedio de los puntos del obejto de calibracion.
	//este punto ser� el centro inicial del modelo del objeto.
	x = y = z = 0.0;

	for(i=0; i<numPts; ++i)
	{
		x += calObj[i][0];
		y += calObj[i][1];
		z += calObj[i][2];
	}
	x /= numPts;
	y /= numPts;
	z /= numPts;

	c[0] = x;
	c[1] = y;
	c[2] = z;

	double u[3], w[3], n[3], den, error, d;
	R = K/2;


	u[0] = v[0][0] - c[0];
	u[1] = v[0][1] - c[1];
	u[2] = v[0][2] - c[2];
	den = sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
	u[0] /= den;
	u[1] /= den;
	u[2] /= den;

	n[0] = alfa[0] / (alfa[0] + alfa[1] + alfa[2]);
	n[1] = alfa[1] / (alfa[0] + alfa[1] + alfa[2]);
	n[2] = alfa[2] / (alfa[0] + alfa[1] + alfa[2]);
	
	w[0] = n[1]*u[2] - n[2]*u[1];
	w[1] = n[2]*u[0] - n[0]*u[2];
	w[2] = n[0]*u[1] - n[1]*u[0];
	den = sqrt(w[0]*w[0]+w[1]*w[1]+w[2]*w[2]);
	w[0] /= den;
	w[1] /= den;
	w[2] /= den;

	theta = 0;
	v[0][0] = R*u[0] + c[0];
	v[0][1] = R*u[1] + c[1];
	v[0][2] = R*u[2] + c[2];
	projFactor = (alfa[0]*v[0][0] + alfa[1]*v[0][1] + alfa[2]*v[0][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
	v[0][0] -= projFactor*alfa[0];
	v[0][1] -= projFactor*alfa[1];
	v[0][2] -= projFactor*alfa[2];

	theta = ANGULO_A;
	v[1][0] = R*cos(theta)*u[0] + R*sin(theta)*w[0] + c[0];
	v[1][1] = R*cos(theta)*u[1] + R*sin(theta)*w[1] + c[1];
	v[1][2] = R*cos(theta)*u[2] + R*sin(theta)*w[2] + c[2];
	projFactor = (alfa[0]*v[1][0] + alfa[1]*v[1][1] + alfa[2]*v[1][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
	v[1][0] -= projFactor*alfa[0];
	v[1][1] -= projFactor*alfa[1];
	v[1][2] -= projFactor*alfa[2];

	theta = PI;
	v[2][0] = R*cos(theta)*u[0] + R*sin(theta)*w[0] + c[0];
	v[2][1] = R*cos(theta)*u[1] + R*sin(theta)*w[1] + c[1];
	v[2][2] = R*cos(theta)*u[2] + R*sin(theta)*w[2] + c[2];
	projFactor = (alfa[0]*v[2][0] + alfa[1]*v[2][1] + alfa[2]*v[2][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
	v[2][0] -= projFactor*alfa[0];
	v[2][1] -= projFactor*alfa[1];
	v[2][2] -= projFactor*alfa[2];

	theta = -1*ANGULO_B;
	v[3][0] = R*cos(theta)*u[0] + R*sin(theta)*w[0] + c[0];
	v[3][1] = R*cos(theta)*u[1] + R*sin(theta)*w[1] + c[1];
	v[3][2] = R*cos(theta)*u[2] + R*sin(theta)*w[2] + c[2];
	projFactor = (alfa[0]*v[3][0] + alfa[1]*v[3][1] + alfa[2]*v[3][2] - alfa[3])/sqrt(alfa[0]*alfa[0] + alfa[1]*alfa[1] + alfa[2]*alfa[2]);
	v[3][0] -= projFactor*alfa[0];
	v[3][1] -= projFactor*alfa[1];
	v[3][2] -= projFactor*alfa[2];

	error = ErrorCalibracion(c, v);
	CalibrationModelCenterCorrection(c, v, alfa, calObj, numPts);
	error = ErrorCalibracion(c, v);
}

double Input::ErrorCalibracion(double c[3], double v[4][3])
{
	double error;
	double e0, e1, e2, e3, e4, e5, e6, e7;
	double var[20];

	var[0] = (v[0][0]-c[0])*(v[0][0]-c[0]) + (v[0][1]-c[1])*(v[0][1]-c[1]) + (v[0][2]-c[2])*(v[0][2]-c[2]);
	var[1] = (v[1][0]-c[0])*(v[1][0]-c[0]) + (v[1][1]-c[1])*(v[1][1]-c[1]) + (v[1][2]-c[2])*(v[1][2]-c[2]);
	var[2] = (v[2][0]-c[0])*(v[2][0]-c[0]) + (v[2][1]-c[1])*(v[2][1]-c[1]) + (v[2][2]-c[2])*(v[2][2]-c[2]);
	var[3] = (v[3][0]-c[0])*(v[3][0]-c[0]) + (v[3][1]-c[1])*(v[3][1]-c[1]) + (v[3][2]-c[2])*(v[3][2]-c[2]);

	var[4] = (v[0][0]-c[0])*(v[1][0]-c[0]) + (v[0][1]-c[1])*(v[1][1]-c[1]) + (v[0][2]-c[2])*(v[1][2]-c[2]);
	var[5] = (v[1][0]-c[0])*(v[2][0]-c[0]) + (v[1][1]-c[1])*(v[2][1]-c[1]) + (v[1][2]-c[2])*(v[2][2]-c[2]);
	var[6] = (v[2][0]-c[0])*(v[3][0]-c[0]) + (v[2][1]-c[1])*(v[3][1]-c[1]) + (v[2][2]-c[2])*(v[3][2]-c[2]);
	var[7] = (v[3][0]-c[0])*(v[0][0]-c[0]) + (v[3][1]-c[1])*(v[0][1]-c[1]) + (v[3][2]-c[2])*(v[0][2]-c[2]);

	var[8] = (v[2][0]-v[1][0])*(v[2][0]-v[1][0]) + (v[2][1]-v[1][1])*(v[2][1]-v[1][1]) + (v[2][2]-v[1][2])*(v[2][2]-v[1][2]);
	var[9] = (v[2][0]-v[3][0])*(v[2][0]-v[3][0]) + (v[2][1]-v[3][1])*(v[2][1]-v[3][1]) + (v[2][2]-v[3][2])*(v[2][2]-v[3][2]);
	var[10]= (v[0][0]-v[3][0])*(v[0][0]-v[3][0]) + (v[0][1]-v[3][1])*(v[0][1]-v[3][1]) + (v[0][2]-v[3][2])*(v[0][2]-v[3][2]);
	var[11]= (v[0][0]-v[1][0])*(v[0][0]-v[1][0]) + (v[0][1]-v[1][1])*(v[0][1]-v[1][1]) + (v[0][2]-v[1][2])*(v[0][2]-v[1][2]);
	
	//calcula el error del modelo
	e0 = abs(LADO_A-sqrt(var[11]));
	e1 = abs(LADO_B-sqrt(var[8] ));
	e2 = abs(LADO_A-sqrt(var[9] ));
	e3 = abs(LADO_B-sqrt(var[10]));
	
	e4 = K*abs(ANGULO_A - acos(	var[4]/(sqrt(var[0])*sqrt(var[1])) ) );
	e5 = K*abs(ANGULO_B - acos(	var[5]/(sqrt(var[1])*sqrt(var[2])) ) );
	e6 = K*abs(ANGULO_A - acos(	var[6]/(sqrt(var[2])*sqrt(var[3])) ) );
	e7 = K*abs(ANGULO_B - acos(	var[7]/(sqrt(var[3])*sqrt(var[0])) ) );

	error = ((e0+e1+e2+e3)/4 + (e4+e5+e6+e7)/4)/2;

	return error;
}

double **Input::MatrizInversa(double m[3][3])
{
	int i;
	double determinante;
	double **respuesta;

	determinante = m[0][0]*(m[1][1]*m[2][2]-m[1][2]*m[2][1]) - m[0][1]*(m[1][0]*m[2][2]-m[1][2]*m[2][0]) + m[0][2]*(m[1][0]*m[2][1] - m[1][1]*m[2][0]);

	if (determinante == 0)
		return NULL;
	else
	{

		respuesta = (double**)malloc(3*sizeof(double*));
		for (i=0;i<3;i++)
			respuesta[i]=(double*)malloc(3*sizeof(double));

		respuesta[0][0] = (m[1][1]/determinante*m[2][2]-m[1][2]/determinante*m[2][1]);
		respuesta[0][1] = (m[0][2]/determinante*m[2][1]-m[0][1]/determinante*m[2][2]);
		respuesta[0][2] = (m[0][1]/determinante*m[1][2]-m[0][2]/determinante*m[1][1]);

		respuesta[1][0] = (m[1][2]/determinante*m[2][0]-m[1][0]/determinante*m[2][2]);
		respuesta[1][1] = (m[0][0]/determinante*m[2][2]-m[0][2]/determinante*m[2][0]);
		respuesta[1][2] = (m[0][2]/determinante*m[1][0]-m[0][0]/determinante*m[1][2]);

		respuesta[2][0] = (m[1][0]/determinante*m[2][1]-m[1][1]/determinante*m[2][0]);
		respuesta[2][1] = (m[0][1]/determinante*m[2][0]-m[0][0]/determinante*m[2][1]);
		respuesta[2][2] = (m[0][0]/determinante*m[1][1]-m[0][1]/determinante*m[1][0]);

		return respuesta;
	}
}

void Input::PolarToRectangular(double rho, double phi, double theta, double *x, double *y, double *z)
{
	*x = rho * cos(phi);
	*y = rho * sin(phi) * cos(theta);
	*z = rho * sin(phi) * sin(theta);
}

FlyCaptureImage* Input::LoadBMP(char *filename)
{
	FILE* fileBMP;
	BITMAPFILEHEADER bmpFile;
	BITMAPINFOHEADER *bmpInfo;
	byte *bmpImage = NULL;
	byte *tempBmpImage = NULL;
	//TCHAR fullPath[256];
	FlyCaptureImage* imagen;

	bmpInfo = (BITMAPINFOHEADER*)malloc(sizeof(BITMAPINFOHEADER));

	//OPEN FILE
/*	GetModuleFileName(NULL, fullPath, 256);
	TCHAR *pos = strrchr(fullPath, '\\');
	*(pos + 1) = '\0';

	sprintf(fullPath, "%s%s", fullPath, "..\\mesh\\alvaro\\");
	strcat(fullPath, filename);*/
	
	fileBMP = fopen(filename,"rb");
	
	if (!fileBMP)
	{
		MessageBox(NULL, "Can't Find Bitmap", "Error", MB_OK);
	}
	else
	{
		//BITMAPFILEHEADER
		fread(&bmpFile,sizeof(BITMAPFILEHEADER),1,fileBMP);

		if (bmpFile.bfType == 0x4D42)
		{
			//BITMAPINFOHEADER
			fread(bmpInfo,sizeof(BITMAPINFOHEADER),1,fileBMP);

			fseek(fileBMP,bmpFile.bfOffBits,SEEK_SET);

			bmpImage = new unsigned char[bmpInfo->biSizeImage];
			tempBmpImage = new unsigned char[bmpInfo->biSizeImage];

			if (!tempBmpImage)
			{
				MessageBox(NULL, "Out of Memory", "Error", MB_OK);
				delete[] tempBmpImage;
				delete[] bmpImage;
			}
			else
			{
				fread(tempBmpImage,1,bmpInfo->biSizeImage,fileBMP);

				//LOAD IMAGE
				if (!tempBmpImage)
				{
					MessageBox(NULL, "Error reading bitmap", "Error", MB_OK);
				}
				else
				{
					//BGR to RGB & inverse
					int n = bmpInfo->biSizeImage;
					int i, j;

					for (i=0; i<bmpInfo->biHeight; ++i)
					{
						for (j=0; j<bmpInfo->biWidth; ++j)
						{
							bmpImage[3*bmpInfo->biWidth*(bmpInfo->biHeight -i -1) + 3*j]   = tempBmpImage[3*bmpInfo->biWidth*i + 3*j+2];
							bmpImage[3*bmpInfo->biWidth*(bmpInfo->biHeight -i -1) + 3*j+1] = tempBmpImage[3*bmpInfo->biWidth*i + 3*j+1];
							bmpImage[3*bmpInfo->biWidth*(bmpInfo->biHeight -i -1) + 3*j+2] = tempBmpImage[3*bmpInfo->biWidth*i + 3*j];
						}
					}

					imagen = (FlyCaptureImage*)malloc(sizeof(FlyCaptureImage));
					imagen->iCols = bmpInfo->biWidth;
					imagen->iRows = bmpInfo->biHeight;
					imagen->pData = bmpImage;
				}
			}
		}
		else
		{
			MessageBox(NULL, "Incorrect texture type", "Error", MB_OK);
		}
		fclose(fileBMP);
	}

	return imagen;
}

void Input::SetTexturizar(BOOL texturizar)
{
	Input::texturizar = texturizar;
}

BOOL Input::GetTexturizar()
{
	return Input::texturizar;
}

//LISTAS DE ELEMENTOS
void Input::InicializarListaVertices(int largo)
{
	int i;
	totalVertices = 0;
	Input::listadoVertices = (Vertex**)malloc(largo*sizeof(Vertex));
	for(i=0; i<largo; ++i)
		Input::listadoVertices[i] = NULL;
}

void Input::InicializarListaTriangulos(int largo)
{
	totalTriangulos = 0;
	Input::listadoTriangulos = (Triangle**)malloc(largo*sizeof(Triangle));
}

void Input::SaveTriangle(int indiceV1, int indiceV2, int indiceV3, Vertex *v1, Vertex *v2, Vertex *v3)
{
	if (Input::listadoVertices[indiceV1] == NULL)
		Input::listadoVertices[indiceV1] = new Vertex(v1->GetX(), v1->GetY(), v1->GetZ());
	if (Input::listadoVertices[indiceV2] == NULL)
		Input::listadoVertices[indiceV2] = new Vertex(v2->GetX(), v2->GetY(), v2->GetZ());
	if (Input::listadoVertices[indiceV3] == NULL)
		Input::listadoVertices[indiceV3] = new Vertex(v3->GetX(), v3->GetY(), v3->GetZ());
	
	Input::listadoTriangulos[totalTriangulos] = new Triangle(Input::listadoVertices[indiceV1], Input::listadoVertices[indiceV2], Input::listadoVertices[indiceV3]);
	++totalTriangulos;
}

Vertex *Input::ReadVertex()
{
	static int position = 0;

	if (position >= totalVertices)
		return false;

	return Input::listadoVertices[position++];
}

Triangle* Input::ReadTriangle()
{
	static int position = 0;

	if (position >= totalTriangulos)
		return NULL;

	return Input::listadoTriangulos[position++];
}

int Input::GetNumTriangulos()
{
	return totalTriangulos;
}

int Input::GetNumVertices()
{
	return totalVertices;
}

Vertex** Input::GetListadoVertices()
{
	return listadoVertices;
}

void Input::LiberarListas()
{
	free(listadoVertices);
	free(listadoTriangulos);
}