#include "StdAfx.h"
#include ".\capturacamara.h"

CapturaCamara::CapturaCamara(void)
{
	
}

void CapturaCamara::IniciarCamara(void)
{
	int i;

	uiCurTime     = 0;
	uiLastTime    = 0;
	uiTotalTime   = 0;
	uiSeconds     = 0;
	uiCount		  = 0;
	uiOffset      = 0;
	dGrabTime     = 0.0;

	// Create context
	for(i=0; i<NUM_IMAGENES; ++i)
	{
		error = ::flycaptureCreateContext( &(context[i]) );
		CHECK_ERROR( "flycaptureCreateContext()", error );

		error = ::flycaptureInitialize( context[i], 0 );
		CHECK_ERROR( "flycaptureInitialize()", error );

		// Enable image timestamping
		error = ::flycaptureGetImageTimestamping( context[i], &bOn );
		CHECK_ERROR( "flycaptureGetImageTimestamping()", error );

		if( !bOn )
		{
			error = ::flycaptureSetImageTimestamping( context[i], true );
			CHECK_ERROR( "flycaptureSetImageTimestamping()", error );
		}

	// Query and report on the camera's ability to handle custom image modes.
	
		error = ::flycaptureQueryCustomImage(
			context[i],
			MODE,
			&bAvailable,
			&uiMaxImageSizeCols,
			&uiMaxImageSizeRows,
			&uiUnitSizeHorz,
			&uiUnitSizeVert,
			&uiPixelFormats );
		CHECK_ERROR( "flycaptureQueryCustomImage()", error );
	}
}

void CapturaCamara::CerrarCamara()
{
	/*// stop the camera and destroy the context.
	::flycaptureStop( context );
	::flycaptureDestroyContext( context );*/
}

//=============================================================================
// Main Program
//=============================================================================
FlyCaptureImage *CapturaCamara::Capturar(int numImage, char *dir)
{
	// Grab a series of images, computing the time difference
	// between consecutive images.
	FlyCaptureImage *image;

	// Start camera using custom image size mode.
	error = ::flycaptureStartCustomImage(
		context[numImage], 
		MODE, 
		START_COL, 
		START_ROW, 
		COLS, 
		ROWS, 
		SPEED, 
		PIXEL_FORMAT );
	CHECK_ERROR( "flycaptureStartCustomImage()", error );

	image = (FlyCaptureImage*)malloc(sizeof(FlyCaptureImage));
	
	//image = { 0 };
	
	// Grab an image
	error = ::flycaptureGrabImage2( context[numImage], image );
	CHECK_ERROR( "flycaptureGrabImage2()", error );

	// Save the last image to disk
	saveFinalImage(context[numImage], image, numImage, dir);

	::flycaptureStop( context[numImage] );
	//::flycaptureDestroyContext( context );

	return image;
}

FlyCaptureError CapturaCamara::saveFinalImage(FlyCaptureContext context, FlyCaptureImage *pImage, int numImage, char *dir)
{
	int i, j;
	unsigned char* invertedData;
	FlyCaptureError error = FLYCAPTURE_OK;
	int wBitsPerPixel, lHeight, lWidth;

	wBitsPerPixel = 24;
	lHeight = 480;
	lWidth = 640;

	//Create Bitmap information header
	 BITMAPINFOHEADER bmpInfoHeader = {0};
    // Set the size
    bmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
    // Bit count
    bmpInfoHeader.biBitCount = wBitsPerPixel;
    // Use all colors
    bmpInfoHeader.biClrImportant = 0;
    // Use as many colors according to bits per pixel
    bmpInfoHeader.biClrUsed = 0;
    // Store as un Compressed
    bmpInfoHeader.biCompression = BI_RGB;
    // Set the height in pixels
    bmpInfoHeader.biHeight = lHeight;
    // Width of the Image in pixels
    bmpInfoHeader.biWidth = lWidth;
    // Default number of planes
    bmpInfoHeader.biPlanes = 1;
    // Calculate the image size in bytes
    bmpInfoHeader.biSizeImage = lWidth* lHeight * (wBitsPerPixel/8);

	/*BITMAPINFOHEADER BMIH;

	BMIH.biSize = sizeof(BITMAPINFOHEADER);
    BMIH.biBitCount = 24;
	
	BMIH.biCompression = 0;//BI_RGB;
    BMIH.biHeight = pImage->iRows;
	BMIH.biWidth = pImage->iCols;
	BMIH.biPlanes = 0;//1;
    BMIH.biSizeImage = ((((BMIH.biWidth * BMIH.biBitCount) + 31) & ~31) >> 3) * BMIH.biHeight;*/

/**********************/

	//Create a bitmap file header
	BITMAPFILEHEADER bfh = {0}; // This value should be values of BM letters i.e 0�4D42 // 0�4D = M 0�42 = B storing in reverse order to match with endian
    bfh.bfType=0x4D42; // /*or*/ bfh.bfType = �B�+(�M� << 8); // <<8 used to shift �M� to end
    // Offset to the RGBQUAD
    bfh.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
    // Total size of image including size of headers
    bfh.bfSize = bfh.bfOffBits + bmpInfoHeader.biSizeImage;


	/*BITMAPFILEHEADER bmfh;
	bmfh.bfType = 'B'+('M'<<8);
	bmfh.bfOffBits = nBitsOffset;
	bmfh.bfSize = lFileSize;

    int nBitsOffset = sizeof(BITMAPFILEHEADER) + BMIH.biSize; 
    LONG lImageSize = BMIH.biSizeImage;
    LONG lFileSize = nBitsOffset + lImageSize;

    bmfh.bfReserved1 = bmfh.bfReserved2 = 0;
    //Write the bitmap file header
    UINT nWrittenFileHeaderSize = (UINT)fwrite(&bmfh, 1, sizeof(BITMAPFILEHEADER), pFile);
    //And then the bitmap info header
    UINT nWrittenInfoHeaderSize = (UINT)fwrite(&BMIH, 1, sizeof(BITMAPINFOHEADER), pFile);*/

	/****************************/
	
	// Create the file in disk to write
	char s[200];
	sprintf_s(s, 200, "%scamara%d.bmp", dir, numImage);
    HANDLE hFile = CreateFile( s,GENERIC_WRITE, 0,NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL);

    if( !hFile ) // return if error opening file
        return FLYCAPTURE_ERROR_UNKNOWN;

    DWORD dwWritten = 0;
    // Write the File header
    WriteFile( hFile, &bfh, sizeof(bfh), &dwWritten , NULL );
    // Write the bitmap info header
    WriteFile( hFile, &bmpInfoHeader, sizeof(bmpInfoHeader), &dwWritten, NULL );
    
	// Write the RGB Data
	invertedData = (unsigned char *)malloc(3*lWidth*lHeight*sizeof(unsigned char*));
	for(i=0; i<lHeight; ++i)
	{
		for(j=0; j<lWidth; ++j)
		{
			invertedData[3*lWidth*i + 3*j]   = pImage->pData[3*lWidth*(lHeight-i-1) + 3*j+2];
			invertedData[3*lWidth*i + 3*j+1] = pImage->pData[3*lWidth*(lHeight-i-1) + 3*j+1];
			invertedData[3*lWidth*i + 3*j+2] = pImage->pData[3*lWidth*(lHeight-i-1) + 3*j];
		}
	}

    WriteFile( hFile, invertedData, bmpInfoHeader.biSizeImage, &dwWritten, NULL );
    // Close the file handle
    CloseHandle( hFile );


	/*char *s;
	s = (char *)malloc(15*sizeof(char*));
	sprintf(s, "%scamara%d.bmp", dir, numImage);
	FILE *pFile = fopen(s, "wb");
    if(pFile == NULL)
    {
		return FLYCAPTURE_ERROR_UNKNOWN;
    }
   
    //Finally, write the image data itself 
    //-- the data represents our drawing

	invertedData = (unsigned char *)malloc(3*BMIH.biWidth*BMIH.biHeight*sizeof(unsigned char*));
	for(i=0; i<BMIH.biHeight; ++i)
	{
		for(j=0; j<BMIH.biWidth; ++j)
		{
			invertedData[3*BMIH.biWidth*i + 3*j] = pImage->pData[3*BMIH.biWidth*(BMIH.biHeight-i-1) + 3*j+2];
			invertedData[3*BMIH.biWidth*i + 3*j+1] = pImage->pData[3*BMIH.biWidth*(BMIH.biHeight-i-1) + 3*j+1];
			invertedData[3*BMIH.biWidth*i + 3*j+2] = pImage->pData[3*BMIH.biWidth*(BMIH.biHeight-i-1) + 3*j];
		}
	}

	UINT nWrittenDIBDataSize = (UINT)fwrite(invertedData, 1, lImageSize, pFile);

    fclose(pFile);*/
   
	return error;
}