#pragma once

#ifndef LALAL_H
#define LALAL_H
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <errno.h> 
#include <ctime> 
#include "MeshGenerator.h"
#include "CapturaCamara.h"
#include "Stepper.h"
#include "LaserSick.h"
#include "Triangle.h"

#define PI 3.1415926535897932384626433832795
#define NUM_PANS 361
#define DEGREES_PAN 0.5
#define DY_MIRROR 26.5 //DESPLAZAMIENTO HACIA ADELANTE
#define DZ_MIRROR 29.45 //DESPLAZAMIENTO HACIA ARRIBA

#define	IMG_ROWS 480
#define IMG_COLS 640
#define BLOCK_SIZE 32
#define BLOCK_ROWS IMG_ROWS/BLOCK_SIZE
#define BLOCK_COLS IMG_COLS/BLOCK_SIZE

#define MIN_SEL_CAL 89
#define MAX_SEL_CAL 270

#define TILT_IMG1	48
#define TILT_IMG2	88
#define TILT_IMG3  128

#define LADO_A 151.5
#define LADO_B 151.0
#define K sqrt((double)(LADO_A*LADO_A + LADO_B*LADO_B))
#define TAM_CELDA_CALIB	15

//por teorema del coseno se saca el �ngulo menor del rect�ngulo
#define ANGULO_A acos( (K*K/2.0 - LADO_A*LADO_A) / (K*K/2.0))	//1.564152	//180-2*atan(LL/LC)
#define ANGULO_B (PI - ANGULO_A)								//1.577441	//2*atan(LL/LC)

#define ANGULO_ROT 0.001
#define NUM_TESTS	1000

//		  ^
//	151.0	151.5
//	<	     	>
//	151.5	151.0
//		  v



#define MAX_DIST_CALIBRATION 800//1150 //mm

#pragma once

class Input
{
private:
	//ATRIBUTOS
	static double **medicionesLaser;
	static double **angulosPhi;
	static double **angulosTheta;
	static Vertex **listadoVertices;
	static Triangle **listadoTriangulos;
	static int		totalVertices;
	static int		totalTriangulos;

	static bool			 numTrianglesLeido;
	static bool			 numVerticesLeido;
	static CapturaCamara camara;
	static LaserSick	 laser;
	static int tilt;
	static char*		 modelName;
	static char*		 modelDir;
	static FILE*		 fileMedicionesLaser;
	static BOOL			 texturizar;

	//METODOS
	static void				GetLaserScan(int tilt);
	static void				GetImage();
	static void				CrearMesh();
	static void				CorregirMedicion(int medicion, int pan, int tilt);
	static double			CorregirPan(int medicion, int tilt, int pan);
	static double			**MatrizInversa(double [3][3]);
	static void				CalibrationModelCenterCorrection(double centro[3], double v[4][3], const double alfa[4], double **calObj, int numPts);
	static void				FiltrarPuntos(double cluster1[3], double cluster2[3], double X[2][4*NUM_PANS], double Y[4*NUM_PANS], int numPuntos);
	static void				CargarImagenes();
	static FlyCaptureImage	*LoadBMP(char *filename);
	static void				LoadLaserData(char *f);
	static void				PointProjection(int maxDistance, double vertices[4][3], double alfa[4]);
	static double			ErrorCalibracion(double centro[3], double vertices[4][3]);
	static void				TraslateRectangle(double v[4][3], double centro[3], const double alfa[4], double **calObj,int totalPts );
	static void				RotateRectangle(double v[4][3], double theta, const double alfa[4]);
	static void				PuntosInteriores(double vertices[4][3], double vInternos[81][3], const double alfa[4]);

public:
	//ATRIBUTOS
	static Stepper			motor;
	static FlyCaptureImage	*imagen;
	static unsigned char imagenes[NUM_IMAGENES][BLOCK_ROWS*BLOCK_COLS][BLOCK_SIZE][BLOCK_SIZE][3];
	static unsigned char imagenesDown[NUM_IMAGENES][(BLOCK_ROWS-1)*BLOCK_COLS][BLOCK_SIZE][BLOCK_SIZE][3];
	static unsigned char imagenesRight[NUM_IMAGENES][BLOCK_ROWS*(BLOCK_COLS-1)][BLOCK_SIZE][BLOCK_SIZE][3];
	static unsigned char imagenesCross[NUM_IMAGENES][(BLOCK_ROWS-1)*(BLOCK_COLS-1)][BLOCK_SIZE][BLOCK_SIZE][3];

	//METODOS
	static bool		Scan(int puertoLaser, int puertoStepper);
	static void		Calibrate();
	static void		GenerarMesh();
	static void		CargarMesh();
	static void		SetFile(FILE*, FILE*);
	static bool		NumTrianglesLeido();
	static bool		NumVerticesLeido();
	static void		CerrarArchivos();
	static void		PolarToRectangular(double rho, double phi, double theta, double *x, double *y, double *z);
	static int		GetTilt();
	static void		SetModelName(char *modelName);
	static char*	GetModelName();
	static char*	GetModelDir();
	static void		CargarDatos(char *f);
	static void		Inicializar();
	static bool		PosicionarStepper(int puertoStepper);
	static bool		RewindStepper(int puertoStepper);

	static void		SetTexturizar(BOOL texturizar);
	static BOOL		GetTexturizar();

	//LISTAS DE ELEMENTOS
	static Vertex**	GetListadoVertices();
	static void		InicializarListaVertices(int largo);
	static void		InicializarListaTriangulos(int largo);
	static void		SaveTriangle(int indiceV1, int indiceV2, int indiceV3, Vertex *v1, Vertex *v2, Vertex *v3);
	static Vertex*	ReadVertex();
	static Triangle* ReadTriangle();
	static int		GetNumTriangulos();
	static int		GetNumVertices();
	static void		LiberarListas();
	
};

#endif
