#pragma once

//=============================================================================
// System Includes
//=============================================================================
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//=============================================================================
// Project Includes
//=============================================================================
#include "pgrflycapture.h"
#include "pgrflycaptureplus.h"

#pragma once

//=============================================================================
// Macro Defintions
//=============================================================================
#define IMAGES_TO_GRAB	1//200

#define MODE		0
#define START_COL	0
#define START_ROW	0

#define COLS		640
#define	ROWS		480
//#define COLS		1024
//#define	ROWS		768

#define	SPEED		100.0
#define PIXEL_FORMAT    FLYCAPTURE_RGB8
#define NUM_TILTS 129
#define NUM_IMAGENES 3

//
// Small macro to help handle error checking
//
#define CHECK_ERROR( function, error ) \
{ \
	if( error != FLYCAPTURE_OK ) \
	{ \
		printf( \
		"ERROR: %s returned \"%s\" (%d).\n", \
		function, \
		::flycaptureErrorToString( error ), \
		error ); \
		\
		::exit( 1 ); \
	} \
} \

class CapturaCamara
{
public:
	CapturaCamara(void);
	FlyCaptureImage* Capturar(int numImage, char *dir);
	void CerrarCamara();
	void IniciarCamara();

private:
	//metodos
	FlyCaptureError saveFinalImage(FlyCaptureContext context, FlyCaptureImage *pImage, int numImage, char *dir);

	//atributos
	FlyCaptureError   error;
	FlyCaptureContext context[NUM_TILTS];

	bool bOn;

	unsigned int uiCurTime;
	unsigned int uiLastTime;
	unsigned int uiTotalTime;
	unsigned int uiSeconds;
	unsigned int uiCount;
	unsigned int uiOffset;
	double dGrabTime;

	bool bAvailable;
	unsigned int uiMaxImageSizeCols;
	unsigned int uiMaxImageSizeRows;
	unsigned int uiUnitSizeHorz;
	unsigned int uiUnitSizeVert;
	unsigned int uiPixelFormats;
};