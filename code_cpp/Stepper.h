#pragma once

#include "PuertoSerialStepper.h"
#include <math.h>

#define NUM_TILTS 129
#define DEGREES_TILT 0.3515625
#define ANGULO_INICIO_TILT 22.5
#define MULT_STEPS 500
#define MAX_READTRY 5

class Stepper
{
public:
	Stepper(void);
	void Move();
	bool EnviarComando(unsigned char command);
	bool IniciarComunicacion(unsigned int puerto);
	void TerminarComunicacion();
	bool Configurar();
	void GoToStart();
	void HalfBack();
	void WaitMovement();

private:
	int Posicion();
	PuertoSerialStepper pSerial;
	bool RelativeMove(int steps);
};
