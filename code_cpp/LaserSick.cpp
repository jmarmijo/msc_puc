#include "StdAfx.h"
#include "LaserSick.h"
#include <time.h>
#include <winspool.h>
#include <string.h>

FILE *tiempos;
clock_t comienzo;

LaserSick::LaserSick()
{
}

bool LaserSick::Inicializar(unsigned int puerto)
{
	/*unsigned int i;
	DWORD cbNeeded = 0,cReturned = 0;
	LPSTR str;

	EnumPorts(0,1,0,0,&cbNeeded,&cReturned); // Detection of the necessary memory amount

	if (cbNeeded)
	{
		unsigned char* info = new unsigned char[cbNeeded];
		memset(info,0,cbNeeded);
		if(EnumPorts(0,1,info,cbNeeded,&cbNeeded,&cReturned ) ) // Reading the information about ports
		{
			PORT_INFO_1* pi = (PORT_INFO_1*)info; // filling the structure

			for(i=0; i<cReturned; ++i)
			{
				str = pi[i].pName; // Port name
				if (strncmp(str, "COM", 3)==0)
					i=i;
			}
			delete []info;
		}
	}*/

	char *port;
	port = new char[6];

	sprintf_s(port, 6, "COM%d", puerto);

	if (pSerial.abrir(port, 9600))
	{
		mensaje = Mensaje(HIGH_SPEED);
		pSerial.escribir(mensaje, TAM_HIGH_SPEED);
		pSerial.cerrar();

		if (pSerial.abrir(port, 19200))
			return true;
		else
			return false;
	}
	else
		return false;
}

void LaserSick::Finalizar()
{
	pSerial.cerrar();
}

void LaserSick::PedirDatos()
{
	mensaje = Mensaje(REQ_MEDICIONES);
	pSerial.escribir(mensaje, TAM_REQ_MEDICIONES);

	while(!CapturarDatos());
}

bool LaserSick::CapturarDatos()
{
	int i;
	bool retorno;
	unsigned short CRCreal;
	unsigned short CRCcalculated;
	int datalen, lenBytes;
//	unsigned char* datos;

	respuesta = new unsigned char[MAXNDATA];

	CRCreal = 0;
	
	comienzo = clock();
	do
	{
		respuesta[0] = pSerial.leer();
	} while (respuesta[0] != 0x02);
	
	for(i=1; i<7; ++i)
		respuesta[i] = pSerial.leer();

	datalen = respuesta[5] | ((respuesta[6] & 0x1f) << 8);
	datalen = datalen * 2; /*each reading is 2 bytes*/

	// Check that we have a valid ADR byte, valid data length and valid CMD byte
	if ((datalen > MAXNDATA) || (respuesta[1] != 0x80) || (respuesta[4] != 0xb0))
	{
		retorno = false;
	}
	else
	{
		for(i=0; i<datalen; ++i)
			respuesta[i+7] = pSerial.leer();

		respuesta[datalen + 7] = pSerial.leer(); // Status byte
		respuesta[datalen + 8] = pSerial.leer(); // should be CRC low byte*/
		respuesta[datalen + 9] = pSerial.leer(); // should be CRC high byte*/
		CRCreal = respuesta[datalen+8] | (respuesta[datalen+9] << 8);

		lenBytes = datalen+8;
		CRCcalculated = LMSCRC(respuesta, lenBytes);

		if (CRCcalculated != CRCreal)
			retorno = false;

		retorno = true;
	}

	if (retorno == false)
		delete [] respuesta;

	return retorno;
}

bool LaserSick::RespuestaConfigurar()
{
	int i;
	bool retorno;
	unsigned short CRCreal;
	unsigned short CRCcalculated;
	int datalen, lenBytes;

	respuesta = new unsigned char[MAXNDATA];
	CRCreal = 0;
	do
	{
		respuesta[0] = pSerial.leer();
	} while (respuesta[0] != 0x06);

	for(i=0; i<9; ++i)
		respuesta[i] = pSerial.leer();

	datalen = respuesta[2] | ((respuesta[3] & 0x1f) << 8);

	// Check that we have a valid ADR byte, valid data length and valid CMD byte
	if ((datalen > MAXNDATA) || (respuesta[0] != 0x02) || (respuesta[1] != 0x80))
	{
		retorno = false;
	}
	else
	{
		CRCreal = respuesta[7] | (respuesta[8] << 8);

		lenBytes = datalen + 4;
		CRCcalculated = LMSCRC(respuesta, lenBytes);

		if (CRCcalculated != CRCreal)
			retorno = false;

		retorno = true;
	}

	if (retorno == false)
		delete [] respuesta;

	return retorno;
}

bool LaserSick::RespuestaChangeBaudRate()
{
	int i;
	bool retorno;
	unsigned short CRCreal;
	unsigned short CRCcalculated;
	int datalen, lenBytes;

	respuesta = new unsigned char[MAXNDATA];
	CRCreal = 0;
	do
	{
		respuesta[0] = pSerial.leer();
	} while (respuesta[0] != 0x06);

	for(i=0; i<9; ++i)
		respuesta[i] = pSerial.leer();

	datalen = respuesta[2] | ((respuesta[3] & 0x1f) << 8);

	// Check that we have a valid ADR byte, valid data length and valid CMD byte
	if ((datalen > MAXNDATA) || (respuesta[0] != 0x02) || (respuesta[1] != 0x80))
	{
		retorno = false;
	}
	else
	{
		CRCreal = respuesta[7] | (respuesta[8] << 8);

		lenBytes = datalen + 4;
		CRCcalculated = LMSCRC(respuesta, lenBytes);

		if (CRCcalculated != CRCreal)
			retorno = false;

		retorno = true;
	}

	if (retorno == false)
		delete [] respuesta;

	return retorno;
}

bool LaserSick::RespuestaMM()
{
	int i=0;
	bool retorno;
	unsigned short CRCreal;
	unsigned short CRCcalculated;
	int datalen, lenBytes;

	respuesta = new unsigned char[MAXNDATA];
	CRCreal = 0;

	do
	{
		respuesta[0] = pSerial.leer();
	} while (respuesta[0] != 0x06);

	for(i=0; i<41; ++i)
		respuesta[i] = pSerial.leer();

 	datalen = respuesta[2] | ((respuesta[3] & 0x1f) << 8);

	// Check that we have a valid ADR byte, valid data length and valid CMD byte
	if ((datalen > MAXNDATA) || (respuesta[0] != 0x02) || (respuesta[1] != 0x80))
	{
		retorno = false;
	}
	else
	{
		CRCreal = respuesta[7] | (respuesta[8] << 8);

		lenBytes = datalen + 4;
		CRCcalculated = LMSCRC(respuesta, lenBytes);

		if (CRCcalculated != CRCreal)
			retorno = false;

		retorno = true;
	}

	if (retorno == false)
		delete [] respuesta;

	return retorno;
}

int LaserSick::ObtenerMedicionAngulo(int angulo)
{
	int val;
	val=( (respuesta[7+ 2*angulo +1] & 0x1f) <<8 | respuesta[7+ 2*angulo]);
	return val;
}

unsigned short LaserSick::LMSCRC(unsigned char* theBytes, int lenBytes)
{
	unsigned char xyz1 = 0;
	unsigned char xyz2 = 0;
	unsigned short uCrc16 = 0;
	int i;

	for (i = 0; i < lenBytes; i++)
	{
		xyz2 = xyz1;
		xyz1 = theBytes[i];

		if (uCrc16 & 0x8000)
		{
			uCrc16 = (uCrc16 & 0x7fff) << 1;
			uCrc16 = uCrc16 ^ 0x8005;
		}
		else
		{
			uCrc16 = uCrc16 << 1;
		}
		uCrc16 = uCrc16 ^ (xyz1 | (xyz2 << 8));
	}
	return uCrc16;
}

unsigned char* LaserSick::Mensaje(int tipoMensaje)
{
	int i;
	unsigned char *mensaje;
	unsigned short CRCcalculated;

	switch(tipoMensaje)
	{
	case RESET:
		mensaje = new unsigned char [TAM_RESET];
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x01;
		mensaje[3] = 0x00;
		mensaje[4] = 0x10;
		mensaje[5] = 0x34;
		mensaje[6] = 0x12;
		break;
	case REQ_STOP:
		mensaje = new unsigned char [TAM_REQ_STOP];
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x02;
		mensaje[3] = 0x00;
		mensaje[4] = 0x20;
		mensaje[5] = 0x25;
		mensaje[6] = 0x35;
		mensaje[7] = 0x08;
		break;
	case REQ_START:
		mensaje = new unsigned char [TAM_REQ_START];
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x02;
		mensaje[3] = 0x00;
		mensaje[4] = 0x20;
		mensaje[5] = 0x24;
		mensaje[6] = 0x34;
		mensaje[7] = 0x08;
		break;
	case REQ_MEDICIONES:
		mensaje = new unsigned char [TAM_REQ_START];
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x02;
		mensaje[3] = 0x00;
		mensaje[4] = 0x30;
		mensaje[5] = 0x01;
		mensaje[6] = 0x31;
		mensaje[7] = 0x18;
		break;
	case CONFIGURAR:
		mensaje = new unsigned char [TAM_CONFIGURAR];//16
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x0A;
		mensaje[3] = 0x00;
		mensaje[4] = 0x20;
		mensaje[5] = 0x00;
		mensaje[6] = 0x53;
		mensaje[7] = 0x49;
		mensaje[8] = 0x43;
		mensaje[9] = 0x4B;
		mensaje[10] = 0x5F;
		mensaje[11] = 0x4C;
		mensaje[12] = 0x4D;
		mensaje[13] = 0x53;
		mensaje[14] = 0xBE;
		mensaje[15] = 0xC5;
		break;
	case MM:
		mensaje = new unsigned char [TAM_MM];
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x21;
		mensaje[3] = 0x00;
		mensaje[4] = 0x77;
		for(i=5; i<11; ++i) mensaje[i] = 0x00;
		mensaje[11] = 0x01;
		mensaje[12] = 0x00;
        mensaje[13] = 0x00;
		mensaje[14] = 0x02;
		mensaje[15] = 0x02;
		for(i=16; i<37; ++i) mensaje[i] = 0x00;
		mensaje[37] = 0xFC;
		mensaje[38] = 0x7E;
		break;
	case HIGH_SPEED:
		mensaje = new unsigned char [TAM_HIGH_SPEED];
		mensaje[0] = 0x02;
		mensaje[1] = 0x00;
		mensaje[2] = 0x02;
		mensaje[3] = 0x00;
		mensaje[4] = 0x20;

		//mensaje[5] = 0x40;	//38.400 Bd
		mensaje[5] = 0x41;	//19.200 Bd
		//mensaje[5] = 0x42;	//9.600 Bd
		//mensaje[5] = 0x48;	//500.000 Bd

		CRCcalculated = LMSCRC(mensaje, TAM_HIGH_SPEED-2);

		mensaje[6] = CRCcalculated & 0x00FF;
        mensaje[7] = (CRCcalculated >> 8) & 0x00FF;
		break;
	}

	return mensaje;
}

/*void LaserSick::ProcesarDatos(int posicion, int **matrizDeDatos)
{
	unsigned int valor;
	int i, cont;
	//int dazzling, warning, secure;

	for (i = 7, cont = 0; i < (totalMediciones - 3); i += 2)
	{
		valor = ((respuesta[i+1] << 8) | (respuesta[i])) & 0x1FFF;

		//dazzling = (buffer[i+1] & 0x20) >> 5;
		//warning = (buffer[i+1] & 0x40) >> 6;
		//secure = (buffer[i+1] & 0x80) >> 7;

		matrizDeDatos[posicion][cont++] = valor;
	}

	if(totalMediciones > 0)
		anguloMedido[posicion] = true;
}*/