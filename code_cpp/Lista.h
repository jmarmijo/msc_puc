#pragma once

#include ".\celda.h"

class Lista
{
private:
	Celda *first, *last;
public:
	Lista(void);
	void AgregarElemento(Celda *);
	Celda *ObtenerElemento();
	void RemoverElemento();
	bool Empty();
	~Lista(void);
};
