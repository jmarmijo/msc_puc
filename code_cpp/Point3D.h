#pragma once

class Point3D
{
private:
	double x;
	double y;
	double z;
public:
	Point3D(void);
	Point3D(double x, double y, double z);
	void SetValues(double x, double y, double z);
	void GetValues(double *x, double *y, double *z);
	double GetX();
	double GetY();
	double GetZ();

	~Point3D(void);
};
