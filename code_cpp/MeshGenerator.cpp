#include "StdAfx.h"
#include ".\meshgenerator.h"

MeshGenerator::MeshGenerator()
{
}

void MeshGenerator::GenerarMesh(double **mediciones, double **angulosPhi, double **angulosTheta)
{
	int i, j;
	int v1, v2, v3, v4;
	int turno;

	double rho, theta, phi;
	double x, y, z;
	double a1, a2, a3, a4, d1, d2;
	double e1, e2;
	Vertex **buf;
	double rhos[2][NUM_PANS];
	double r1, r2, r3;
	Vertex vMedio[6];
	
	buf = (Vertex**)malloc(2*sizeof(Vertex));
	buf[0] = (Vertex*)malloc(NUM_PANS*sizeof(Vertex));
	buf[1] = (Vertex*)malloc(NUM_PANS*sizeof(Vertex));

	//inicializa las lista de vertices y triangulos
	Input::InicializarListaVertices(NUM_PANS*NUM_TILTS);
	Input::InicializarListaTriangulos(2*(NUM_PANS-1)*(NUM_TILTS-1));

	turno = 0;

	for(i=0; i<NUM_TILTS; ++i)
	{
		for(j=0; j<NUM_PANS; ++j)
		{
			rho = mediciones[i][j];
			theta = angulosTheta[i][j];
			phi = angulosPhi[i][j];

			rhos[turno][j] = rho;
			Input::PolarToRectangular(rho, phi, theta, &x, &y, &z);
			buf[turno][j].Set(x,y,z);
		}

		if (i>0)
		{
			for(j=0; j<NUM_PANS-1; ++j)
			{
				//ejemplo del orden de los vertices y de las diagonales
				//v1 a1 v2
				//a4 []	a2	d1 /	d2 \
				//v4 a3 v3

				//calcula el largo de cada arista y de las diagonales
				a1 = Distancia(buf[1-turno][j]  ,	buf[1-turno][j+1]);
				a2 = Distancia(buf[1-turno][j+1],	buf[turno][j+1]  );
				a3 = Distancia(buf[turno][j+1]  ,	buf[turno][j]    );
				a4 = Distancia(buf[turno][j]    ,	buf[1-turno][j]  );
				d1 = Distancia(buf[1-turno][j+1],	buf[turno][j]    );
				d2 = Distancia(buf[1-turno][j]  ,	buf[turno][j+1]  );

				//calcula la diferencia entre las areas de los triangulos
				e1 = .25*(sqrt((a1+a2+d2)*(a2+d2-a1)*(d2+a1-a2)*(a1+a2-d2)) - sqrt((a3+a4+d2)*(a4+d2-a3)*(d2+a3-a4)*(a3+a4-d2)));
				e2 = .25*(sqrt((a2+a3+d1)*(a3+d1-a2)*(d1+a2-a3)*(a2+a3-d1)) - sqrt((a4+a1+d1)*(a1+d1-a4)*(d1+a4-a1)*(a4+a1-d1)));

				//crea los v�rtices del cuadrado <=> de los triangulos.
				v1 = NUM_PANS*(i-1) + j;
				v2 = NUM_PANS*(i-1) + j + 1;
				v3 = NUM_PANS*i + j + 1;
				v4 = NUM_PANS*i + j;

				//vertice medio de a1
				vMedio[0].Set((buf[1-turno][j].GetX() + buf[1-turno][j+1].GetX())/2.0,
							  (buf[1-turno][j].GetY() + buf[1-turno][j+1].GetY())/2.0,
							  (buf[1-turno][j].GetZ() + buf[1-turno][j+1].GetZ())/2.0);

				//vertice medio de a2
				vMedio[1].Set((buf[1-turno][j+1].GetX() + buf[turno][j+1].GetX())/2.0,
							  (buf[1-turno][j+1].GetY() + buf[turno][j+1].GetY())/2.0,
							  (buf[1-turno][j+1].GetZ() + buf[turno][j+1].GetZ())/2.0);

				//vertice medio de a3
				vMedio[2].Set((buf[turno][j+1].GetX() + buf[turno][j].GetX())/2.0,
							  (buf[turno][j+1].GetY() + buf[turno][j].GetY())/2.0,
							  (buf[turno][j+1].GetZ() + buf[turno][j].GetZ())/2.0);

				//vertice medio de a4
				vMedio[3].Set((buf[turno][j].GetX() + buf[1-turno][j].GetX())/2.0,
							  (buf[turno][j].GetY() + buf[1-turno][j].GetY())/2.0,
							  (buf[turno][j].GetZ() + buf[1-turno][j].GetZ())/2.0);

				if (e1 <= e2)
				{
					//vertice medio de d2
					vMedio[5].Set((buf[1-turno][j].GetX() + buf[turno][j+1].GetX())/2.0,
								  (buf[1-turno][j].GetY() + buf[turno][j+1].GetY())/2.0,
								  (buf[1-turno][j].GetZ() + buf[turno][j+1].GetZ())/2.0);

					//calcula la distancia desde el origen hasta el punto medio de las aristas
					r1 = sqrt(pow(vMedio[0].GetX(),2) + pow(vMedio[0].GetY(),2) + pow(vMedio[0].GetZ(),2));
					r2 = sqrt(pow(vMedio[1].GetX(),2) + pow(vMedio[1].GetY(),2) + pow(vMedio[1].GetZ(),2));
					r3 = sqrt(pow(vMedio[5].GetX(),2) + pow(vMedio[5].GetY(),2) + pow(vMedio[5].GetZ(),2));

					if (a1 <= r1*BASE_DELTA/LARGO_DELTA && a2<=r2*BASE_DELTA/LARGO_DELTA && d2 <= r3*BASE_DELTA/LARGO_DELTA)
						if ((buf[1-turno][j].GetX()<=  MAX_DIST && buf[1-turno][j+1].GetX()<=  MAX_DIST && buf[turno][j+1].GetX()<=  MAX_DIST) &&
							(buf[1-turno][j].GetX()>= -MAX_DIST && buf[1-turno][j+1].GetX()>= -MAX_DIST && buf[turno][j+1].GetX()>= -MAX_DIST) &&
							(buf[1-turno][j].GetZ()<=  MAX_DIST && buf[1-turno][j+1].GetZ()<=  MAX_DIST && buf[turno][j+1].GetZ()<=  MAX_DIST) &&
							(buf[1-turno][j].GetZ()>= -MAX_DIST && buf[1-turno][j+1].GetZ()>= -MAX_DIST && buf[turno][j+1].GetZ()>= -MAX_DIST) &&
							(buf[1-turno][j].GetY()<=  MAX_DIST && buf[1-turno][j+1].GetY()<=  MAX_DIST && buf[turno][j+1].GetY()<=  MAX_DIST) &&
							(buf[1-turno][j].GetY()>=  0		&& buf[1-turno][j+1].GetY()>=  0		&& buf[turno][j+1].GetY()>=  0))
								Input::SaveTriangle(v1, v2, v3, &buf[1-turno][j], &buf[1-turno][j+1], &buf[turno][j+1]);

					r1 = sqrt(pow(vMedio[2].GetX(),2) + pow(vMedio[2].GetY(),2) + pow(vMedio[2].GetZ(),2));
					r2 = sqrt(pow(vMedio[3].GetX(),2) + pow(vMedio[3].GetY(),2) + pow(vMedio[3].GetZ(),2));
					r3 = sqrt(pow(vMedio[5].GetX(),2) + pow(vMedio[5].GetY(),2) + pow(vMedio[5].GetZ(),2));

					if (a3 <= r1*BASE_DELTA/LARGO_DELTA && a4<=r3*BASE_DELTA/LARGO_DELTA && d2 <= r3*BASE_DELTA/LARGO_DELTA)
						if ((buf[1-turno][j].GetX()<=  MAX_DIST && buf[turno][j+1].GetX()<=  MAX_DIST && buf[turno][j].GetX()<=  MAX_DIST) &&
							(buf[1-turno][j].GetX()>= -MAX_DIST && buf[turno][j+1].GetX()>= -MAX_DIST && buf[turno][j].GetX()>= -MAX_DIST) &&
							(buf[1-turno][j].GetZ()<=  MAX_DIST && buf[turno][j+1].GetZ()<=  MAX_DIST && buf[turno][j].GetZ()<=  MAX_DIST) &&
							(buf[1-turno][j].GetZ()>= -MAX_DIST && buf[turno][j+1].GetZ()>= -MAX_DIST && buf[turno][j].GetZ()>= -MAX_DIST) &&
							(buf[1-turno][j].GetY()<=  MAX_DIST && buf[turno][j+1].GetY()<=  MAX_DIST && buf[turno][j].GetY()<=  MAX_DIST) &&
							(buf[1-turno][j].GetY()>=  0		&& buf[turno][j+1].GetY()>=  0		  && buf[turno][j].GetY()>=  0))
								Input::SaveTriangle(v1, v3, v4, &buf[1-turno][j], &buf[turno][j+1], &buf[turno][j]);
				}
				else
				{
					//vertice medio de d1
					vMedio[4].Set((buf[1-turno][j+1].GetX() + buf[turno][j].GetX())/2.0,
								  (buf[1-turno][j+1].GetY() + buf[turno][j].GetY())/2.0,
								  (buf[1-turno][j+1].GetZ() + buf[turno][j].GetZ())/2.0);

					r1 = sqrt(pow(vMedio[1].GetX(),2) + pow(vMedio[1].GetY(),2) + pow(vMedio[1].GetZ(),2));
					r2 = sqrt(pow(vMedio[2].GetX(),2) + pow(vMedio[2].GetY(),2) + pow(vMedio[2].GetZ(),2));
					r3 = sqrt(pow(vMedio[4].GetX(),2) + pow(vMedio[4].GetY(),2) + pow(vMedio[4].GetZ(),2));

					if (a2 <= r1*BASE_DELTA/LARGO_DELTA && a3<=r2*BASE_DELTA/LARGO_DELTA && d1 <= r3*BASE_DELTA/LARGO_DELTA)
						if ((buf[1-turno][j+1].GetX()<=  MAX_DIST && buf[turno][j+1].GetX()<=  MAX_DIST && buf[turno][j].GetX()<=  MAX_DIST) &&
							(buf[1-turno][j+1].GetX()>= -MAX_DIST && buf[turno][j+1].GetX()>= -MAX_DIST && buf[turno][j].GetX()>= -MAX_DIST) &&
							(buf[1-turno][j+1].GetZ()<=  MAX_DIST && buf[turno][j+1].GetZ()<=  MAX_DIST && buf[turno][j].GetZ()<=  MAX_DIST) &&
							(buf[1-turno][j+1].GetZ()>= -MAX_DIST && buf[turno][j+1].GetZ()>= -MAX_DIST && buf[turno][j].GetZ()>= -MAX_DIST) &&
							(buf[1-turno][j+1].GetY()<=  MAX_DIST && buf[turno][j+1].GetY()<=  MAX_DIST && buf[turno][j].GetY()<=  MAX_DIST) &&
							(buf[1-turno][j+1].GetY()>=  0		  && buf[turno][j+1].GetY()>=  0		&& buf[turno][j].GetY()>=  0))
								Input::SaveTriangle(v2, v3, v4, &buf[1-turno][j+1], &buf[turno][j+1], &buf[turno][j]);
					
					r1 = sqrt(pow(vMedio[2].GetX(),2) + pow(vMedio[2].GetY(),2) + pow(vMedio[2].GetZ(),2));
					r2 = sqrt(pow(vMedio[3].GetX(),2) + pow(vMedio[3].GetY(),2) + pow(vMedio[3].GetZ(),2));
					r3 = sqrt(pow(vMedio[4].GetX(),2) + pow(vMedio[4].GetY(),2) + pow(vMedio[4].GetZ(),2));

					if (a3 <= r1*BASE_DELTA/LARGO_DELTA && a4<=r2*BASE_DELTA/LARGO_DELTA && d1 <= r3*BASE_DELTA/LARGO_DELTA)
						if ((buf[1-turno][j].GetX()<=  MAX_DIST && buf[1-turno][j+1].GetX()<=  MAX_DIST && buf[turno][j].GetX()<=  MAX_DIST) &&
							(buf[1-turno][j].GetX()>= -MAX_DIST && buf[1-turno][j+1].GetX()>= -MAX_DIST && buf[turno][j].GetX()>= -MAX_DIST) &&
							(buf[1-turno][j].GetZ()<=  MAX_DIST && buf[1-turno][j+1].GetZ()<=  MAX_DIST && buf[turno][j].GetZ()<=  MAX_DIST) &&
							(buf[1-turno][j].GetZ()>= -MAX_DIST && buf[1-turno][j+1].GetZ()>= -MAX_DIST && buf[turno][j].GetZ()>= -MAX_DIST) &&
							(buf[1-turno][j].GetY()<=  MAX_DIST && buf[1-turno][j+1].GetY()<=  MAX_DIST && buf[turno][j].GetY()<=  MAX_DIST) &&
							(buf[1-turno][j].GetY()>=  0		&& buf[turno][j+1].GetY()  >=  0		&& buf[turno][j].GetY()>=  0))
								Input::SaveTriangle(v1, v2, v4, &buf[1-turno][j], &buf[1-turno][j+1], &buf[turno][j]);
				}
			}
		}

		turno = 1-turno;
	}

	//libera la memoria utilizada por la matriz de mediciones del l�ser
	for(i=0; i<NUM_TILTS; ++i)
	{
		free(mediciones[i]);
		free(angulosPhi[i]);
		free(angulosTheta[i]);
	}

	free(mediciones);
	free(angulosPhi);
	free(angulosTheta);
	free(buf[0]);
	free(buf[1]);
	free(buf);
}

double MeshGenerator::Distancia(Vertex v1, Vertex v2)
{
	double dx, dz, dy;

	dx = (v1.GetX() - v2.GetX()) * (v1.GetX() - v2.GetX());
	dy = (v1.GetY() - v2.GetY()) * (v1.GetY() - v2.GetY());
	dz = (v1.GetZ() - v2.GetZ()) * (v1.GetZ() - v2.GetZ());

	return sqrt(dx + dz);
}

double MeshGenerator::Min(double a, double b, double c)
{
	if (a<=b && a<=c)
		return a;
	else if (b<=a && b<=c)
		return b;
	else
		return c;
}