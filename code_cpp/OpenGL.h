#pragma once

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <gl/glut.h>
#include ".\celda.h"
#include ".\lista.h"
#include "Surface.h"

#pragma comment (lib,"opengl32.lib")
#pragma comment (lib,"glu32.lib")
#pragma comment (lib,"glut32.lib")
#pragma comment (lib,"glaux.lib")

#define AnchoVentana	640//981
#define AltoVentana		480//736
#define TamPuntos		10.0
#define POS_X			250
#define POS_Y			0
#define _PI				3.1415926535897932384626433832795

//void Light();

class OpenGL
{
private:
	//ATRIBUTOS
	GLdouble eyeX, eyeY, eyeZ;
	GLdouble upX, upY, upZ;
	GLdouble desplazamientoX, desplazamientoY, desplazamientoZ;
	double phiX, phiY, phiZ;
	double rho;
	int ventana;
	
	Lista *rot;	//rotaciones
	GLuint texName;	

	int maxPoligonosX;
	int maxPoligonosY;
	int maxPoligonosZ;

	//METODOS
	void CrearVentana();
	void Finalizar();

public:
	Surface *s;
	//METODOS
	OpenGL();
	void SetSurface(Surface*);
	~OpenGL(void);
	void start();
	void Transformacion(int *u, int *v, float Xp, float Yp, float Zp);

	friend void Dibujar();
	friend void DibujarPuntos();
	friend void DibujarTriangulo(Vertex *p, Vertex *q, Vertex *r, int u[3], int v[3], int indiceImagen);
	friend void DibujarTriangulo(Vertex *p, Vertex *q, Vertex *r);
	friend GLvoid WindowKeyUp(unsigned char c, int x, int y);
	friend GLvoid WindowKeyDown(unsigned char c, int x, int y);
	friend void RotarX(double angulo);
	friend void RotarY(double angulo);
	friend void RotarZ(double angulo);
	friend void Zoom(double zoom);
	friend void KeyCallback(int c, int x,int y);
};
