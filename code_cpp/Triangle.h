#pragma once
#include "Vertex.h"
#include <float.h>

class Triangle
{
private:
	Vertex *v1;
	Vertex *v2;
	Vertex *v3;

	double distanciaCentroImagen[NUM_IMAGENES];
	void Inicializar();
	
	int image;
	int **pixelsU;
	int **pixelsV;

public:
	Triangle();
	Triangle(Vertex*, Vertex*, Vertex*);
	Vertex* GetVertex(int vertex);
	void DistanciaCentroImagen(int indiceImagen, double distancia);
	//double *Normal();
	void SetImagePixels(int img, int u[NUM_IMAGENES][3], int v[NUM_IMAGENES][3]);
	void GetImagePixels(int *img, int u[3], int v[3]);
	void ResetTexture();
	~Triangle(void);

	bool visitado;
};
