#pragma once

class PuertoSerialStepper
{
private:
	HANDLE hPort;
	DCB PortDCB;
	DWORD lastError;
	unsigned char byteLeido;
	unsigned char *buffer;
	unsigned int tamBuffer;

public:
	PuertoSerialStepper();
	unsigned char* GetBuffer();
	bool abrir(LPCTSTR puerto);
	bool cerrar();
	unsigned char leer();
	bool escribir(unsigned char * bufferIn, int largo);
	unsigned char ObtenerByteLeido();

};
