#include "StdAfx.h"
#include ".\Stepper.h"

Stepper::Stepper(void)
{
}

bool Stepper::IniciarComunicacion(unsigned int puerto)
{
	char *port;
	port = new char[6];

	sprintf_s(port, 6, "COM%d", puerto);
	
	if (pSerial.abrir(port))
	{
		if (Configurar())
			return true;
		else
			return false;
	}
	else
		return false;
}

void Stepper::TerminarComunicacion()
{
	pSerial.cerrar();
}

bool Stepper::EnviarComando(unsigned char command)
{
	int i;
	unsigned char *mensaje;
	unsigned char leido;

	mensaje = new unsigned char [1];
	mensaje[0] = command;

	i = 0;
	pSerial.escribir(mensaje, 1);
	do
	{
		leido = pSerial.leer();
	} while (leido != mensaje[0] && (++i)<MAX_READTRY);

	if (i >= MAX_READTRY)
		return false;
	else
		return true;
}

bool Stepper::Configurar()
{
	//Set acceleration = 100.000
	if(!EnviarComando(0x41)) return false; //A
	if(!EnviarComando(0x3D)) return false; //=
	if(!EnviarComando(0x31)) return false; //1
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	//EnviarComando(0x30); //0
	if(!EnviarComando(0x0D)) return false; //C.Ret

	//Set deseleration = 100.000
	if(!EnviarComando(0x44)) return false; //D
	if(!EnviarComando(0x3D)) return false; //=
	if(!EnviarComando(0x31)) return false; //1
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	if(!EnviarComando(0x30)) return false; //0
	//EnviarComando(0x30); //0
	if(!EnviarComando(0x0D)) return false; //C.Ret

	return true;
}

void Stepper::GoToStart()
{
	//RelativeMove(-(int)((NUM_TILTS-1)/2));
	//WaitMovement();
	RelativeMove(-(int)((NUM_TILTS-1)));
}

void Stepper::HalfBack()
{
	//RelativeMove(-(int)((NUM_TILTS-1)/2));
	//WaitMovement();
	RelativeMove(-(int)((NUM_TILTS-1)/2));
}

void Stepper::Move()
{
	//5120*100 --> 1 vuelta --> 360�
	//1280*100 = 90�
	//   100 = 0.0703125�
	//10*100 = 0.703125�
	// 3*100 = 0.2109375�

	RelativeMove(1);
}

bool Stepper::RelativeMove(int steps)
{
	int i, largo, stepsAux, valor, potencia;

	steps = (int)steps*MULT_STEPS;

	if (!EnviarComando(0x6D)) return false; //m
	if (!EnviarComando(0x72)) return false; //r
	if (!EnviarComando(0x20)) return false; //' '
	
	if (steps < 0)
		if (!EnviarComando(0x2d)) return false; //-
		
	stepsAux = abs(steps);
	largo = (int)(log10((double)stepsAux) + 1);

	for(i=0; i<largo; ++i)
	{
		potencia = (int)pow((double)10,(double)largo-i-1);
		valor = (int)(stepsAux/potencia);
		stepsAux -= valor*potencia;
		
		if (!EnviarComando((unsigned char)(valor + 48))) return false;
	}
	
	if (!EnviarComando(0x0D)) return false; //C.Ret

	if (!EnviarComando(0x68)) return false; //h
	if (!EnviarComando(0x0D)) return false; //C.Ret

	return true;
}

void Stepper::WaitMovement()
{
	int turno;
	int pos[2];

	turno = 0;
	pos[0] = Posicion();

	do
	{
		turno = 1-turno;
		pos[turno] = Posicion();
	} while (pos[turno] != pos[1-turno]);
}

int Stepper::Posicion()
{
	int i;
	char *leido;
	leido = new char[15];

	EnviarComando(0x50); //P
	EnviarComando(0x52); //R
	EnviarComando(0x20); //' '
	EnviarComando(0x50); //P
	EnviarComando(0x0D); //C.Ret

	i = 0;

	while( pSerial.leer() != 0x0A);

	do
	{
		leido[i++] = (char)pSerial.leer();
	}
	while(leido[i-1]!=0x0D && i<15);
	leido[i-2] = 0x00;

	return atoi(leido);
}