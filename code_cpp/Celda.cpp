#include "StdAfx.h"
#include ".\celda.h"

Celda::Celda(void)
{
	this->anguloGrados = new double[1];
	this->ejeCoordenado = new int[1];
	this->next = NULL;
}

/*Celda::Celda(double anguloGrados, int ejeCoordenado)
{
	try
	{
		this->anguloGrados = new double[1];
		this->ejeCoordenado = new int[1];

		this->anguloGrados[0] = anguloGrados;//*PI/180;
		this->ejeCoordenado[0] = ejeCoordenado;
		this->next = NULL;
	}
	catch(int i)
	{
		i = 0;
	}
}*/

Celda::~Celda(void)
{
	delete [] anguloGrados;
	delete [] ejeCoordenado;
}

float Celda::AnguloX()
{
	if (ejeCoordenado[0] == 1)
		return (float)anguloGrados[0];
	else
		return 0.0;
}

float Celda::AnguloY()
{
	if (ejeCoordenado[0] == 2)
		return (float)anguloGrados[0];
	else
		return 0.0;
}

float Celda::AnguloZ()
{
	if (ejeCoordenado[0] == 3)
		return (float)anguloGrados[0];
	else
		return 0.0;
}

void Celda::SetNext(Celda *next)
{
	this->next = next;
}

Celda* Celda::GetNext()
{
	return this->next;
}
