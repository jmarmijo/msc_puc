#include "StdAfx.h"
#include ".\trianglecell.h"

TriangleCell::TriangleCell(void)
{
	t = NULL;
	next = NULL;
	prev = NULL;
}

TriangleCell::~TriangleCell(void)
{
}

void TriangleCell::SetTriangle(Triangle *newT)
{
	t = newT;
}

void TriangleCell::SetNext(TriangleCell *nextTC)
{
	next = nextTC;
}

void TriangleCell::SetPrev(TriangleCell *prevTC)
{
	prev = prevTC;
}

TriangleCell* TriangleCell::GetNext()
{
	return next;
}

TriangleCell* TriangleCell::GetPrev()
{
	return prev;
}

Triangle* TriangleCell::GetTriangle()
{
	return t;
}
