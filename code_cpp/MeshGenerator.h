#pragma once

#include "Vertex.h"
#include <math.h>
#include "Input.h"

//#define NUM_DATOS_MEDICION 181
#define PI 3.1415926535897932384626433832795
#define BASE_DELTA 2
#define LARGO_DELTA 90

#define MAXERROR 2000000
#define MAX_DIST 3000

#define NUM_PANS 361
#define NUM_TILTS 129

class MeshGenerator
{
private:
	//METODOS
	double Distancia(Vertex v1, Vertex v2);
	double Min(double a, double b, double c);

public:
	MeshGenerator();
	void GenerarMesh(double **mediciones, double **angulosPhi, double **angulosTheta);
};