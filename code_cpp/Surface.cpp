#include "StdAfx.h"
#include ".\surface.h"

#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_NO_DEPRECATE 1



/* Triangle/triangle intersection test routine,
 * by Tomas Moller, 1997.
 * See article "A Fast Triangle-Triangle Intersection Test",
 * Journal of Graphics Tools, 2(2), 1997
 * updated: 2001-06-20 (added line of intersection)
 *
 * int tri_tri_intersect(float V0[3],float V1[3],float V2[3],
 *                       float U0[3],float U1[3],float U2[3])
 *
 * parameters: vertices of triangle 1: V0,V1,V2
 *             vertices of triangle 2: U0,U1,U2
 * result    : returns 1 if the triangles intersect, otherwise 0
 *
 * Here is a version withouts divisions (a little faster)
 * int NoDivTriTriIsect(float V0[3],float V1[3],float V2[3],
 *                      float U0[3],float U1[3],float U2[3]);
 * 
 * This version computes the line of intersection as well (if they are not coplanar):
 * int tri_tri_intersect_with_isectline(float V0[3],float V1[3],float V2[3], 
 *				        float U0[3],float U1[3],float U2[3],int *coplanar,
 *				        float isectpt1[3],float isectpt2[3]);
 * coplanar returns whether the tris are coplanar
 * isectpt1, isectpt2 are the endpoints of the line of intersection
 */

#include <math.h>

#define FABS(x) ((float)fabs(x))        /* implement as is fastest on your machine */

/* if USE_EPSILON_TEST is true then we do a check: 
         if |dv|<EPSILON then dv=0.0;
   else no check is done (which is less robust)
*/
#define USE_EPSILON_TEST TRUE  
#define EPSILON 0.000001


/* some macros */
#define CROSS(dest,v1,v2)                      \
              dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
              dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
              dest[2]=v1[0]*v2[1]-v1[1]*v2[0];

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

#define SUB(dest,v1,v2) dest[0]=v1[0]-v2[0]; dest[1]=v1[1]-v2[1]; dest[2]=v1[2]-v2[2]; 

#define ADD(dest,v1,v2) dest[0]=v1[0]+v2[0]; dest[1]=v1[1]+v2[1]; dest[2]=v1[2]+v2[2]; 

#define MULT(dest,v,factor) dest[0]=factor*v[0]; dest[1]=factor*v[1]; dest[2]=factor*v[2];

#define SET(dest,src) dest[0]=src[0]; dest[1]=src[1]; dest[2]=src[2]; 

/* sort so that a<=b */
#define SORT(a,b)       \
             if(a>b)    \
             {          \
               float c; \
               c=a;     \
               a=b;     \
               b=c;     \
             }

#define ISECT(VV0,VV1,VV2,D0,D1,D2,isect0,isect1) \
              isect0=VV0+(VV1-VV0)*D0/(D0-D1);    \
              isect1=VV0+(VV2-VV0)*D0/(D0-D2);


#define COMPUTE_INTERVALS(VV0,VV1,VV2,D0,D1,D2,D0D1,D0D2,isect0,isect1) \
  if(D0D1>0.0f)                                         \
  {                                                     \
    /* here we know that D0D2<=0.0 */                   \
    /* that is D0, D1 are on the same side, D2 on the other or on the plane */ \
    ISECT(VV2,VV0,VV1,D2,D0,D1,isect0,isect1);          \
  }                                                     \
  else if(D0D2>0.0f)                                    \
  {                                                     \
    /* here we know that d0d1<=0.0 */                   \
    ISECT(VV1,VV0,VV2,D1,D0,D2,isect0,isect1);          \
  }                                                     \
  else if(D1*D2>0.0f || D0!=0.0f)                       \
  {                                                     \
    /* here we know that d0d1<=0.0 or that D0!=0.0 */   \
    ISECT(VV0,VV1,VV2,D0,D1,D2,isect0,isect1);          \
  }                                                     \
  else if(D1!=0.0f)                                     \
  {                                                     \
    ISECT(VV1,VV0,VV2,D1,D0,D2,isect0,isect1);          \
  }                                                     \
  else if(D2!=0.0f)                                     \
  {                                                     \
    ISECT(VV2,VV0,VV1,D2,D0,D1,isect0,isect1);          \
  }                                                     \
  else                                                  \
  {                                                     \
    /* triangles are coplanar */                        \
    return coplanar_tri_tri(N1,V0,V1,V2,U0,U1,U2);      \
  }



/* this edge to edge test is based on Franlin Antonio's gem:
   "Faster Line Segment Intersection", in Graphics Gems III,
   pp. 199-202 */ 
#define EDGE_EDGE_TEST(V0,U0,U1)                      \
  Bx=U0[i0]-U1[i0];                                   \
  By=U0[i1]-U1[i1];                                   \
  Cx=V0[i0]-U0[i0];                                   \
  Cy=V0[i1]-U0[i1];                                   \
  f=Ay*Bx-Ax*By;                                      \
  d=By*Cx-Bx*Cy;                                      \
  if((f>0 && d>=0 && d<=f) || (f<0 && d<=0 && d>=f))  \
  {                                                   \
    e=Ax*Cy-Ay*Cx;                                    \
    if(f>0)                                           \
    {                                                 \
      if(e>=0 && e<=f) return 1;                      \
    }                                                 \
    else                                              \
    {                                                 \
      if(e<=0 && e>=f) return 1;                      \
    }                                                 \
  }                                

#define EDGE_AGAINST_TRI_EDGES(V0,V1,U0,U1,U2) \
{                                              \
  float Ax,Ay,Bx,By,Cx,Cy,e,d,f;               \
  Ax=V1[i0]-V0[i0];                            \
  Ay=V1[i1]-V0[i1];                            \
  /* test edge U0,U1 against V0,V1 */          \
  EDGE_EDGE_TEST(V0,U0,U1);                    \
  /* test edge U1,U2 against V0,V1 */          \
  EDGE_EDGE_TEST(V0,U1,U2);                    \
  /* test edge U2,U1 against V0,V1 */          \
  EDGE_EDGE_TEST(V0,U2,U0);                    \
}

#define POINT_IN_TRI(V0,U0,U1,U2)           \
{                                           \
  float a,b,c,d0,d1,d2;                     \
  /* is T1 completly inside T2? */          \
  /* check if V0 is inside tri(U0,U1,U2) */ \
  a=U1[i1]-U0[i1];                          \
  b=-(U1[i0]-U0[i0]);                       \
  c=-a*U0[i0]-b*U0[i1];                     \
  d0=a*V0[i0]+b*V0[i1]+c;                   \
                                            \
  a=U2[i1]-U1[i1];                          \
  b=-(U2[i0]-U1[i0]);                       \
  c=-a*U1[i0]-b*U1[i1];                     \
  d1=a*V0[i0]+b*V0[i1]+c;                   \
                                            \
  a=U0[i1]-U2[i1];                          \
  b=-(U0[i0]-U2[i0]);                       \
  c=-a*U2[i0]-b*U2[i1];                     \
  d2=a*V0[i0]+b*V0[i1]+c;                   \
  if(d0*d1>0.0)                             \
  {                                         \
    if(d0*d2>0.0) return 1;                 \
  }                                         \
}

bool coplanar_tri_tri(float N[3],float V0[3],float V1[3],float V2[3],
                     float U0[3],float U1[3],float U2[3])
{
   float A[3];
   short i0,i1;
   /* first project onto an axis-aligned plane, that maximizes the area */
   /* of the triangles, compute indices: i0,i1. */
   A[0]=fabs(N[0]);
   A[1]=fabs(N[1]);
   A[2]=fabs(N[2]);
   if(A[0]>A[1])
   {
      if(A[0]>A[2])  
      {
          i0=1;      /* A[0] is greatest */
          i1=2;
      }
      else
      {
          i0=0;      /* A[2] is greatest */
          i1=1;
      }
   }
   else   /* A[0]<=A[1] */
   {
      if(A[2]>A[1])
      {
          i0=0;      /* A[2] is greatest */
          i1=1;                                           
      }
      else
      {
          i0=0;      /* A[1] is greatest */
          i1=2;
      }
    }               
                
    /* test all edges of triangle 1 against the edges of triangle 2 */
    EDGE_AGAINST_TRI_EDGES(V0,V1,U0,U1,U2);
    EDGE_AGAINST_TRI_EDGES(V1,V2,U0,U1,U2);
    EDGE_AGAINST_TRI_EDGES(V2,V0,U0,U1,U2);
                
    /* finally, test if tri1 is totally contained in tri2 or vice versa */
    POINT_IN_TRI(V0,U0,U1,U2);
    POINT_IN_TRI(U0,V0,V1,V2);

    return false;
}

#define NEWCOMPUTE_INTERVALS(VV0,VV1,VV2,D0,D1,D2,D0D1,D0D2,A,B,C,X0,X1) \
{ \
        if(D0D1>0.0f) \
        { \
                /* here we know that D0D2<=0.0 */ \
            /* that is D0, D1 are on the same side, D2 on the other or on the plane */ \
                A=VV2; B=(VV0-VV2)*D2; C=(VV1-VV2)*D2; X0=D2-D0; X1=D2-D1; \
        } \
        else if(D0D2>0.0f)\
        { \
                /* here we know that d0d1<=0.0 */ \
            A=VV1; B=(VV0-VV1)*D1; C=(VV2-VV1)*D1; X0=D1-D0; X1=D1-D2; \
        } \
        else if(D1*D2>0.0f || D0!=0.0f) \
        { \
                /* here we know that d0d1<=0.0 or that D0!=0.0 */ \
                A=VV0; B=(VV1-VV0)*D0; C=(VV2-VV0)*D0; X0=D0-D1; X1=D0-D2; \
        } \
        else if(D1!=0.0f) \
        { \
                A=VV1; B=(VV0-VV1)*D1; C=(VV2-VV1)*D1; X0=D1-D0; X1=D1-D2; \
        } \
        else if(D2!=0.0f) \
        { \
                A=VV2; B=(VV0-VV2)*D2; C=(VV1-VV2)*D2; X0=D2-D0; X1=D2-D1; \
        } \
        else \
        { \
                /* triangles are coplanar */ \
                return coplanar_tri_tri(N1,V0,V1,V2,U0,U1,U2); \
        } \
}

bool NoDivTriTriIsect(float V0[3],float V1[3],float V2[3],
                     float U0[3],float U1[3],float U2[3])
{
  float E1[3],E2[3];
  float N1[3],N2[3],d1,d2;
  float du0,du1,du2,dv0,dv1,dv2;
  float D[3];
  float isect1[2], isect2[2];
  float du0du1,du0du2,dv0dv1,dv0dv2;
  short index;
  float vp0,vp1,vp2;
  float up0,up1,up2;
  float bb,cc,max;
  float a,b,c,x0,x1;
  float d,e,f,y0,y1;
  float xx,yy,xxyy,tmp;

  /* compute plane equation of triangle(V0,V1,V2) */
  SUB(E1,V1,V0);
  SUB(E2,V2,V0);
  CROSS(N1,E1,E2);
  d1=-DOT(N1,V0);
  /* plane equation 1: N1.X+d1=0 */

  /* put U0,U1,U2 into plane equation 1 to compute signed distances to the plane*/
  du0=DOT(N1,U0)+d1;
  du1=DOT(N1,U1)+d1;
  du2=DOT(N1,U2)+d1;

  /* coplanarity robustness check */
#if USE_EPSILON_TEST==TRUE
  if(FABS(du0)<EPSILON) du0=0.0;
  if(FABS(du1)<EPSILON) du1=0.0;
  if(FABS(du2)<EPSILON) du2=0.0;
#endif
  du0du1=du0*du1;
  du0du2=du0*du2;

  if(du0du1>0.0f && du0du2>0.0f) /* same sign on all of them + not equal 0 ? */
    return false;                    /* no intersection occurs */

  /* compute plane of triangle (U0,U1,U2) */
  SUB(E1,U1,U0);
  SUB(E2,U2,U0);
  CROSS(N2,E1,E2);
  d2=-DOT(N2,U0);
  /* plane equation 2: N2.X+d2=0 */

  /* put V0,V1,V2 into plane equation 2 */
  dv0=DOT(N2,V0)+d2;
  dv1=DOT(N2,V1)+d2;
  dv2=DOT(N2,V2)+d2;

#if USE_EPSILON_TEST==TRUE
  if(FABS(dv0)<EPSILON) dv0=0.0;
  if(FABS(dv1)<EPSILON) dv1=0.0;
  if(FABS(dv2)<EPSILON) dv2=0.0;
#endif

  dv0dv1=dv0*dv1;
  dv0dv2=dv0*dv2;

  if(dv0dv1>0.0f && dv0dv2>0.0f) /* same sign on all of them + not equal 0 ? */
    return false;                    /* no intersection occurs */

  /* compute direction of intersection line */
  CROSS(D,N1,N2);

  /* compute and index to the largest component of D */
  max=(float)FABS(D[0]);
  index=0;
  bb=(float)FABS(D[1]);
  cc=(float)FABS(D[2]);
  if(bb>max) max=bb,index=1;
  if(cc>max) max=cc,index=2;

  /* this is the simplified projection onto L*/
  vp0=V0[index];
  vp1=V1[index];
  vp2=V2[index];

  up0=U0[index];
  up1=U1[index];
  up2=U2[index];

  /* compute interval for triangle 1 */
  NEWCOMPUTE_INTERVALS(vp0,vp1,vp2,dv0,dv1,dv2,dv0dv1,dv0dv2,a,b,c,x0,x1);

  /* compute interval for triangle 2 */
  NEWCOMPUTE_INTERVALS(up0,up1,up2,du0,du1,du2,du0du1,du0du2,d,e,f,y0,y1);

  xx=x0*x1;
  yy=y0*y1;
  xxyy=xx*yy;

  tmp=a*xxyy;
  isect1[0]=tmp+b*x1*yy;
  isect1[1]=tmp+c*x0*yy;

  tmp=d*xxyy;
  isect2[0]=tmp+e*xx*y1;
  isect2[1]=tmp+f*xx*y0;

  SORT(isect1[0],isect1[1]);
  SORT(isect2[0],isect2[1]);

  if(isect1[1]<isect2[0] || isect2[1]<isect1[0]) return false;
  return true;
}

/* sort so that a<=b */
#define SORT2(a,b,smallest)       \
             if(a>b)       \
             {             \
               float c;    \
               c=a;        \
               a=b;        \
               b=c;        \
               smallest=1; \
             }             \
             else smallest=0;


Surface::Surface()
{
	FILE *f;
	double delta_central, delta_superior, delta_inferior;
	f = fopen("../parametros.txt", "r");

	fscanf(f, "%lf", &alfax);
	fscanf(f, "%lf", &alfay);
	fscanf(f, "%lf", &u0);
	fscanf(f, "%lf", &v0);
	fscanf(f, "%lf", &s);

	fscanf(f, "%lf", &delta_central);
	fscanf(f, "%lf", &delta_superior);
	fscanf(f, "%lf", &delta_inferior);

	//wx[0] = 0.090174770424681035 + delta_wx;
	//wx[1] = -0.14726215563702155 + delta_wx;
	//wx[2] = -0.39269908169872414 + delta_wx;

	wx[1] = -0.14726215563702155 + delta_central;
	wx[0] = wx[1] + delta_superior;// 0,237436926061702585
	wx[2] = wx[1] - delta_inferior;// -0,24543692606170259

	fscanf(f, "%lf", &wy[0]);
	wy[1] = wy[0];
	wy[2] = wy[0];

	fscanf(f, "%lf", &wz[0]);
	wz[1] = wz[0];
	wz[2] = wz[0];

	fscanf(f, "%lf", &tx);
	fscanf(f, "%lf", &ty);
	fscanf(f, "%lf", &tz);

	//  5.625
	// -8.4375
	// -22.5


	fclose(f);

	centroCamara[0][0] = tx;
	centroCamara[0][1] = ty;
	centroCamara[0][2] = -tz;

	centroCamara[1][0] = 0;//tx;
	centroCamara[1][1] = 400;//ty;
	centroCamara[1][2] = 65;//-tz;

	centroCamara[2][0] = 0;//tx;
	centroCamara[2][1] = 400;//ty;
	centroCamara[2][2] = 65;//-tz;

	numTriangulosConTextura = numTriangulosOcluidos = 0;

	listasInicializadas = InitialiceLists();
}

bool Surface::CreateSurface(bool texturizar)
{
	int numT;
	if (listasInicializadas)
	{
		GetData(texturizar);
		if (texturizar)
		{
			OcclusionDetection();
			//FiltrarTriangulos();
		}

		numT = Input::GetNumTriangulos();
		return true;
	}
	else
		return false;
}

Surface::~Surface(void)
{
}

bool Surface::InitialiceLists()
{
	register int i, largo;

	largoX = 2*(MAX_DIST/TAM_CELDA);
	largoY =   (MAX_DIST/TAM_CELDA);
	largoZ = 2*(MAX_DIST/TAM_CELDA);

	listaVertices = (Vertex**)malloc(numVertices*sizeof(Vertex));

	mallaTriangulos = (TriangleList**)malloc(largoX*largoY*largoZ*sizeof(TriangleList));
	if (mallaTriangulos == NULL)
		return false;

	largo = largoX*largoY*largoZ;
	for(i=0; i<largo; ++i)
		mallaTriangulos[i] = NULL;

	notProyectedTriangles = new TriangleList();
	if (notProyectedTriangles == NULL)
		return false;

	occludedTriangles = new TriangleList();
	if (occludedTriangles == NULL)
		return false;

	return true;
}

void Surface::GetData(bool texturizar)
{
	register int i, indiceTriangulos;
	int x, y, z;
	int u0[NUM_IMAGENES][3], v0[NUM_IMAGENES][3];
	double px[3], py[3], pz[3];
	Vertex *v[3];
	Vertex *vMedio;
	Triangle *t;
	double dMin, d;
	int indiceImagen;
	bool seProyecta;

	minCeldaY = largoY*2;
	maxCeldaY = 0;

	MatrizProyeccion();

	FILE *aux;
//	double a, b, c;
	aux = fopen("calibration-object.txt", "r");
	//int uaux, vaux;

	listaVertices = Input::GetListadoVertices();
	numTriangles = Input::GetNumTriangulos();

	for(indiceTriangulos = 0; indiceTriangulos<numTriangles; ++indiceTriangulos)
	{
		t = Input::ReadTriangle();

		if (texturizar)
		{
			v[0] = t->GetVertex(0);
			v[1] = t->GetVertex(1);
			v[2] = t->GetVertex(2);

			//obtiene el punto medio del triangulo (deberia usarse una clase punto en vez de la clase vertice, pero solo por correctitud de la estructura de clases)
			vMedio = this->TriangleCenter(t);

			//obtiene las coordenadas de la celda que contiene al punto central.
			x = (int)(MAX_DIST+vMedio->GetX())/TAM_CELDA;
			y = (int)(vMedio->GetY()/TAM_CELDA);
			z = (int)(MAX_DIST+vMedio->GetZ())/TAM_CELDA;

			//obtiene las coordenadas de los 3 vertices
			for(i=0; i<3; ++i)
			{
				px[i] = t->GetVertex(i)->GetX();
				py[i] = t->GetVertex(i)->GetY();
				pz[i] = t->GetVertex(i)->GetZ();
			}

			//revisa los tres vertices para las N imagenes
			dMin = DBL_MAX;
			seProyecta = false;

			for(i=0; i<NUM_IMAGENES/*NUM_IMAGENES-1*/; ++i)
			{
				//proyecta las coordenadas sobre las NUM_IMAGENES imagenes, encontrando los pixeles correspondientes.
				Transformacion(i, px[0], py[0], pz[0], &u0[i][0], &v0[i][0]);
				Transformacion(i, px[1], py[1], pz[1], &u0[i][1], &v0[i][1]);
				Transformacion(i, px[2], py[2], pz[2], &u0[i][2], &v0[i][2]);

				//revisa que las 3 proyecciones se realicen sobre la imagen
				if ((u0[i][0]>=0 && v0[i][0]>=0 && u0[i][0]<640 && v0[i][0]<480) &&
					(u0[i][1]>=0 && v0[i][1]>=0 && u0[i][1]<640 && v0[i][1]<480) &&
					(u0[i][2]>=0 && v0[i][2]>=0 && u0[i][2]<640 && v0[i][2]<480))
				{
					//calcula la distancia desde el centro del triangulo al centro de la imagen
					d  = sqrt( pow((u0[i][0] + u0[i][1] + u0[i][2])/3.0 - 320,2) + pow((v0[i][0] + v0[i][1] + v0[i][2])/3.0 - 240,2));
					d /= (i+1);
					
					//guarda esa distancia
					t->DistanciaCentroImagen(i,d);
					
					//indica que el triangulo se proyecta al menos sobre una imagen
					seProyecta = true;

					indiceImagen = i;
					
					//guarda la menor de las distancias y el indice de la imagen correspondiente
					if (d < dMin)
					{
						dMin = d;
						indiceImagen = i;
					}
				}
			}
		
			if (seProyecta)
			{
				if (mallaTriangulos[x + largoX*z + largoX*largoZ*y] == NULL)
					mallaTriangulos[x + largoX*z + largoX*largoZ*y] = new TriangleList();

				mallaTriangulos[x + largoX*z + largoX*largoZ*y]->Add(t);
				t->SetImagePixels(indiceImagen, u0, v0);
				if (y < minCeldaY) minCeldaY = y;
				if (y > maxCeldaY) maxCeldaY = y;
				++numTriangulosConTextura;
			}
			/*else
			{
				if (mallaTriangulos[x + largoX*z + largoX*largoZ*y] == NULL)
					mallaTriangulos[x + largoX*z + largoX*largoZ*y] = new TriangleList();

				mallaTriangulos[x + largoX*z + largoX*largoZ*y]->Add(t);
				//t->SetImagePixels(-1, u0, v0);
			}*/
			//else
			//{
			//	notProyectedTriangles->Add(t);
			//}
		}
		//else
		//{
		//	notProyectedTriangles->Add(t);
		//}
	}

	yMedio = TAM_CELDA*(minCeldaY + maxCeldaY)/2;

	//libera la memoria utilizada
	Input::LiberarListas();
}

TriangleCell* Surface::GetNTTriangleCell()
{
	return notProyectedTriangles->GetFirst();
}

TriangleCell* Surface::GetOccludedTriangleCell()
{
	return occludedTriangles->GetFirst();
}

Vertex* Surface::TriangleCenter(Triangle *t)
{
	int i;
	double x, y, z;

	x = y = z = 0.0;
	for(i=0; i<3; ++i)
	{
		x += t->GetVertex(i)->GetX()/3;
		y += t->GetVertex(i)->GetY()/3;
		z += t->GetVertex(i)->GetZ()/3;
	}
	return new Vertex(x, y, z);
}

double Surface::Radio(Triangle *t, Vertex *v)
{
	double a, b, c;
	double d1, d2, d3;

	a = t->GetVertex(0)->GetX() - v->GetX();
	b = t->GetVertex(0)->GetY() - v->GetY();
	c = t->GetVertex(0)->GetZ() - v->GetZ();
	d1 = sqrt(a*a + b*b + c*c);

	a = t->GetVertex(1)->GetX() - v->GetX();
	b = t->GetVertex(1)->GetY() - v->GetY();
	c = t->GetVertex(1)->GetZ() - v->GetZ();
	d2 = sqrt(a*a + b*b + c*c);

	a = t->GetVertex(2)->GetX() - v->GetX();
	b = t->GetVertex(2)->GetY() - v->GetY();
	c = t->GetVertex(2)->GetZ() - v->GetZ();
	d3 = sqrt(a*a + b*b + c*c);

	if (d1 < d2 && d1 < d3)
		return d1;
	else if (d2 < d1 && d2 < d3)
		return d2;
	else
		return d3;
}

double Surface::Distancia(double x[3], Vertex *v)
{
	double a, b, c;
	a = x[0] - v->GetX();
	b = x[1] - v->GetY();
	c = x[2] - v->GetZ();
	return sqrt(a*a + b*b + c*c);
}

void Surface::OcclusionDetection()
{
	int i, j, k;
	int i2, j2, k2;
	int ic, jc, kc;
	register int auxI, auxJ, auxK;
	TriangleList *l, *lAux;
	TriangleCell *c, *temp;
	Triangle *t;
	bool ocluido;
	double d;
	int numComp = 0;
	
	ic = (int)(MAX_DIST+centroCamara[0][0])/TAM_CELDA;
	jc = (int)(			centroCamara[0][1])/TAM_CELDA;
	kc = (int)(MAX_DIST+centroCamara[0][2])/TAM_CELDA;

	for(j=jc/*minCeldaY + largoY/4*/; j<largoY; ++j)
	{
		for(k=0; k<largoZ; ++k)
		{
			for(i=0; i<largoX; ++i)
			{
				if (!IsEmptyTriangleList(i, j, k))
				{
					l = mallaTriangulos[i + largoX*k + largoX*largoZ*j];
					c = l->GetFirst();

					while(c!=NULL)
					{
						t = c->GetTriangle();

						ocluido = false;
						for(j2=jc/*minCeldaY*/; j2<j; ++j2)
						{
							++numComp;

							if (j==jc)
								continue;

							d = ((double)(j2-j))/(double)(j-jc);
							i2 = (int)(i + d*(i-ic));
							k2 = (int)(k + d*(k-kc));

							if (sqrt((double)((i-i2)*(i-i2) + (j-j2)*(j-j2) + (k-k2)*(k-k2))) <= 10)
								continue;

							ocluido = false;
							auxI = 0;
							auxJ = 0;
							auxK = 0;
							for(auxI=-1; auxI<2 && !ocluido; ++auxI)
							{
								//for(auxJ=-1; auxJ<2 && !ocluido; ++auxJ)
								{
									for(auxK=-1; auxK<2 && !ocluido; ++auxK)
									{
										if (!ocluido && !IsEmptyTriangleList(i2+auxI, j2+auxJ, k2+auxK))
										{
											lAux = mallaTriangulos[i2+auxI + largoX*(k2+auxK) + largoX*largoZ*(j2+auxJ)];
											if (OcclusionDetection(t, lAux))
												ocluido = true;
										}
									}
								}
							}

							if (ocluido)
							{
								//t->SetImagePixels(-1,NULL, NULL);
								temp = c->GetNext();
								occludedTriangles->Add(t);
								//delete t;
								l->Bypass(c);
								c = temp;
								++numTriangulosOcluidos;
								break;
							}
						}
						if (!ocluido)
							c = c->GetNext();
					}
				}
			}
		}
	}
}

void Surface::FiltrarTriangulos()
{
	int i, j, k;
//	int i2, j2, k2;
//	TriangleList *l, *lAux;
	TriangleList *l;
	TriangleCell *c, *temp;
	Triangle *t;
//	bool ocluido;
//	double d;

	int indiceImagen;
	int u[3], v[3];
	
	for(j=minCeldaY + largoY/4; j<largoY; ++j)
	{
		for(k=0; k<largoZ; ++k)
		{
			for(i=0; i<largoX; ++i)
			{
				if (!IsEmptyTriangleList(i, j, k))
				{
					l = mallaTriangulos[i + largoX*k + largoX*largoZ*j];
					c = l->GetFirst();

					while(c!=NULL)
					{
						t = c->GetTriangle();
						t->GetImagePixels(&indiceImagen, u, v);

						if (u==NULL || v== NULL || indiceImagen==-1)
						{
							temp = c->GetNext();
							delete t;
							l->Bypass(c);
							c = temp;
							break;
						}
						else
							c = c->GetNext();
					}
				}
			}
		}
	}
}

//Revisa si el triangulo t es tapado por algun otro triangulo en la celda c
//Si al menos un vertice de t es tapado de dira que el triangulo esta ocluido
bool Surface::OcclusionDetection(Triangle *t, TriangleList *l)
{
	int i;
	float A, B, C, D;
	float x[3], y[3], z[3];
	float xo[3], yo[3], zo[3], intersection[3][3];
	float n[3];
	double dotProduct, d;
	double difY;
	
	Vertex *v[3], *vo[3];
	TriangleCell *c;
	Triangle *to;

	for(i=0; i<3; ++i)
	{
		//obtiene los vertices y las coordenadas de los v�rtices del triangulo ocluido
		v[i] = t->GetVertex(i);
		x[i] = (float)v[i]->GetX();
		y[i] = (float)v[i]->GetY();
		z[i] = (float)v[i]->GetZ();
	}

	c = l->GetFirst();
	while (c!=NULL)
	{
		to = c->GetTriangle();
		if (t == to)
		{
			c = c->GetNext();
			continue;
		}

		for(i=0; i<3; ++i)
		{
			//obtiene los vertices y las coordenadas de los v�rtices del supuesto triangulo oclusor
			vo[i] = to->GetVertex(i);
			xo[i] = (float)vo[i]->GetX();
			yo[i] = (float)vo[i]->GetY();
			zo[i] = (float)vo[i]->GetZ();
		}

		difY = (yo[0] + yo[1] + yo[2])/3.0 - (y[0] + y[1] + y[2])/3.0;
		if (difY > 0)
		{
			c = c->GetNext();
			continue;
		}
		else
		{
			/*if ((xo[0] == x[0] && yo[0] == y[0]) ||
				(xo[0] == x[1] && yo[0] == y[1]) ||
				(xo[0] == x[2] && yo[0] == y[2]) ||
				(xo[1] == x[0] && yo[1] == y[0]) ||
				(xo[1] == x[1] && yo[1] == y[1]) ||
				(xo[1] == x[2] && yo[1] == y[2]) ||
				(xo[2] == x[0] && yo[2] == y[0]) ||
				(xo[2] == x[1] && yo[2] == y[1]) ||
				(xo[2] == x[2] && yo[2] == y[2]))
			{
				c = c->GetNext();
				continue;
			}*/
		}

		///encuentra el plano Ax + By + Cz + D = 0 sobre el que se encuentra el triangulo oclusor (to)
		A = ( yo[0]*(zo[1] - zo[2]) + yo[1]*(zo[2] - zo[0]) + yo[2]*(zo[0] - zo[1]) );
		B = ( zo[0]*(xo[1] - xo[2]) + zo[1]*(xo[2] - xo[0]) + zo[2]*(xo[0] - xo[1]) );
		C = ( xo[0]*(yo[1] - yo[2]) + xo[1]*(yo[2] - yo[0]) + xo[2]*(yo[0] - yo[1]) );
		D = ( xo[0]*(yo[2]*zo[1] - yo[1]*zo[2]) + xo[1]*(yo[0]*zo[2] - yo[2]*zo[0]) + xo[2]*(yo[1]*zo[0] - yo[0]*zo[1]) );

		//setea la normal del plano Ax + By + Cz + D = 0, que es [A, B, C]
		n[0] = A;
		n[1] = B;
		n[2] = C;

		for(i=0; i<3; ++i)
		{
			//si el producto punto entre (X - CentroCamara) y n es 0 entonces no hay interseccion
			dotProduct = (centroCamara[0][0] - x[i])*A + 
						 (centroCamara[0][1] - y[i])*B +
						 (centroCamara[0][2] - z[i])*C;

			if (dotProduct != 0)
			{
				//calcula el punto de interseccion del plano con la recta que une la camara con el triangulo t ocluido 
				d = -(A*x[i] + B*y[i] + C*z[i] + D)/dotProduct;

				intersection[i][0] = (float) ( x[i] + d*(centroCamara[0][0]-x[i]) );
				intersection[i][1] = (float) ( y[i] + d*(centroCamara[0][1]-y[i]) );
				intersection[i][2] = (float) ( z[i] + d*(centroCamara[0][2]-z[i]) );
			}
			else
				return false;
		}

		if (HaySolapamiento(n, intersection, to))
			return true;
		
		c = c->GetNext();
	}

	return false;
}

bool Surface::HaySolapamiento(float n[3], float intersection[3][3], Triangle *t)
{
	int i, j;
	float inters[3][3];
	float tr[3][3];

	for(i=0; i<3; ++i)
		for(j=0; j<3; ++j)
			inters[i][j] = (float)intersection[i][j];

	tr[0][0] = (float)t->GetVertex(0)->GetX();
	tr[0][1] = (float)t->GetVertex(0)->GetY();
	tr[0][2] = (float)t->GetVertex(0)->GetZ();

	tr[1][0] = (float)t->GetVertex(1)->GetX();
	tr[1][1] = (float)t->GetVertex(1)->GetY();
	tr[1][2] = (float)t->GetVertex(1)->GetZ();

	tr[2][0] = (float)t->GetVertex(2)->GetX();
	tr[2][1] = (float)t->GetVertex(2)->GetY();
	tr[2][2] = (float)t->GetVertex(2)->GetZ();

	//return NoDivTriTriIsect(inters[0], inters[1], inters[2], tr[0], tr[1], tr[2]);
	return coplanar_tri_tri(n, inters[0], inters[1], inters[2], tr[0], tr[1], tr[2]);

	/*double xt1[3], yt1[3], xt2[3], yt2[3];
	double x1, x2, x3, x4, y1, y2, y3, y4;
	double ua, ub, den;

	xt1[0] = t->GetVertex(0)->GetX();
	yt1[0] = t->GetVertex(0)->GetZ();
	xt1[1] = t->GetVertex(1)->GetX();
	yt1[1] = t->GetVertex(1)->GetZ();
	xt1[2] = t->GetVertex(2)->GetX();
	yt1[2] = t->GetVertex(2)->GetZ();

	xt2[0] = intersection[0][0];
	yt2[0] = intersection[0][2];
	xt2[1] = intersection[1][0];
	yt2[1] = intersection[1][2];
	xt2[2] = intersection[2][0];
	yt2[2] = intersection[2][2];

	for(i=0; i<3; ++i)
	{
		x1 = xt1[i%3];
		y1 = yt1[i%3];
		x2 = xt1[(i+1)%3];
		y2 = yt1[(i+1)%3];

		for(j=0; j<3; ++j)
		{
			x3 = xt2[j%3];
			y3 = yt2[j%3];
			x4 = xt2[(j+1)%3];
			y4 = yt2[(j+1)%3];

			den= (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1);
			if (den==0) continue;

			ua = (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3);
			if (ua ==0) continue;

			ub = (x2-x1)*(y1-y3) - (y2-y1)*(x1-x3);
			if (ub ==0) continue;

			ua /= den;
			ub /= den;

			if (ua > 0 && ua < 1 && ub > 0 && ub < 1)
				return true;
		}
	}

	return false;*/
}

/*bool Surface::IsPointInTriangle(double intersection[3], Triangle *t, double normal[3])
{
	int i;
	double vec1[3], vec2[3], vecN[3];

	for(i=0; i<3; ++i)
	{
		//vector B-A / C-B / A-C
		vec1[0] = t->GetVertex((i+1)%3)->GetX() - t->GetVertex(i)->GetX(); 
		vec1[1] = t->GetVertex((i+1)%3)->GetY() - t->GetVertex(i)->GetY();
		vec1[2] = t->GetVertex((i+1)%3)->GetZ() - t->GetVertex(i)->GetZ();

		//vector p-A / p-B / p-C
		vec2[0] = intersection[0] - t->GetVertex(i)->GetX();
		vec2[1] = intersection[1] - t->GetVertex(i)->GetY();
		vec2[2] = intersection[2] - t->GetVertex(i)->GetZ();

		//normal del triangulo segun considerando la interseccion en vez del punto C (mediante producto cruz)
		vecN[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1];
		vecN[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2];
		vecN[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0];
		
		//si vecN != normal => interseccion esta fuera del triangulo
		if (vecN[0]!=normal[0] || vecN[1]!=normal[1] || vecN[2]!=normal[2])
			return false;
	}

	return true;
}*/

/*//Revisa si tc1 es tapado por alguien
void Surface::OcclusionDetection(int i0, int j0, int k0, TriangleCell *tc1)
{
	int i, j, k, l;//, m;
	double A, B, C, D;
	double t, VD;
	double Rd[3];
	double intersection[3];
	//bool hayOclusion;
	bool oclusion[NUM_IMAGENES];
	int dummy[3];
	int img;
	Vector *normal;
	Vertex *v1, *v2;
	Triangle *tr2;
	TriangleCell *tc2;
	
	v1 = TriangleCenter(tc1->GetTriangle());



	for(j=0; j<=j0; ++j)
	{
		tc1->GetTriangle()->GetImagePixels(&img, dummy, dummy);
		
		//l = img;
		for(l=0; l<NUM_IMAGENES; ++l)
		{
			Rd[0] = v1->GetX() - posCamara[l][0];
			Rd[1] = v1->GetY() - posCamara[l][1];
			Rd[2] = v1->GetZ() - posCamara[l][2];

			t = ((TAM_CELDA*j) - posCamara[l][1])/Rd[1]; 

			i = TAM_CELDA + (int)((posCamara[l][0] + t*Rd[0])/TAM_CELDA);
			//j = (int)((0 + t*v1->GetY())/TAM_CELDA);
			k = TAM_CELDA + (int)((posCamara[l][2] + t*Rd[2])/TAM_CELDA);

			if (i==i0 && j==j0 && k==k0)
				return;

			oclusion[l] = false;

			if (!IsEmptyTriangleList(i, j, k))
			{
				tc2 = mallaTriangulos[i + largoX*k + largoX*largoZ*j]->GetFirst();

				while(tc2 != NULL)
				{
					//queremos ver si tc2 tapa a tc1 (excepto si tc2 es tc1)
					tr2 = tc2->GetTriangle();
					v2 = TriangleCenter(tr2);
					if (tc1 != tc2 && Distancia3D(v1, v2) > 2*DELTA)
					{
						oclusion[l] = true;
						
						//si v2 esta antes que v1 entonces v2 puede tapar a v1
						if (v2->GetZ() < v1->GetZ())
						{
							//Ax + By + Cz + D = 0
							A = tr2->GetVertex(0)->GetY()*(tr2->GetVertex(1)->GetZ() - tr2->GetVertex(2)->GetZ()) + tr2->GetVertex(1)->GetY()*(tr2->GetVertex(2)->GetZ() - tr2->GetVertex(0)->GetZ()) + tr2->GetVertex(2)->GetY()*(tr2->GetVertex(0)->GetZ() - tr2->GetVertex(1)->GetZ());
							B = tr2->GetVertex(0)->GetZ()*(tr2->GetVertex(1)->GetX() - tr2->GetVertex(2)->GetX()) + tr2->GetVertex(1)->GetZ()*(tr2->GetVertex(2)->GetX() - tr2->GetVertex(0)->GetX()) + tr2->GetVertex(2)->GetZ()*(tr2->GetVertex(0)->GetX() - tr2->GetVertex(1)->GetX());
							C = tr2->GetVertex(0)->GetX()*(tr2->GetVertex(1)->GetY() - tr2->GetVertex(2)->GetY()) + tr2->GetVertex(1)->GetX()*(tr2->GetVertex(2)->GetY() - tr2->GetVertex(0)->GetY()) + tr2->GetVertex(2)->GetX()*(tr2->GetVertex(0)->GetY() - tr2->GetVertex(1)->GetY());
							D = tr2->GetVertex(0)->GetX()*(tr2->GetVertex(2)->GetY()*tr2->GetVertex(1)->GetZ() - tr2->GetVertex(1)->GetY()*tr2->GetVertex(2)->GetZ()) + tr2->GetVertex(1)->GetX()*(tr2->GetVertex(0)->GetY()*tr2->GetVertex(2)->GetZ() - tr2->GetVertex(2)->GetY()*tr2->GetVertex(0)->GetZ()) + tr2->GetVertex(2)->GetX()*(tr2->GetVertex(1)->GetY()*tr2->GetVertex(0)->GetZ() - tr2->GetVertex(0)->GetY()*tr2->GetVertex(1)->GetZ());

							//PlaneNormal dot V1 == 0 => Not intersection
							VD = A*v1->GetX() + B*v1->GetY() + C*v1->GetZ();
							if (VD != 0)
							{
								t = -(A*posCamara[l][0] + B*posCamara[l][1] + C*posCamara[l][2] + D)/VD;

								intersection[0] = posCamara[l][0] + t*v1->GetX();
								intersection[1] = posCamara[l][1] + t*v1->GetY();
								intersection[2] = posCamara[l][2] + t*v1->GetZ();

								//normal del triangulo
								normal = new Vector(A, B, C);

								//if (PointInTriangle(intersection, tr2, normal))
								//	oclusion[l] = true;
								//else
								//	oclusion[l] = false;

								delete normal;
							}
						}

						delete v2;
					}
					tc2 = tc2->GetNext();
				}
			}
		}
		
		//hayOclusion = true;
		//for(m=0; m<NUM_IMAGENES; ++m)
		//	if (oclusion[m] == false)
		//		hayOclusion = false;

		//if (hayOclusion)
		//{
			//tc1->GetTriangle()->SetImagePixels(-1, NULL, NULL);
		//	break;
		//}
		//else
		
		if (img>=0 && oclusion[img])
		{
			//hayOclusion = true;
			//for(m=0; m<NUM_IMAGENES; ++m)
			//{
			//	if (!oclusion[m])
			//	{
			//		px = tc1->GetTriangle()->GetVertex(0)->GetX();
			//		py = tc1->GetTriangle()->GetVertex(0)->GetY();
			//		pz = tc1->GetTriangle()->GetVertex(0)->GetZ();
			//		Transformacion(m, px, py ,pz, &u0[0], &v0[0]);

			//		if (u0[0]<0 || v0[0]<0 || u0[0]>=640 || v0[0]>=480)
			//			break;

			//		px = tc1->GetTriangle()->GetVertex(1)->GetX();
			//		py = tc1->GetTriangle()->GetVertex(1)->GetY();
			//		pz = tc1->GetTriangle()->GetVertex(1)->GetZ();
			//		Transformacion(m, px, py ,pz, &u0[1], &v0[1]);

			//		if (u0[1]<0 || v0[1]<0 || u0[1]>=640 || v0[1]>=480)
			//			break;

			//		px = tc1->GetTriangle()->GetVertex(2)->GetX();
			//		py = tc1->GetTriangle()->GetVertex(2)->GetY();
			//		pz = tc1->GetTriangle()->GetVertex(2)->GetZ();
			//		Transformacion(m, px, py ,pz, &u0[2], &v0[2]);

			//		if (u0[2]<0 || v0[2]<0 || u0[2]>=640 || v0[2]>=480)
			//			break;

			//		tc1->GetTriangle()->SetImagePixels(m, u0, v0);
			//		hayOclusion = false;
			//		break;
			//	}
			//}

			//if (hayOclusion)
			{
				Vertex *v1Temp, *v2Temp, *v3Temp;
				Triangle *trTemp;

				v1Temp = new Vertex(tc1->GetTriangle()->GetVertex(0)->GetX(), tc1->GetTriangle()->GetVertex(0)->GetY(), tc1->GetTriangle()->GetVertex(0)->GetZ());
				v2Temp = new Vertex(tc1->GetTriangle()->GetVertex(1)->GetX(), tc1->GetTriangle()->GetVertex(1)->GetY(), tc1->GetTriangle()->GetVertex(1)->GetZ());
				v3Temp = new Vertex(tc1->GetTriangle()->GetVertex(2)->GetX(), tc1->GetTriangle()->GetVertex(2)->GetY(), tc1->GetTriangle()->GetVertex(2)->GetZ());

				trTemp = new Triangle(v1Temp, v2Temp, v3Temp);

				mallaTriangulos[i0 + largoX*k0 + largoX*largoZ*j0]->Bypass(tc1);
				occludedTriangles->Add(trTemp);
				delete tc1;
				break;
			}
		}
		else
		{
			int c = 1;
		}
	}

	delete v1;
}*/

void Surface::Transformacion(int imagen, double Xp, double Yp, double Zp, int *u, int *v)
{
	double U, V;
	double divisor;
	double X, Y, Z;

	X = Zp;
	Y = Xp;
	Z = Yp;
	
	//C�lculo de m
	U = KPS[imagen][0][0]*X + KPS[imagen][0][1]*Y + KPS[imagen][0][2]*Z + KPS[imagen][0][3]*1;
	V = KPS[imagen][1][0]*X + KPS[imagen][1][1]*Y + KPS[imagen][1][2]*Z + KPS[imagen][1][3]*1;
	divisor = KPS[imagen][2][0]*X + KPS[imagen][2][1]*Y + KPS[imagen][2][2]*Z + KPS[imagen][2][3]*1;

	*u = (int)(U/divisor);
	*v = (int)(V/divisor);

	/*double U, V;
	double divisor;
	double X, Y, Z;

	X = Zp;
	Y = Xp;
	Z = Yp;
	
	//C�lculo de m
	U = KPS[imagen][0][0]*X + KPS[imagen][0][1]*Y + KPS[imagen][0][2]*Z + KPS[imagen][0][3]*1;
	V = KPS[imagen][1][0]*X + KPS[imagen][1][1]*Y + KPS[imagen][1][2]*Z + KPS[imagen][1][3]*1;
	divisor = KPS[imagen][2][0]*X + KPS[imagen][2][1]*Y + KPS[imagen][2][2]*Z + KPS[imagen][2][3]*1;

	U = U/divisor;
	V = V/divisor;

	*u = (int)(V);
	*v = (int)(480-U);*/
}

void Surface::MatrizProyeccion()
{
	double KP[3][4];
	double S[NUM_IMAGENES][4][4];
	int i;
	double wxtemp;

	//MATRIZ KP
	KP[0][0] = alfax;
	KP[0][1] = s;
	KP[0][2] = u0;
	KP[0][3] = 0;
	
	KP[1][0] = 0;
	KP[1][1] = alfay;
	KP[1][2] = v0;
	KP[1][3] = 0;
	
	KP[2][0] = 0;
	KP[2][1] = 0;
	KP[2][2] = 1;
	KP[2][3] = 0;

	//MATRIZ S64
	for (i=0; i<NUM_IMAGENES; ++i)
	{
		wxtemp = wx[i];

		S[i][0][0] = cos(wy[i])*cos(wz[i]);
		S[i][0][1] = cos(wy[i])*sin(wz[i]);
		S[i][0][2] = -sin(wy[i]);
		S[i][0][3] = tx;

		S[i][1][0] = sin(wxtemp)*sin(wy[i])*cos(wz[i])-cos(wxtemp)*sin(wz[i]);
		S[i][1][1] = sin(wxtemp)*sin(wy[i])*sin(wz[i])+cos(wxtemp)*cos(wz[i]);
		S[i][1][2] = sin(wxtemp)*cos(wy[i]);
		S[i][1][3] = ty;

		S[i][2][0] = cos(wxtemp)*sin(wy[i])*cos(wz[i])+sin(wxtemp)*sin(wz[i]);
		S[i][2][1] = cos(wxtemp)*sin(wy[i])*sin(wz[i])-sin(wxtemp)*cos(wz[i]);
		S[i][2][2] = cos(wxtemp)*cos(wy[i]);
		S[i][2][3] = tz;

		S[i][3][0] = 0;
		S[i][3][1] = 0;
		S[i][3][2] = 0;
		S[i][3][3] = 1;

		//MATRICES KPS
		KPS[i][0][0] = KP[0][0]*S[i][0][0] + KP[0][1]*S[i][1][0] + KP[0][2]*S[i][2][0];
		KPS[i][0][1] = KP[0][0]*S[i][0][1] + KP[0][1]*S[i][1][1] + KP[0][2]*S[i][2][1];
		KPS[i][0][2] = KP[0][0]*S[i][0][2] + KP[0][1]*S[i][1][2] + KP[0][2]*S[i][2][2];
		KPS[i][0][3] = KP[0][0]*S[i][0][3] + KP[0][1]*S[i][1][3] + KP[0][2]*S[i][2][3];

		KPS[i][1][0] = KP[1][1]*S[i][1][0] + KP[1][2]*S[i][2][0];
		KPS[i][1][1] = KP[1][1]*S[i][1][1] + KP[1][2]*S[i][2][1];
		KPS[i][1][2] = KP[1][1]*S[i][1][2] + KP[1][2]*S[i][2][2];
		KPS[i][1][3] = KP[1][1]*S[i][1][3] + KP[1][2]*S[i][2][3];

		KPS[i][2][0] = S[i][2][0];
		KPS[i][2][1] = S[i][2][1];
		KPS[i][2][2] = S[i][2][2];
		KPS[i][2][3] = S[i][2][3];
	}
}

double* Surface::GetNormalObjeto()
{
	double* direccion;
	direccion = new double[3];

	direccion[0] = direccionNormal[0];
	direccion[1] = direccionNormal[1];
	direccion[2] = direccionNormal[2];

	return direccion;
}

void Surface::NumTriangles(int *numX, int* numY, int *numZ)
{
	(*numX) = largoX;
	(*numY) = largoY;
	(*numZ) = largoZ;
}

int Surface::NumVertices()
{
	return numVertices;
}

Triangle* Surface::GetTriangle(int triangleIndexX, int triangleIndexY, int triangleIndexZ)
{
	static TriangleCell* tc = NULL;

	if (tc == NULL)
		tc = mallaTriangulos[triangleIndexX + largoX*triangleIndexZ + largoX*largoZ*triangleIndexY]->GetFirst();
	else
		tc = tc->GetNext();

	if (tc == NULL)
		return NULL;
	else
		return tc->GetTriangle();
}

Triangle* Surface::GetTriangle(int indice)
{
	static TriangleCell* tc = NULL;

	if (tc == NULL)
		tc = mallaTriangulos[indice]->GetFirst();
	else
		tc = tc->GetNext();

	if (tc == NULL)
		return NULL;
	else
		return tc->GetTriangle();
}

bool Surface::IsEmptyTriangleList(int i, int j, int k)
{
	if (mallaTriangulos[i + largoX*k + largoX*largoZ*j] == NULL)
		return true;
	else
		return mallaTriangulos[i + largoX*k + largoX*largoZ*j]->IsEmpty();
}

bool Surface::IsEmptyTriangleList(int indice)
{
	if (mallaTriangulos[indice] == NULL)
		return true;
	else
		return mallaTriangulos[indice]->IsEmpty();
}

double Surface::GetMaxX()
{
	return maxX;
}

double Surface::GetMaxY()
{
	return maxY;
}

double Surface::GetMaxZ()
{
	return maxZ;
}

double Surface::GetMinX()
{
	return minX;
}

double Surface::GetMinY()
{
	return minY;
}

double Surface::GetMinZ()
{
	return minZ;
}