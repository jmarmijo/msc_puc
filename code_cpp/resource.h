//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SurfaceSimplification.rc
//
#define IDD_SURFACESIMPLIFICATION_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_BTN_BROWSE                  1000
#define IDC_RELOCATE_LASER              1000
#define IDC_STEPPER_PORT                1001
#define IDC_TEXTURA                     1002
#define IDC_STEPPER_PORT2               1003
#define IDC_LASER_PORT                  1003
#define IDC_TAKEIMAGE                   1004
#define IDC_EDIT1                       1006
#define IDC_EDIT_TILTS                  1006
#define IDC_LOAD                        1007
#define IDC_EDIT2                       1008
#define IDC_MODEL_NAME                  1008
#define IDC_BUTTON2                     1009
#define IDC_CHECK2                      1015
#define IDC_CALIBRATION                 1015
#define IDC_ESTADO                      1016
#define IDC_REPORTE_ESTADO              1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
