// SurfaceSimplificationDlg.cpp: archivo de implementaci�n
//
#include "stdafx.h"
#include ".\surfacesimplificationdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cuadro de di�logo de CSurfaceSimplificationDlg
CSurfaceSimplificationDlg::CSurfaceSimplificationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSurfaceSimplificationDlg::IDD, pParent)
	, stepperPortNumber(_T("5"))
	, addTexture(FALSE)
	, laserPortNumber(_T("9"))
	, modelNameString(_T(""))
	, calibrate(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSurfaceSimplificationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STEPPER_PORT, stepperPort);
	DDX_Text(pDX, IDC_STEPPER_PORT, stepperPortNumber);
	DDV_MaxChars(pDX, stepperPortNumber, 2);
	DDX_Check(pDX, IDC_TEXTURA, addTexture);
	DDX_Control(pDX, IDC_LASER_PORT, laserPort);
	DDX_Text(pDX, IDC_LASER_PORT, laserPortNumber);
	DDV_MaxChars(pDX, laserPortNumber, 2);
	DDX_Control(pDX, IDC_MODEL_NAME, modelName);
	DDX_Text(pDX, IDC_MODEL_NAME, modelNameString);
	DDX_Check(pDX, IDC_CALIBRATION, calibrate);
}

BEGIN_MESSAGE_MAP(CSurfaceSimplificationDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_EN_CHANGE(IDC_STEPPER_PORT, OnEnChangeStepperPort)
	ON_EN_CHANGE(IDC_LASER_PORT, OnEnChangeLaserPort)
	ON_WM_MOUSEMOVE()
	ON_WM_NCPAINT()
	ON_BN_CLICKED(IDC_LOAD, OnBnClickedLoad)
	ON_EN_CHANGE(IDC_MODEL_NAME, OnEnChangeModelName)
	ON_BN_CLICKED(IDC_RELOCATE_LASER, &CSurfaceSimplificationDlg::OnBnClickedRelocateLaser)
END_MESSAGE_MAP()


// Controladores de mensaje de CSurfaceSimplificationDlg

BOOL CSurfaceSimplificationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

	this->SetWindowPos(0, WINDOW_POSX, WINDOW_POSY, WINDOW_WIDTH, WINDOW_HEIGHT, 0);

	UpdateControls();

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CSurfaceSimplificationDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CSurfaceSimplificationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSurfaceSimplificationDlg::DeshabilitarFormulario()
{
	GetDlgItem(IDOK)->EnableWindow(FALSE);
	GetDlgItem(IDC_LOAD)->EnableWindow(FALSE);

	GetDlgItem(IDC_TEXTURA)->EnableWindow(FALSE);
	GetDlgItem(IDC_CALIBRATION)->EnableWindow(FALSE);

	GetDlgItem(IDC_MODEL_NAME)->EnableWindow(FALSE);

	stepperPort.EnableWindow(false);
	laserPort.EnableWindow(false);

	UpdateData(true);
}

void CSurfaceSimplificationDlg::HabilitarFormulario()
{
	GetDlgItem(IDOK)->EnableWindow(TRUE);
	GetDlgItem(IDC_LOAD)->EnableWindow(TRUE);

	GetDlgItem(IDC_TEXTURA)->EnableWindow(TRUE);
	GetDlgItem(IDC_CALIBRATION)->EnableWindow(TRUE);

	GetDlgItem(IDC_MODEL_NAME)->EnableWindow(TRUE);

	stepperPort.EnableWindow(true);
	laserPort.EnableWindow(true);
}


//SCAN
void CSurfaceSimplificationDlg::OnBnClickedOk()
{
	UpdateData(true);
	if (calibrate)
	{
		DeshabilitarFormulario();
		hThreadCalibrate = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartCalibrate, (LPVOID) this, 0, &dwThreadCalibrate);
	}
	else
	{
		DeshabilitarFormulario();
		hThreadScan = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartScan, (LPVOID) this, 0, &dwThreadScan);
	}
}

void CSurfaceSimplificationDlg::OnBnClickedRelocateLaser()
{
	UpdateData(true);
	
	DeshabilitarFormulario();
	hThreadRelocateLaser = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartRelocateLaser, (LPVOID) this, 0, &dwThreadRelocateLaser);
}

DWORD CSurfaceSimplificationDlg::StartCalibrate(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->Calibrate();
	return 0;
}

DWORD CSurfaceSimplificationDlg::StartScan(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->Scan();
	return 0;
}

DWORD CSurfaceSimplificationDlg::StartRelocateLaser(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->RelocateLaser();
	return 0;
}

DWORD CSurfaceSimplificationDlg::StartRewindStepper(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->RewindStepper();
	return 0;
}

DWORD CSurfaceSimplificationDlg::StartDibujar(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->Dibujar();
	return 0;
}

void CSurfaceSimplificationDlg::RelocateLaser()
{
	this->SetWindowPos(0, WINDOW_POSX, WINDOW_POSY, WINDOW_WIDTH, WINDOW_HEIGHT+77, 0);
	GetDlgItem(IDC_REPORTE_ESTADO)->ShowWindow(TRUE);

	AgregarReporte("Relocating the laser...");
	if (Input::PosicionarStepper(atoi(stepperPortNumber)))
		AgregarReporte(" done.\r\n");
	else
		AgregarReporte(" fail.\r\n");

	HabilitarFormulario();
}

void CSurfaceSimplificationDlg::Dibujar()
{
	draw = new OpenGL();
	draw->start();
}

void CSurfaceSimplificationDlg::RewindStepper()
{
	Input::RewindStepper(atoi(stepperPortNumber));
}

void CSurfaceSimplificationDlg::Calibrate()
{
	Input::SetTexturizar(true);
	Input::SetModelName((char *) (LPCTSTR)modelNameString);

	this->SetWindowPos(0, WINDOW_POSX, WINDOW_POSY, WINDOW_WIDTH, WINDOW_HEIGHT+77, 0);
	GetDlgItem(IDC_REPORTE_ESTADO)->ShowWindow(TRUE);

	AgregarReporte("Scanning laser data...");
	if (Input::Scan(atoi(laserPortNumber), atoi(stepperPortNumber)))
	{
		AgregarReporte(" done.\r\n");

		//devuelve el laser a su posicion de partida mientras mienstras se ejecuta el resto del codigo.
		hThreadRewindStepper = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartRewindStepper, (LPVOID) this, 0, &dwThreadRewindStepper);

		AgregarReporte("Creating calibration data file...");
		Input::Calibrate();
		AgregarReporte(" done.\r\n");

		HabilitarFormulario();
	}
	else
		TerminarPrograma();
}

void CSurfaceSimplificationDlg::Scan()
{
	Input::SetTexturizar(addTexture);
	Input::SetModelName((char *) (LPCTSTR)modelNameString);

	this->SetWindowPos(0, WINDOW_POSX, WINDOW_POSY, WINDOW_WIDTH, WINDOW_HEIGHT+77, 0);
	GetDlgItem(IDC_REPORTE_ESTADO)->ShowWindow(TRUE);

	AgregarReporte("Scanning laser data...");
	if (Input::Scan(atoi(laserPortNumber), atoi(stepperPortNumber)))
	{
		AgregarReporte(" done.\r\n");

		//devuelve el laser a su posicion de partida mientras mienstras se ejecuta el resto del codigo.
		hThreadRewindStepper = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartRewindStepper, (LPVOID) this, 0, &dwThreadRewindStepper);
		
		AgregarReporte("Loading drawing screen...");
		hThreadDibujar = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartDibujar, (LPVOID) this, 0, &dwThreadDibujar);
		AgregarReporte(" done.\r\n");

		AgregarReporte("Reconstructing surface...");
		Input::CargarMesh();
		Input::GenerarMesh();
		AgregarReporte(" done.\r\n");

		if (!addTexture) AgregarReporte("Processing data...");
		else AgregarReporte("Adding texture...");
		Surface *surf = new Surface();
		AgregarReporte(" done.\r\n");

		AgregarReporte("Starting model rendering...");

		if ((bool)(surf->CreateSurface((BOOL)addTexture)))
		{
			if(draw==NULL)
				AgregarReporte(" fail.\r\n");
			else
			{
				draw->SetSurface(surf);
				AgregarReporte(" done.\r\n");
			}
		}
		else
		{
			AgregarReporte(" fail.\r\n");
		}

		HabilitarFormulario();
	}
	else
	{
		AgregarReporte(" fail.\r\n");
		TerminarPrograma();
	}
}

//LOAD SCAN
void CSurfaceSimplificationDlg::OnBnClickedLoad()
{
	UpdateData(true);
	if (calibrate)
	{
		DeshabilitarFormulario();
		hThreadCalibrate = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) LoadCalibration, (LPVOID) this, 0, &dwThreadCalibrate);
	}
	else
	{
		DeshabilitarFormulario();
		hThreadScan = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) LoadScan, (LPVOID) this, 0, &dwThreadScan);
	}
}

DWORD CSurfaceSimplificationDlg::LoadCalibration(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->LoadCalibrationData();
	return 0;
}

DWORD CSurfaceSimplificationDlg::LoadScan(LPVOID lpvoid)
{
	CSurfaceSimplificationDlg * pThis = (CSurfaceSimplificationDlg *) lpvoid;
	pThis->LoadScanData();
	return 0;
}

void CSurfaceSimplificationDlg::LoadCalibrationData()
{
	char *file, *titulo, *aux;
	HWND hwnd;

	//Input::SetTexturizar(true);
	//Input::SetModelName((char *) (LPCTSTR)modelNameString);

	file = new char [500];
	titulo = new char [100];
	aux = new char [100];
	hwnd = this->GetSafeHwnd();

	sprintf_s(titulo, 100, "Select a Laser scans file:");
	if (OpenFileDialogBox(hwnd, file, titulo, "TXT"))
	{
		Input::SetModelName(file);
		Input::CargarDatos(file);
		Input::Calibrate();
		HabilitarFormulario();
	}
}

void CSurfaceSimplificationDlg::LoadScanData()
{
	char *file, *titulo, *aux;
	HWND hwnd;

	file = (char*)malloc(500*sizeof(char));
	titulo = (char*)malloc(100*sizeof(char));
	aux = (char*)malloc(100*sizeof(char));
	hwnd = this->GetSafeHwnd();

	sprintf_s(titulo, 100, "Select a Laser scans file:");
	if (OpenFileDialogBox(hwnd, file, titulo, "TXT"))
	{
		this->SetWindowPos(0, WINDOW_POSX, WINDOW_POSY, WINDOW_WIDTH, WINDOW_HEIGHT+77, 0);
		GetDlgItem(IDC_REPORTE_ESTADO)->ShowWindow(TRUE);

		Input::SetTexturizar(addTexture);
		Input::SetModelName(file);

		AgregarReporte("Loading laser data from file...");
		Input::CargarDatos(file);
		AgregarReporte(" done.\r\n");

		AgregarReporte("Loading drawing screen...");
		hThreadDibujar = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) StartDibujar, (LPVOID) this, 0, &dwThreadDibujar);
		AgregarReporte(" done.\r\n");

		AgregarReporte("Reconstructing surface...");
		Input::GenerarMesh();
		AgregarReporte(" done.\r\n");
		
		if (!addTexture) AgregarReporte("Processing data...");
		else AgregarReporte("Adding texture...");
		Surface *surf = new Surface();
		AgregarReporte(" done.\r\n");
	
		AgregarReporte("Starting model rendering...");
		if ((bool)(surf->CreateSurface((BOOL)addTexture)))
		{
			if(draw==NULL)
				AgregarReporte(" fail.\r\n");
			else
			{
				draw->SetSurface(surf);
				AgregarReporte(" done.\r\n");
			}
		}
		else
		{
			AgregarReporte(" fail.\r\n");
		}
	}
	HabilitarFormulario();

	free(file);
	free(titulo);
	free(aux);
}

void CSurfaceSimplificationDlg::AgregarReporte(char *nuevoReporte)
{
	GetDlgItemTextA(IDC_REPORTE_ESTADO, reporte, 500);
	strcat(reporte, nuevoReporte);
	SetDlgItemTextA(IDC_REPORTE_ESTADO, reporte);
}

void CSurfaceSimplificationDlg::UpdateTilt()
{
	SetDlgItemInt(IDC_EDIT_TILTS, Input::GetTilt());
}

void CSurfaceSimplificationDlg::OnBnClickedCancel()
{
	TerminarPrograma();
}

void CSurfaceSimplificationDlg::TerminarPrograma()
{
	ExitProcess(0);
	OnCancel();
}

void CSurfaceSimplificationDlg::OnEnChangeStepperPort()
{
	UpdateControls();
}

void CSurfaceSimplificationDlg::OnEnChangeLaserPort()
{
	UpdateControls();
}

void CSurfaceSimplificationDlg::OnEnChangeModelName()
{
	UpdateControls();
}

void CSurfaceSimplificationDlg::UpdateControls()
{
	UpdateData(true);
	if (stepperPortNumber == "" || laserPortNumber == "" || modelNameString == "" )
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	else
		GetDlgItem(IDOK)->EnableWindow(true);
}


void CSurfaceSimplificationDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	UpdateTilt();
	CDialog::OnMouseMove(nFlags, point);
}

bool OpenFileDialogBox(HWND hwnd, char * fileName, char *dlgTitle, char* tipo)
{
	OPENFILENAME ofn;       // common dialog box structure
	char szFile[260];       // buffer for file name

	// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;

	// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
	// use the contents of szFile to initialize itself.
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	
	if (strcmp(tipo, "TXT")==0)
		ofn.lpstrFilter = "TXT\0*.TXT\0\0";
	else if (strcmp(tipo, "Triangle")==0)
		ofn.lpstrFilter = "Triangle\0*.TR\0\0";
	else
		ofn.lpstrFilter = "Vertex\0*.VR\0\0";
	
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = "";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST ;

	ofn.lpstrTitle=dlgTitle;

	// Display the Open dialog box. 
	if (GetOpenFileName(&ofn)==TRUE)
	{//a file was selected
		strcpy(fileName, ofn.lpstrFile);
		return TRUE;
	}
	else
		return FALSE;
}