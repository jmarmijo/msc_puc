#pragma once
#define NUM_IMAGENES 3

class Vertex
{
private:
	double x;
	double y;
	double z;
	int **u0;
	int **v0;

	void AllocatePixels();

public:
	Vertex();
	~Vertex(void);
	Vertex(double, double, double);
	void Set(double x, double y, double z);
	double GetX();
	double GetY();
	double GetZ();
	
	bool HasPixels();

	void SetImagePixel(int numImage, int u, int v);
	void GetImagePixel(int numImage, int* u, int* v);
	
};
