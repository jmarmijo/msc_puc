#pragma once

#include "Triangle.h"
#include "Vertex.h"
#include "Input.h"
#include <float.h>
#include "VortexPlane.h"
#include "TriangleList.h"
#include "Vector.h"
#include "Point3D.h"

#define TAM_CELDA	20//14

class Surface
{
private:
	//ATRIBUTOS
	
	int numTriangles;
	int numVertices;
	double maxX, maxY, maxZ;
	double minX, minY, minZ;
	double direccionNormal[3];
	double KPS[NUM_IMAGENES][3][4];
	bool listasInicializadas;

	//PARAMETROS DE CAMARA
	double alfax, alfay;
	double u0, v0, s;
	double wx[3], wy[3], wz[3];
	double tx, ty, tz;

	//POSICION DE LA CAMARA
	double centroCamara[NUM_IMAGENES][3];

	/*inicio-borrar*/
	double posCamara[NUM_IMAGENES][3];
	Vertex* TriangleCenter(Triangle *t);
	/*fin-borrar*/
	double Distancia(double x[3], Vertex *v);

	//Triangle **listaTriangulos;
	TriangleList **mallaTriangulos;
	TriangleList *notProyectedTriangles;
	TriangleList *occludedTriangles;
	Vertex **listaVertices;

	//METODOS
	bool InitialiceLists();
	void GetData(bool);
	void OcclusionDetection();
	void OcclusionDetection(int i0, int j0, int k0, TriangleCell *tc1);
	void MatrizProyeccion();
	void Transformacion(int imagen, double Xp, double Yp, double Zp, int *u, int *v);
	double Radio(Triangle *t, Vertex *v);
	double Distancia3D(Vertex *t1, Vertex *t2);
	bool IsPointInTriangle(double intersection[3], Triangle *t, double normal[3]);
	bool OcclusionDetection(Triangle *t, TriangleList *l);
	bool HaySolapamiento(float n[3], float intersection[3][3], Triangle *t);
	void FiltrarTriangulos();

public:
	int largoX, largoY, largoZ;
	Surface(void);
	~Surface(void);

	bool CreateSurface(bool);

	void NumTriangles(int *numX, int* numY, int *numZ);
	int NumVertices();
	bool IsEmptyTriangleList(int i, int j, int k);
	bool IsEmptyTriangleList(int indice);
	
	Triangle *GetTriangle(int, int, int);
	Triangle *GetTriangle(int);
	double *GetNormalTriangle(int);
	void CalcularNormalObjeto();
	double *GetNormalObjeto();
	TriangleCell* GetNTTriangleCell();
	TriangleCell* GetOccludedTriangleCell();

	double GetMaxX();
	double GetMaxY();
	double GetMaxZ();
	double GetMinX();
	double GetMinY();
	double GetMinZ();

	int minCeldaY, maxCeldaY, yMedio;

	int numTriangulosConTextura, numTriangulosOcluidos;

};
