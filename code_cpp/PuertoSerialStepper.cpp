#include "StdAfx.h"
#include ".\PuertoSerialStepper.h"

PuertoSerialStepper::PuertoSerialStepper()
{
	hPort = INVALID_HANDLE_VALUE;
	tamBuffer = 1;//000;
}

bool PuertoSerialStepper::abrir(LPCTSTR puerto)
{
	if (hPort != INVALID_HANDLE_VALUE)
	{
		return false;
	}

	// Abre el puerto serial.
	hPort = CreateFile (puerto,		//Pointer to the name of the port
                        GENERIC_READ | GENERIC_WRITE | FILE_FLAG_WRITE_THROUGH , // Access (read-write) mode //FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING
						0,            // Share mode
						NULL,         // Pointer to the security attribute
						OPEN_ALWAYS,// How to open the serial port
						0,            // Port att			
						NULL);        // Handle to port with attribute to copy
						
	lastError = GetLastError();
	if(lastError == ERROR_FILE_NOT_FOUND)
	{
		return false;
	}

    // Initialize the DCBlength member. 
	PortDCB.DCBlength = sizeof (DCB);
	
	// Get the default port setting information.
	GetCommState (hPort, &PortDCB);

	PortDCB.BaudRate = 9600;				// Current baud 
	PortDCB.fBinary = FALSE;				// Binary mode; no EOF check 
	PortDCB.fParity = FALSE;				// Enable parity checking 
	PortDCB.fNull = FALSE;					// Disable null stripping RTS flow control 
	PortDCB.fAbortOnError = FALSE;			// Do not abort reads/writes on error
	PortDCB.ByteSize = 8;					// Number of bits/byte, 4-8 
	PortDCB.Parity = NOPARITY;				// 0-4=no,odd,even,mark,space 
	PortDCB.StopBits = 0;// ONESTOPBIT;			// 0,1,2 = 1, 1.5, 2

	// Configure the port according to the specifications of the DCB 
	// structure.

	COMMTIMEOUTS commTimeout;

	if(GetCommTimeouts(hPort, &commTimeout))
	{
		commTimeout.ReadIntervalTimeout = 500;
		commTimeout.ReadTotalTimeoutConstant = 500;
		commTimeout.ReadTotalTimeoutMultiplier = 500;
		commTimeout.WriteTotalTimeoutConstant = 500;
		commTimeout.WriteTotalTimeoutMultiplier = 500;

		if (!SetCommTimeouts(hPort, &commTimeout))
			return false;

		if (SetCommState(hPort, &PortDCB))
			return true;
		else
			return false;
	}
	else
		return false;
}

bool PuertoSerialStepper::cerrar()
{
	bool closeStatus;
	
	if (CloseHandle(hPort))
		closeStatus = true;
	else
		closeStatus = false;

	hPort = INVALID_HANDLE_VALUE;
	return closeStatus;
}

unsigned char PuertoSerialStepper::leer()
{
	buffer = new unsigned char[tamBuffer];
	DWORD dummy;

    if(!ReadFile(hPort, buffer, tamBuffer, &dummy, NULL))
        return NULL;
	else
	    return buffer[0];
}

unsigned char* PuertoSerialStepper::GetBuffer()
{
	return buffer;
}

unsigned char PuertoSerialStepper::ObtenerByteLeido()
{
	return buffer[0];
}

bool PuertoSerialStepper::escribir(unsigned char * bufferIn, int largo)
{
	DWORD dwNumBytesWritten;
    if(bufferIn == NULL)
		return FALSE;

	/*if (WriteFile(hPort, bufferIn, largo, &dwNumBytesWritten, NULL))
		return true;
	else
		return false;*/

	int contador;
	contador=0;

	for (contador = 0; contador < largo; ++contador)
	{
		if (!WriteFile (hPort,              // Port handle
						&(bufferIn[contador]),// Pointer to the data to write 
						1,                  // Number of bytes to write
						&dwNumBytesWritten, // Pointer to the number of bytes 
											// written
						NULL                // Must be NULL for Windows CE
		))
			return false;
	}
	FlushFileBuffers(hPort);
	return true;
}