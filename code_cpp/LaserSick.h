#include "PuertoSerialLaser.h"

#ifndef LASER_LIB
#define LASER_LIB

#define RESET				0
#define TAM_RESET			8

#define REQ_START			1
#define TAM_REQ_START		8

#define REQ_STOP			2
#define TAM_REQ_STOP		8

#define REQ_MEDICIONES		3
#define TAM_REQ_MEDICIONES	8

#define CONFIGURAR			4
#define TAM_CONFIGURAR		16

#define MM					5
#define TAM_MM				39

#define HIGH_SPEED			6
#define TAM_HIGH_SPEED		8

#define MAXNDATA			802

class LaserSick
{
private:
	unsigned char* respuesta;
	unsigned char* mensaje;
	PuertoSerialLaser pSerial;

	unsigned char* Mensaje(int tipoMensaje);
	bool EsMensaje(unsigned char* respuesta, int tipoMensaje);
	unsigned short LMSCRC(unsigned char* theBytes, int lenBytes);
	bool CapturarDatos();
	bool RespuestaChangeBaudRate();

public:
	LaserSick();
	bool Inicializar(unsigned int puerto);
	bool RespuestaConfigurar();
	bool RespuestaMM();
	int ObtenerMedicionAngulo(int angulo);
	void PedirDatos();
	void Finalizar();
	//void ProcesarDatos(int posicion, int **matriz);
};

#endif