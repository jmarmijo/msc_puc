#include "StdAfx.h"
#include ".\trianglelist.h"

TriangleList::TriangleList(void)
{
	numTriangulos = 0;
	first = NULL;
	last = NULL;
}

bool TriangleList::IsEmpty(void)
{
	if (numTriangulos == 0)
		return true;
	else
		return false;
}

TriangleCell* TriangleList::GetFirst(void)
{
	return first;
}

void TriangleList::SetFirst(TriangleCell* t)
{
	first = t;
}

void TriangleList::Bypass(TriangleCell *tc)
{
	TriangleCell *n, *p;

	n = tc->GetNext();
	p = tc->GetPrev();

	if (first == tc)
		first = n;
	else
		p->SetNext(n);

	if (last == tc)
		last = p;
	else
		n->SetPrev(p);

	tc->SetNext(NULL);
	tc->SetPrev(NULL);

	--numTriangulos;
}

void TriangleList::Add(Triangle *t)
{
	TriangleCell *tc;
	tc = new TriangleCell();
	tc->SetTriangle(t);

	if (IsEmpty())
	{
		first = tc;
		last = tc;
	}
	else
	{
		tc->SetPrev(last);
		last->SetNext(tc);
		last = tc;
	}
	++numTriangulos;
}

TriangleList::~TriangleList(void)
{
}
