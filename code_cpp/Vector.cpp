#include "StdAfx.h"
#include ".\vector.h"

Vector::Vector(void)
{
	x = y = z = 0;
}

Vector::Vector(double valX, double valY, double valZ)
{
	x = valX;
	y = valY;
	z = valZ;
}

Vector::~Vector(void)
{
}

double Vector::GetX()
{
	return x;
}

double Vector::GetY()
{
	return y;
}

double Vector::GetZ()
{
	return z;
}

Vector* Vector::Cross(Vector *v)
{
	double valX, valY, valZ;

	valX = y*v->z - z*v->y;
	valY = z*v->x - x*v->z;
	valZ = x*v->y - y*v->x;

	return new Vector(valX, valY, valZ);
}

double Vector::Dot(Vector *v)
{
	return x*v->x + y*v->y + z*v->z;
}

Vector* Vector::Substract(Vector *v)
{
	return new Vector(this->x - v->x, this->y - v->y, this->z - v->z);
}

bool Vector::Equal(Vector *v)
{
	if (x-v->x>VECTOR_ERROR || x-v->x<-VECTOR_ERROR)
		return false;
	if (y-v->y>VECTOR_ERROR || y-v->y<-VECTOR_ERROR)
		return false;
	if (z-v->z>VECTOR_ERROR || z-v->z<-VECTOR_ERROR)
		return false;

	return true;
}
