#pragma once

#include "Triangle.h"

class TriangleCell
{
private:
	Triangle *t;
	TriangleCell *next;
	TriangleCell *prev;
public:
	void SetTriangle(Triangle *newT);
	void SetNext(TriangleCell *nextTC);
	void SetPrev(TriangleCell *prevTC);
	TriangleCell* GetNext();
	TriangleCell* GetPrev();
	Triangle* GetTriangle();

	Triangle *oclusor;

	TriangleCell(void);
	~TriangleCell(void);
};
