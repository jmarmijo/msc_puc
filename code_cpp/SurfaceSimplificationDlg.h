// SurfaceSimplificationDlg.h: archivo de encabezado
//

#pragma once

#include <stdlib.h>
#include "SurfaceSimplification.h"
#include "OpenGL.h"
#include "Input.h"
#include "Surface.h"
#include "afxwin.h"
#include <string.h>

#define WINDOW_POSX 5
#define WINDOW_POSY 5
#define WINDOW_WIDTH 225
#define WINDOW_HEIGHT 358

bool OpenFileDialogBox(HWND hwnd, char * fileName, char *dlgTitle, char* tipo);

// Cuadro de di�logo de CSurfaceSimplificationDlg
class CSurfaceSimplificationDlg : public CDialog
{
// Construcci�n
public:
	CSurfaceSimplificationDlg(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_SURFACESIMPLIFICATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	HANDLE hThreadScan;
	HANDLE hThreadOpenGL;
	HANDLE hThreadCalibrate;
	HANDLE hThreadRelocateLaser;
	HANDLE hThreadRewindStepper;
	HANDLE hThreadDibujar;
	
	DWORD dwThreadScan;
	DWORD dwThreadOpenGL;
	DWORD dwThreadCalibrate;
	DWORD dwThreadRelocateLaser;
	DWORD dwThreadRewindStepper;
	DWORD dwThreadDibujar;
	
	char reporte[500];
	OpenGL *draw;

	void Scan();
	void Calibrate();
	void LoadScanData();
	void LoadCalibrationData();
	void RelocateLaser();
	void RewindStepper();
	void Dibujar();

	void TerminarPrograma();
	void UpdateControls();
	void DeshabilitarFormulario();
	void HabilitarFormulario();

	static DWORD WINAPI StartScan(LPVOID lpvoid);
	static DWORD WINAPI StartCalibrate(LPVOID lpvoid);
	static DWORD WINAPI LoadScan(LPVOID lpvoid);
	static DWORD WINAPI LoadCalibration(LPVOID lpvoid);
	static DWORD WINAPI StartRelocateLaser(LPVOID lpvoid);
	static DWORD WINAPI StartRewindStepper(LPVOID lpvoid);
	static DWORD WINAPI StartDibujar(LPVOID lpvoid);
	
public:
	//METODOS
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnBrowse();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnEnChangeStepperPort();
	afx_msg void OnEnChangeLaserPort();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedLoad();
	afx_msg void OnEnChangeModelName();
	afx_msg void OnBnClickedRelocateLaser();

	void UpdateTilt();
	void AgregarReporte(char *reporte);
	
	//ATRIBUTOS
	CEdit stepperPort;
	CString stepperPortNumber;
	BOOL addTexture;
	CEdit laserPort;
	CString laserPortNumber;
	CEdit numTilts;
	CString tilts;
	CEdit modelName;
	CString modelNameString;
	BOOL calibrate;
};
