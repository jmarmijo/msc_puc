#include "StdAfx.h"
#include "Point3D.h"

Point3D::Point3D(void)
{
	x = y = z = 0.0;
}

Point3D::Point3D(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Point3D::~Point3D(void)
{
}

void Point3D::SetValues(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void Point3D::GetValues(double *x, double *y, double *z)
{
	*x = this->x;
	*y = this->y;
	*z = this->z;
}

double Point3D::GetX()
{
	return x;
}

double Point3D::GetY()
{
	return y;
}

double Point3D::GetZ()
{
	return z;
}

