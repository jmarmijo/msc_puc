#pragma once

#define PI 3.1415926535897932384626433832795

class Celda
{
private:
	double *anguloGrados;
	int *ejeCoordenado;
	Celda *next;
public:
	Celda(void);
	//Celda(double anguloGrados, int ejeCoordenado);
	float AnguloX();
	float AnguloY();
	float AnguloZ();
	void SetNext(Celda *next);
	Celda* GetNext();
	~Celda(void);
};
