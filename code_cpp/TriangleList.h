#pragma once
#include "TriangleCell.h"

class TriangleList
{
public:
	TriangleList(void);
	~TriangleList(void);
	bool IsEmpty(void);
	TriangleCell* GetFirst(void);
	void SetFirst(TriangleCell*);
	void Add(Triangle *tc);
	void Bypass(TriangleCell *tc);

private:
	int numTriangulos;
	TriangleCell *first, *last;
};
