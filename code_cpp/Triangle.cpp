#include "StdAfx.h"
#include ".\triangle.h"

double Abs(double val)
{
	if (val < 0)
		val *= -1;

	return val;
}

void Triangle::Inicializar()
{
	int i;
	for(i=0; i<NUM_IMAGENES; ++i)
		distanciaCentroImagen[i] = DBL_MAX;

	pixelsU = NULL;
	pixelsV = NULL;
	
	image = -1;
}

void Triangle::ResetTexture()
{
	if (pixelsU != NULL && pixelsV != NULL)
	{
		free(pixelsU);
		free(pixelsV);
	}
	
	Inicializar();
}

Triangle::Triangle()
{
	this->v1 = NULL;
	this->v2 = NULL;
	this->v3 = NULL;

	visitado = false;

	Inicializar();
}

Triangle::Triangle(Vertex *v1, Vertex *v2, Vertex *v3)
{
	this->v1 = v1;
	this->v2 = v2;
	this->v3 = v3;

	visitado = false;

	Inicializar();
}

void Triangle::DistanciaCentroImagen(int indiceImagen, double distancia)
{
	distanciaCentroImagen[indiceImagen] = distancia;
}

void Triangle::SetImagePixels(int img, int u[NUM_IMAGENES][3], int v[NUM_IMAGENES][3])
{
	int i, j;
	image = img;

	if (img >= 0)
	{
		if (pixelsU == NULL || pixelsV == NULL)
		{
			pixelsU = (int**)malloc(NUM_IMAGENES*sizeof(int));
			pixelsV = (int**)malloc(NUM_IMAGENES*sizeof(int));

			for(i=0; i<NUM_IMAGENES; ++i)
			{
				pixelsU[i] = (int*)malloc(3*sizeof(int));
				pixelsV[i] = (int*)malloc(3*sizeof(int));
			}
		}
	
		for(i=0; i<NUM_IMAGENES; ++i)
		{
			for(j=0; j<3; ++j)
			{
				pixelsU[i][j] = u[i][j];
				pixelsV[i][j] = v[i][j];
			}
		}
	}
	else
	{
		image = -1;
	}
}

void Triangle::GetImagePixels(int *img, int u[3], int v[3])
{
	*img = image;
	if (image >= 0)
	{
		u[0] = pixelsU[*img][0];
		u[1] = pixelsU[*img][1];
		u[2] = pixelsU[*img][2];
		v[0] = pixelsV[*img][0];
		v[1] = pixelsV[*img][1];
		v[2] = pixelsV[*img][2];
	}
}

Vertex* Triangle::GetVertex(int vertex)
{
	switch(vertex)
	{
	case 0:
		return v1;
		break;
	case 1:
		return v2;
		break;
	case 2:
		return v3;
		break;
	default:
		return NULL;
		break;
	}
}

/*double* Triangle::Normal()
{
	double *n = new double[3];

	n[0] = n1;
	n[1] = n2;
	n[2] = n3;

	return n;
}*/

Triangle::~Triangle(void)
{
}
