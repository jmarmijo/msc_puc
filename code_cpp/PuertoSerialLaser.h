#ifndef PURTO_SERIAL_LIB
#define PURTO_SERIAL_LIB

#ifndef WINVER				// Permitir el uso de características específicas de Windows 95 y Windows NT 4 o posterior.
#define WINVER 0x0501		// Cambiar para establecer el valor apropiado para Windows 98 y Windows 2000 o posterior.
#endif

#include "afxwin.h"
#include "afxcmn.h"

class PuertoSerialLaser
{
private:
	HANDLE hPort;
	DCB PortDCB;
	DWORD lastError;
	unsigned char byteLeido;
	unsigned char *buffer;
	unsigned int tamBuffer;

public:
	PuertoSerialLaser();
	unsigned char* GetBuffer();
	bool abrir(LPCTSTR puerto, int BaudRate);
	bool cerrar();
	unsigned char leer();
	unsigned char *PuertoSerialLaser::leerDatos(unsigned int tamDatos);
	bool escribir(unsigned char * bufferIn, int largo);
	unsigned char ObtenerByteLeido();
};

#endif