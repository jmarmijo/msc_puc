#pragma once

#define VECTOR_ERROR 0.000000000001

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	
	Vector(void);
	Vector(double valX, double valY, double valZ);

	Vector *Cross(Vector *v);
	double Dot(Vector *v);
	Vector* Substract(Vector *v);
	bool Equal(Vector *v);
	double GetX();
	double GetY();
	double GetZ();
	~Vector(void);
};
