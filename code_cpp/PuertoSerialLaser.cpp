#include "StdAfx.h"
#include "PuertoSerialLaser.h"

PuertoSerialLaser::PuertoSerialLaser()
{
	hPort = INVALID_HANDLE_VALUE;
	tamBuffer = 1;
}

bool PuertoSerialLaser::abrir(LPCTSTR puerto, int BaudRate)
{
	if (hPort != INVALID_HANDLE_VALUE)
	{
		return false;
	}

	// Abre el puerto serial.
	hPort = CreateFile (puerto,		//Pointer to the name of the port
                        GENERIC_READ | GENERIC_WRITE | FILE_FLAG_WRITE_THROUGH , // Access (read-write) mode //FILE_FLAG_NO_BUFFERING FILE_FLAG_OVERLAPPED
						0,            // Share mode
						NULL,         // Pointer to the security attribute
						OPEN_ALWAYS,// How to open the serial port
						0,            // Port att			
						NULL);        // Handle to port with attribute to copy

	lastError = GetLastError();
	if(lastError == ERROR_FILE_NOT_FOUND)
	{
		return false;
	}

    // Initialize the DCBlength member. 
	PortDCB.DCBlength = sizeof (DCB);
	
	// Get the default port setting information.
	GetCommState (hPort, &PortDCB);

	PortDCB.BaudRate = BaudRate;			// Current baud 
	PortDCB.fBinary = FALSE;				// Binary mode; no EOF check 
	PortDCB.fParity = FALSE;				// Enable parity checking 
	PortDCB.fNull = FALSE;					// Disable null stripping RTS flow control 
	PortDCB.fAbortOnError = FALSE;			// Do not abort reads/writes on error
	PortDCB.ByteSize = 8;					// Number of bits/byte, 4-8 
	PortDCB.Parity = NOPARITY;				// 0-4=no,odd,even,mark,space 
	PortDCB.StopBits = ONESTOPBIT;			// 0,1,2 = 1, 1.5, 2

	COMMTIMEOUTS commTimeout;

	if(GetCommTimeouts(hPort, &commTimeout))
	{
		commTimeout.ReadIntervalTimeout = 65;
		commTimeout.ReadTotalTimeoutConstant = 65;
		commTimeout.ReadTotalTimeoutMultiplier = 65;
		commTimeout.WriteTotalTimeoutConstant = 65;
		commTimeout.WriteTotalTimeoutMultiplier = 65;

		if (!SetCommTimeouts(hPort, &commTimeout))
			return false;

		if (SetCommState(hPort, &PortDCB))
			return true;
		else
			return false;
	}
	else
		return false;

	// Configure the port according to the specifications of the DCB 
	// structure.
	if (!SetCommState(hPort, &PortDCB))
	{
		// Could not configure the serial port.
		return false;
	}

	return true;
}

bool PuertoSerialLaser::cerrar()
{
	bool closeStatus;
	
	if (CloseHandle(hPort))
		closeStatus = true;
	else
		closeStatus = false;

	hPort = INVALID_HANDLE_VALUE;
	return closeStatus;
}

unsigned char PuertoSerialLaser::leer()
{
	buffer = new unsigned char[tamBuffer];
	DWORD dummy;

    if(!ReadFile(hPort, buffer, tamBuffer, &dummy, NULL))
        return NULL;
	else
	    return buffer[0];
}

/*unsigned char *PuertoSerialLaser::leerDatos(unsigned int tamDatos)
{
	buffer = new unsigned char[tamDatos];
	DWORD dummy;

    if(!ReadFile(hPort, buffer, tamDatos, &dummy, NULL))
        return NULL;
	else
	    return buffer;
}

unsigned char* PuertoSerialLaser::GetBuffer()
{
	return buffer;
}

unsigned char PuertoSerialLaser::ObtenerByteLeido()
{
	return buffer[0];
}*/

bool PuertoSerialLaser::escribir(unsigned char * bufferIn, int largo)
{
	DWORD dwNumBytesWritten;
    if(bufferIn == NULL)
		return FALSE;

	if (WriteFile(hPort, bufferIn, largo, &dwNumBytesWritten, NULL))
		return true;
	else
		return false;

	/*int contador;
	contador=0;

	for (contador = 0; contador < largo; ++contador)
	{
		if (!WriteFile (hPort,              // Port handle
						&(bufferIn[contador]),// Pointer to the data to write 
						1,                  // Number of bytes to write
						&dwNumBytesWritten, // Pointer to the number of bytes 
											// written
						NULL                // Must be NULL for Windows CE
		))
			return false;
	}
	FlushFileBuffers(hPort);
	return true;*/
}